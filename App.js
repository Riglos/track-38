import React, { useEffect, useState } from "react";
import { BackHandler, DeviceEventEmitter } from "react-native";

import * as TaskManager from "expo-task-manager";
// import AppLoading from "./src/components/AppLoading";
import { AppLoading } from "expo";
import AppLoadingComponent from "./src/components/AppLoading";
import { Asset } from "expo-asset";
// import { EventEmitter } from "fbemitter";
import Navigation from "./src/Navigation";
import { Block } from "./src/components";
import { Provider } from "react-redux";
import { persistor, store } from "./src/configureStore";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/lib/integration/react";

// import { persistor, store } from './store';
import { actualizar } from "./src/actions/recorridoActions";

// const STORAGE_KEY = "expo-home-locations";
const LOCATION_UPDATES_TASK = "location-updates";

// const locationEventsEmitter = new EventEmitter();

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === "string") {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

const App = () => {
  const [isReady, setisReady] = useState(false);
  const [rehydrated, setrehydrated] = useState(false);

  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     isReady: false,
  //     rehydrated: false
  //   };
  //   this.backPressSubscriptions = new Set();
  // }

  const _loadAssetsAsync = async () => {
    const imageAssets = cacheImages([
      require("./assets/images/illustration_1.png"),
      require("./assets/images/illustration_2.png"),
      require("./assets/images/illustration_3.png")
    ]);
    // const fontAssets = cacheFonts([FontAwesome.font]);
    await Promise.all(...imageAssets);
  };

  // componentWillMount() {
  //   persistStore(store, {}, () => {
  //     this.setState({ rehydrated: true });
  //   });
  // }
  useEffect(() => {
    persistStore(store, {}, () => {
      setrehydrated(true);
      //     this.setState({ rehydrated: true });
    });
  }, []);

  // componentDidMount = () => {
  //   DeviceEventEmitter.removeAllListeners("hardwareBackPress");
  //   DeviceEventEmitter.addListener("hardwareBackPress", () => {
  //     let invokeDefault = true;
  //     const subscriptions = [];

  //     this.backPressSubscriptions.forEach(sub => subscriptions.push(sub));

  //     for (let i = 0; i < subscriptions.reverse().length; i += 1) {
  //       if (subscriptions[i]()) {
  //         invokeDefault = false;
  //         break;
  //       }
  //     }

  //     //CIERRA APP EN CASO DE TOCAR EL BOTON ATRAS
  //     if (invokeDefault) {
  //       BackHandler.exitApp();
  //     }
  //     // if (invokeDefault) {
  //     //   alert(
  //     //     "Este boton se encuentra deshabilitado por cuestiones de seguridad."
  //     //   );
  //     // }
  //   });

  //   this.backPressSubscriptions.add(this.handleHardwareBack);
  // };
  useEffect(() => {
    DeviceEventEmitter.removeAllListeners("hardwareBackPress");
    DeviceEventEmitter.addListener("hardwareBackPress", () => {
      let invokeDefault = true;
      const subscriptions = [];

      this.backPressSubscriptions.forEach(sub => subscriptions.push(sub));

      for (let i = 0; i < subscriptions.reverse().length; i += 1) {
        if (subscriptions[i]()) {
          invokeDefault = false;
          break;
        }
      }

      //CIERRA APP EN CASO DE TOCAR EL BOTON ATRAS
      if (invokeDefault) {
        BackHandler.exitApp();
      }
      // if (invokeDefault) {
      //   alert(
      //     "Este boton se encuentra deshabilitado por cuestiones de seguridad."
      //   );
      // }
    });

    // this.backPressSubscriptions.add(this.handleHardwareBack);
  }, []);

  // componentWillUnmount = () => {
  //   DeviceEventEmitter.removeAllListeners("hardwareBackPress");
  //   this.backPressSubscriptions.clear();
  // };

  // handleHardwareBack = () => {
  //   // console.log("toco el back hardware");
  // };

  // render() {
  if (!isReady || !rehydrated) {
    return (
      <AppLoading
        startAsync={_loadAssetsAsync}
        onFinish={() => setisReady(true)}
        onError={console.warn}
      />
    );
  }

  return (
    <Provider store={store}>
      <PersistGate loading={<AppLoadingComponent />} persistor={persistor}>
        <Block white>
          <Navigation />
        </Block>
      </PersistGate>
    </Provider>
  );
  // }
};

TaskManager.defineTask(
  LOCATION_UPDATES_TASK,
  async ({ data: { locations } }) => {
    // console.log("actualizo app.js: ", locations);
    if (locations && locations.length > 0) {
      // const savedLocations = await getSavedLocations();
      const newLocations = locations.map(({ coords, timestamp }) => ({
        latitude: coords.latitude,
        longitude: coords.longitude,
        timestamp
      }));
      // savedLocations.push(...newLocations);
      // await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(savedLocations));
      // console.log("tomo punto");
      store.dispatch(actualizar(newLocations));
      // store.dispatch(isUpdate(locations));
      // locationEventsEmitter.emit("update", savedLocations);
    }
  }
);

export default App;
