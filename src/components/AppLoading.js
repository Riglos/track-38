import React, { Component } from "react";
import { Image, StyleSheet, ScrollView, View } from "react-native";
import { Card, Badge, Button, Block, Text, ButtonGreen } from "../components";
import { theme, mocks } from "../constants";

class AppLoading extends Component {
  render() {
    return (
      <Block style={styles.container}>
        {/* <View style={styles.container}> */}
        <Image
          source={require("../../assets/juanri.gif")}
          style={{ width: 300, height: 300 }}
        />
        {/* </View> */}
      </Block>
    );
  }
}

export default AppLoading;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
    // backgroundColor: '#000',
  }
});
