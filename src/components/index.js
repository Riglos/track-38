import Button from './Button';
import Text from './Text';
import Block from './Block';
import Input from './Input';
import Badge from './Badge';
import Card from './Card';
import Switch from './Switch';
import Divider from './Divider';
import Overlay from './Overlay';
import Formulario from './Formulario';
import Lista from './formulario/Lista';
import Fecha from './formulario/Fecha';
import Multiple from './formulario/Multiple';
import Numero from './formulario/Numero';
import Radio from './formulario/Radio';
import TextFormulario from './formulario/TextFormulario';
import Textarea from './formulario/Textarea';
import Label from './formulario/Label';
import Grupo from './formulario/Grupo';
import Repeticion from './formulario/Repeticion';
import Foto from './formulario/Foto';
import Cuve from './formulario/Cuve';

export {
  Cuve,
  Foto,
  Repeticion,
  Grupo,
  Label,
  Textarea,
  TextFormulario,
  Radio,
  Numero,
  Multiple,
  Fecha,
  Lista,
  Formulario,
  Button,
  Text,
  Block,
  Input,
  Badge,
  Card,
  Divider,
  Switch,
  Overlay,
};
