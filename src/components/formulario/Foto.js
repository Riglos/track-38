import React, { useState, useEffect } from "react";
import { StyleSheet, View, ScrollView, Button, TouchableOpacity, Image } from "react-native";
import { Block, Text, Divider } from "../../components";
import * as ImagePicker from 'expo-image-picker';
import { theme } from "../../constants";

export default function Foto({ item, onChange }) {
  console.log("item foto: ", item)
    const [image, setImage]= useState(null);

    useEffect(() => {
      item.value && item.value.uri ? setImage(item.value.uri) : null
    }, [])


    const pickImage = async () => {
        try {
          let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
            base64: true
          });
          if (!result.cancelled) {
            console.log("result image: ", result)
            onChange(result, item.id);
            // this.setState({ image: result.uri });
            setImage(result.uri)
          }
    
          console.log(result);
        } catch (E) {
          console.log(E);
        }
      };
    

    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <Button title="Seleccione imagen desde la galeria" onPress={pickImage} />
        {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
        </View>
    );
  }
//   render() {
//     const { item } = this.props;
//     return (
//       <Block key={item.id} margin={[theme.sizes.base, theme.sizes.base * 2]}>
//         <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
//         <Text key={item.id} style={styles.title}>
//           {item.etiqueta}
//         </Text>
//         <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
//         <View>
//      <Button title="Load Images" onPress={this._handleButtonPress} />
//      <ScrollView>
//        {this.state.photos.map((p, i) => {
//        return (
//          <Image
//            key={i}
//            style={{
//              width: 300,
//              height: 100,
//            }}
//            source={{ uri: p.node.image.uri }}
//          />
//        );
//      })}
//      </ScrollView>
//    </View>
//       </Block>
//     );
// }

export const styles = StyleSheet.create({
  title: {
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
    textDecorationLine: "underline"
  },
  inputs: {
    marginTop: theme.sizes.base * 0.7,
    paddingHorizontal: theme.sizes.base * 2
  },
  inputRow: {
    alignItems: "flex-end"
  },
  textRed: {
    color: "red"
  },
  textArea: {
    // font
    // borderColor: "red",
    // borderWidth: 1
  }
  // block: {
  //   flex: 1,
  // },
  // row: {
  //   flexDirection: 'row',
  // },
  // column: {
  //   flexDirection: 'column',
  // },
  // card: {
  //   borderRadius: theme.sizes.radius,
  // },
  // center: {
  //   alignItems: 'center',
  // },
  // middle: {
  //   justifyContent: 'center',
  // },
  // left: {
  //   justifyContent: 'flex-start',
  // },
  // right: {
  //   justifyContent: 'flex-end',
  // },
  // top: {
  //   justifyContent: 'flex-start',
  // },
  // bottom: {
  //   justifyContent: 'flex-end',
  // },
  // shadow: {
  //   shadowColor: theme.colors.black,
  //   shadowOffset: { width: 0, height: 2 },
  //   shadowOpacity: 0.1,
  //   shadowRadius: 13,
  //   elevation: 2,
  // },
  // accent: { backgroundColor: theme.colors.accent, },
  // primary: { backgroundColor: theme.colors.primary, },
  // secondary: { backgroundColor: theme.colors.secondary, },
  // tertiary: { backgroundColor: theme.colors.tertiary, },
  // black: { backgroundColor: theme.colors.black, },
  // white: { backgroundColor: theme.colors.white, },
  // gray: { backgroundColor: theme.colors.gray, },
  // gray2: { backgroundColor: theme.colors.gray2, },
});
