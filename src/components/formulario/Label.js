import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Block, Text, Divider } from "../../components";
import { theme } from "../../constants";

export default class Label extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { item } = this.props;
    return (
      <Block key={item.id} margin={[theme.sizes.base, theme.sizes.base * 2]}>
        <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
        <Text key={item.id} style={styles.title}>
          {item.etiqueta}
        </Text>
        <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
      </Block>
    );
  }
}

export const styles = StyleSheet.create({
  title: {
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
    textDecorationLine: "underline"
  },
  inputs: {
    marginTop: theme.sizes.base * 0.7,
    paddingHorizontal: theme.sizes.base * 2
  },
  inputRow: {
    alignItems: "flex-end"
  },
  textRed: {
    color: "red"
  },
  textArea: {
    // font
    // borderColor: "red",
    // borderWidth: 1
  }
  // block: {
  //   flex: 1,
  // },
  // row: {
  //   flexDirection: 'row',
  // },
  // column: {
  //   flexDirection: 'column',
  // },
  // card: {
  //   borderRadius: theme.sizes.radius,
  // },
  // center: {
  //   alignItems: 'center',
  // },
  // middle: {
  //   justifyContent: 'center',
  // },
  // left: {
  //   justifyContent: 'flex-start',
  // },
  // right: {
  //   justifyContent: 'flex-end',
  // },
  // top: {
  //   justifyContent: 'flex-start',
  // },
  // bottom: {
  //   justifyContent: 'flex-end',
  // },
  // shadow: {
  //   shadowColor: theme.colors.black,
  //   shadowOffset: { width: 0, height: 2 },
  //   shadowOpacity: 0.1,
  //   shadowRadius: 13,
  //   elevation: 2,
  // },
  // accent: { backgroundColor: theme.colors.accent, },
  // primary: { backgroundColor: theme.colors.primary, },
  // secondary: { backgroundColor: theme.colors.secondary, },
  // tertiary: { backgroundColor: theme.colors.tertiary, },
  // black: { backgroundColor: theme.colors.black, },
  // white: { backgroundColor: theme.colors.white, },
  // gray: { backgroundColor: theme.colors.gray, },
  // gray2: { backgroundColor: theme.colors.gray2, },
});
