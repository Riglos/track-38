import React, { Component } from "react";
import { StyleSheet, TextInput } from "react-native";
import { Block, Text, Divider } from "../../components";
import { theme } from "../../constants";

export default class Textarea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ""
    };
    this.handleChangeText = this.handleChangeText.bind(this);
    // this.agregarContingencia = this.agregarContingencia.bind(this);
  }

  // componentDidMount() {
  //   this.setState({
  //     text: this.props.item.value ? this.props.item.value : ""
  //   });
  // }

  handleChangeText(text, id) {
    this.setState({ text });
    this.props.onChange(text, id);
  }

  render() {
    const { item } = this.props;
    const { text } = this.state;
    return (
      <Block key={item.id} margin={[theme.sizes.base, theme.sizes.base * 2]}>
        <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
        <Text key={item.id} style={item.obligatorio ? styles.textRed : null}>
          {item.etiqueta}
        </Text>
        <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
          <Block shadow>
            {/* <Text gray bold style={{ marginBottom: 10 }}>
              {item.etiqueta}
            </Text> */}
            <TextInput
              style={styles.textArea}
              multiline={true}
              numberOfLines={4}
              defaultValue={item.value ? item.value : text}
              onChangeText={text => this.handleChangeText(text, item.id)}
              placeholder={"Complete el campo"}
            />
          </Block>
        </Block>
      </Block>
    );
  }
}

export const styles = StyleSheet.create({
  inputs: {
    marginTop: theme.sizes.base * 0.7,
    paddingHorizontal: theme.sizes.base * 2
  },
  inputRow: {
    alignItems: "flex-end"
  },
  textRed: {
    color: "red"
  },
  textArea: {
    // font
    // borderColor: "red",
    // borderWidth: 1
  }
  // block: {
  //   flex: 1,
  // },
  // row: {
  //   flexDirection: 'row',
  // },
  // column: {
  //   flexDirection: 'column',
  // },
  // card: {
  //   borderRadius: theme.sizes.radius,
  // },
  // center: {
  //   alignItems: 'center',
  // },
  // middle: {
  //   justifyContent: 'center',
  // },
  // left: {
  //   justifyContent: 'flex-start',
  // },
  // right: {
  //   justifyContent: 'flex-end',
  // },
  // top: {
  //   justifyContent: 'flex-start',
  // },
  // bottom: {
  //   justifyContent: 'flex-end',
  // },
  // shadow: {
  //   shadowColor: theme.colors.black,
  //   shadowOffset: { width: 0, height: 2 },
  //   shadowOpacity: 0.1,
  //   shadowRadius: 13,
  //   elevation: 2,
  // },
  // accent: { backgroundColor: theme.colors.accent, },
  // primary: { backgroundColor: theme.colors.primary, },
  // secondary: { backgroundColor: theme.colors.secondary, },
  // tertiary: { backgroundColor: theme.colors.tertiary, },
  // black: { backgroundColor: theme.colors.black, },
  // white: { backgroundColor: theme.colors.white, },
  // gray: { backgroundColor: theme.colors.gray, },
  // gray2: { backgroundColor: theme.colors.gray2, },
});
