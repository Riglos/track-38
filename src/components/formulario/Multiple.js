import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { CheckBox } from "react-native-elements";
import { Block, Text } from "../../components";
import { theme } from "../../constants";

export default class Multiple extends Component {
  constructor(props) {
    super(props);
    this.handleMultipleChange = this.handleMultipleChange.bind(this);
  }

  handleMultipleChange(i) {
    console.log("this.props: ", this.props);
    let copia = this.props.item;
    let arreglo;
    if (this.props.item.value) {
      arreglo = [...copia.value];
      arreglo[i] = { ...arreglo[i], value: !arreglo[i].value };
    } else {
      arreglo = [...copia.datos];
      for (let index = 0; index < arreglo.length; index++) {
        index === i
          ? (arreglo[i] = { ...arreglo[i], value: !arreglo[i].value })
          : (arreglo[index] = {
              ...arreglo[index],
              value: false
            });
      }
    }
    // console.log("arreglo: ", arreglo);
    this.setState({ radiosButton: arreglo });
    this.props.onChange(arreglo, this.props.item.id);
  }

  render() {
    const { item } = this.props;
    const { datos } = this.props.item;
    return (
      <Block key={item.id} margin={[theme.sizes.base, theme.sizes.base * 2]}>
        <Text key={item.id} style={item.obligatorio ? styles.textRed : null}>
          {item.etiqueta}
        </Text>
        {/* <Text gray bold style={{ marginBottom: 10 }}>
              {item.etiqueta}
            </Text> */}
        {datos.map((dato, i) => {
          return (
            <CheckBox
              key={dato.id}
              title={dato.valor}
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checked={item.value !== undefined ? item.value[i].value : null}
              onPress={() => this.handleMultipleChange(i)}
            ></CheckBox>
          );
        })}
      </Block>
    );
  }
}

export const styles = StyleSheet.create({
  textRed: {
    color: "red"
  }
  // block: {
  //   flex: 1,
  // },
  // row: {
  //   flexDirection: 'row',
  // },
  // column: {
  //   flexDirection: 'column',
  // },
  // card: {
  //   borderRadius: theme.sizes.radius,
  // },
  // center: {
  //   alignItems: 'center',
  // },
  // middle: {
  //   justifyContent: 'center',
  // },
  // left: {
  //   justifyContent: 'flex-start',
  // },
  // right: {
  //   justifyContent: 'flex-end',
  // },
  // top: {
  //   justifyContent: 'flex-start',
  // },
  // bottom: {
  //   justifyContent: 'flex-end',
  // },
  // shadow: {
  //   shadowColor: theme.colors.black,
  //   shadowOffset: { width: 0, height: 2 },
  //   shadowOpacity: 0.1,
  //   shadowRadius: 13,
  //   elevation: 2,
  // },
  // accent: { backgroundColor: theme.colors.accent, },
  // primary: { backgroundColor: theme.colors.primary, },
  // secondary: { backgroundColor: theme.colors.secondary, },
  // tertiary: { backgroundColor: theme.colors.tertiary, },
  // black: { backgroundColor: theme.colors.black, },
  // white: { backgroundColor: theme.colors.white, },
  // gray: { backgroundColor: theme.colors.gray, },
  // gray2: { backgroundColor: theme.colors.gray2, },
});
