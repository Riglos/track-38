import React, { Component } from "react";
import { StyleSheet, Picker } from "react-native";
import { Block, Text } from "../../components";
import { theme } from "../../constants";

export default class Lista extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null
    };
    this.handleChangeSelect = this.handleChangeSelect.bind(this);
  }

  handleChangeSelect(itemValue) {
    this.setState({ value: itemValue });
    this.props.onChange(itemValue, this.props.item.id);
  }

  render() {
    const { item } = this.props;
    const { datos } = this.props.item;
    const { value } = this.state;

    return (
      <Block key={item.id} margin={[theme.sizes.base, theme.sizes.base * 2]}>
        <Text key={item.id} style={item.obligatorio ? styles.textRed : null}>
          {item.etiqueta}
        </Text>
        {/* <Text gray bold style={{ marginBottom: 10 }}>
              {item.etiqueta}
            </Text> */}
        <Picker
          selectedValue={item.value || item.value === 0 ? item.value : value}
          mode="dropdown"
          onValueChange={itemValue => this.handleChangeSelect(itemValue)}
        >
          <Picker.Item key="seleccione" label="Seleccione" value={null} />
          {datos.map(dato => {
            return (
              <Picker.Item key={dato.id} label={dato.valor} value={dato.id} />
            );
          })}
        </Picker>
      </Block>
    );
  }
}

export const styles = StyleSheet.create({
  textRed: {
    color: "red"
  }
});
