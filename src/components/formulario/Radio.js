import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import { CheckBox } from "react-native-elements";
import { Card, Badge, Block, Button, Text, Divider } from "../../components";
import { theme } from "../../constants";

export default class Radio extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   radioButton: null
    // };
    this.handleChangeRadio = this.handleChangeRadio.bind(this);
  }

  handleChangeRadio(id) {
    // this.setState({ text });
    // this.setState({ radioButton: id });
    id === this.props.item.value ? this.props.onChange(null, this.props.item.id):
    this.props.onChange(id, this.props.item.id);
  }

  render() {
    const { item } = this.props;
    const { datos } = this.props.item;

    return (
      <Block key={item.id} margin={[theme.sizes.base, theme.sizes.base * 2]}>
        <Text key={item.id} style={item.obligatorio ? styles.textRed : null}>
          {item.etiqueta}
        </Text>
        {/* <Text gray bold style={{ marginBottom: 10 }}>
              {item.etiqueta}
            </Text> */}
        {datos.map(dato => {
          return (
            <CheckBox
              key={dato.id}
              title={dato.valor}
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checked={item.value === dato.id}
              // checked={this.state.radioButton === dato.id}
              onPress={() => this.handleChangeRadio(dato.id)}
            ></CheckBox>
          );
        })}
      </Block>
    );
  }
}

export const styles = StyleSheet.create({
  textRed: {
    color: "red"
  }
  // block: {
  //   flex: 1,
  // },
  // row: {
  //   flexDirection: 'row',
  // },
  // column: {
  //   flexDirection: 'column',
  // },
  // card: {
  //   borderRadius: theme.sizes.radius,
  // },
  // center: {
  //   alignItems: 'center',
  // },
  // middle: {
  //   justifyContent: 'center',
  // },
  // left: {
  //   justifyContent: 'flex-start',
  // },
  // right: {
  //   justifyContent: 'flex-end',
  // },
  // top: {
  //   justifyContent: 'flex-start',
  // },
  // bottom: {
  //   justifyContent: 'flex-end',
  // },
  // shadow: {
  //   shadowColor: theme.colors.black,
  //   shadowOffset: { width: 0, height: 2 },
  //   shadowOpacity: 0.1,
  //   shadowRadius: 13,
  //   elevation: 2,
  // },
  // accent: { backgroundColor: theme.colors.accent, },
  // primary: { backgroundColor: theme.colors.primary, },
  // secondary: { backgroundColor: theme.colors.secondary, },
  // tertiary: { backgroundColor: theme.colors.tertiary, },
  // black: { backgroundColor: theme.colors.black, },
  // white: { backgroundColor: theme.colors.white, },
  // gray: { backgroundColor: theme.colors.gray, },
  // gray2: { backgroundColor: theme.colors.gray2, },
});
