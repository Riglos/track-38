import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { Icon, Badge } from "react-native-elements";
import * as formActions from "../../actions/formActions";
import { useDispatch, useSelector } from "react-redux";
import {
  Block,
  Text,
  Textarea,
  TextFormulario,
  Radio,
  Numero,
  Multiple,
  Fecha,
  Lista,
  Label,
  Divider,
  Card,
  Foto
} from "../../components";
import { theme } from "../../constants";

export default function Repeticion({ item, onChange, guardar2 }) {
  const dispatch = useDispatch();
  // console.log("GRUPO: ", item);
  let arreglo = item.value !== undefined ? item.value : item.items;
  const handleModify = (e, itemId) => {
    let newForm = arreglo.map(item =>
      item.id === itemId ? { ...item, value: e } : item
    );

    onChange(newForm, item.id);
    // guardar2()
  };

  const handleEliminarRepeticion = () => {
    // console.log("eliminar rep");
    dispatch(formActions.eliminarRepeticion(item));
  };

  return (
    <Block
    // style={{ borderWidth: 0.2 }}
    // shadow
    // margin={[theme.sizes.base, theme.sizes.base * 2]}
    >
      <Card middle shadow>
        {/* <Text style={styles.title}>{item.etiqueta}</Text> */}
        <Text style={styles.title}>REPETICION {item.id}</Text>
        <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
        {arreglo.map((item, i) => {
          console.log("arreglo: ", item)
          switch (item.tipoDato) {
            case "LISTA":
              return (
                <Lista key={item.id} item={item} onChange={handleModify} />
              );
            case "LABEL":
              return <Label key={item.id} item={item} />;
            case "TEXTAREA":
              return (
                <Textarea key={item.id} item={item} onChange={handleModify} />
              );
            case "FECHA":
              return (
                <Fecha key={item.id} item={item} onChange={handleModify} />
              );
              case "FOTO":
                return (
                  <Foto
                    key={item.id}
                    item={item}
                    onChange={handleModify}
                  />
                );
            case "NUMERO":
              return (
                <Numero key={item.id} item={item} onChange={handleModify} />
              );
            case "MULTIPLE":
              return (
                <Multiple key={item.id} item={item} onChange={handleModify} />
              );
            case "RADIO":
              return (
                <Radio key={item.id} item={item} onChange={handleModify} />
              );
            case "TEXT":
              return (
                <TextFormulario
                  key={item.id}
                  item={item}
                  onChange={handleModify}
                />
              );
            default:
              return <Text key={item.id}>no encontrado</Text>;
          }
        })}
        <Block
          row
          space="between"
          margin={[10, 0]}
          //  style={styles.inputRow}
        >
          <Block>
            <Text
              gray
              // style={{ marginBottom: 10 }}
            >
              ELIMINAR REPETICION
            </Text>
            {/* <TextInput
                  placeholder="Ingrese CUVE"
                  defaultValue={cuve}
                  onChangeText={text => this.handleEditCuve(text)}
                /> */}
          </Block>
          <TouchableOpacity onPress={handleEliminarRepeticion}>
            <Icon
              name="remove-circle-outline"
              type="material"
              color={theme.colors.gray}
            />
          </TouchableOpacity>
        </Block>
      </Card>
    </Block>
  );
}

export const styles = StyleSheet.create({
  title: {
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
    textDecorationLine: "underline"
  },
  inputs: {
    marginTop: theme.sizes.base * 0.7,
    paddingHorizontal: theme.sizes.base * 2
  },
  inputRow: {
    alignItems: "flex-end"
  },
  textRed: {
    color: "red"
  },
  textArea: {
    // font
    // borderColor: "red",
    // borderWidth: 1
  }
});
