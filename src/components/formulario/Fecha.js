import React, { Component } from "react";
import { StyleSheet } from "react-native";
import Moment from "moment";
import { DatePicker } from "native-base";
import { Block, Text, Divider } from "../../components";
import { theme } from "../../constants";

export default class Fecha extends Component {
  constructor(props) {
    super(props);
    this.state = { chosenDate: new Date(2020, 4, 4), chosenDate2: null };
    // this.state = { chosenDate: null };
    this.setDate = this.setDate.bind(this);
  }

  setDate(newDate) {
    // console.log("newDate: ", newDate);
    // 03/06/2020
    let fecha = Moment(new Date(newDate)).format("YYYY, M, D");
    this.setState({ chosenDate: newDate });
    this.props.onChange(fecha, this.props.item.id);
  }

  componentWillReceiveProps(props, nextProps) {
    // console.log("entra aca cambia?", props);
    // console.log("entra aca cambia?", nextProps);
    if (props.item.value) {
      let fecha = Moment(new Date(props.item.value)).format("YYYY, M, D");
      let parse = fecha.replace(/(^")|("$)/g, "");
      let fechaParsed = new Date(parse);

      this.setState({
        chosenDate: fechaParsed,
        chosenDate2: fechaParsed
      });
    }
  }

  render() {
    const { item } = this.props;

    return (
      <Block key={item.id} margin={[theme.sizes.base, theme.sizes.base * 2]}>
        <Text key={item.id} style={item.obligatorio ? styles.textRed : null}>
          {item.etiqueta}
        </Text>
        {/* <Text gray bold style={{ marginBottom: 10 }}>
              {item.etiqueta}
            </Text> */}
        {this.state.chosenDate2 && (
          <DatePicker
            defaultDate={this.state.chosenDate2}
            minimumDate={new Date(2020, 1, 1)}
            maximumDate={new Date(2025, 12, 31)}
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Seleccione fecha"
            textStyle={{ color: "green" }}
            placeHolderTextStyle={{ color: "#d3d3d3" }}
            onDateChange={this.setDate}
            disabled={false}
          />
        )}

        {!this.state.chosenDate2 && (
          <DatePicker
            defaultDate={this.state.chosenDate}
            minimumDate={new Date(2020, 1, 1)}
            maximumDate={new Date(2025, 12, 31)}
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Seleccione fecha"
            textStyle={{ color: "green" }}
            placeHolderTextStyle={{ color: "#d3d3d3" }}
            onDateChange={this.setDate}
            disabled={false}
          />
        )}
        <Text>
          Fecha:{" "}
          {item.value
            ? item.value
            : this.state.chosenDate
            ? this.state.chosenDate.toString().substr(4, 12)
            : null}
        </Text>
      </Block>
    );
  }
}

export const styles = StyleSheet.create({
  textRed: {
    color: "red"
  }
  // block: {
  //   flex: 1,
  // },
  // row: {
  //   flexDirection: 'row',
  // },
  // column: {
  //   flexDirection: 'column',
  // },
  // card: {
  //   borderRadius: theme.sizes.radius,
  // },
  // center: {
  //   alignItems: 'center',
  // },
  // middle: {
  //   justifyContent: 'center',
  // },
  // left: {
  //   justifyContent: 'flex-start',
  // },
  // right: {
  //   justifyContent: 'flex-end',
  // },
  // top: {
  //   justifyContent: 'flex-start',
  // },
  // bottom: {
  //   justifyContent: 'flex-end',
  // },
  // shadow: {
  //   shadowColor: theme.colors.black,
  //   shadowOffset: { width: 0, height: 2 },
  //   shadowOpacity: 0.1,
  //   shadowRadius: 13,
  //   elevation: 2,
  // },
  // accent: { backgroundColor: theme.colors.accent, },
  // primary: { backgroundColor: theme.colors.primary, },
  // secondary: { backgroundColor: theme.colors.secondary, },
  // tertiary: { backgroundColor: theme.colors.tertiary, },
  // black: { backgroundColor: theme.colors.black, },
  // white: { backgroundColor: theme.colors.white, },
  // gray: { backgroundColor: theme.colors.gray, },
  // gray2: { backgroundColor: theme.colors.gray2, },
});
