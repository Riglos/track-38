import React, {useState}from "react";
import { StyleSheet, TextInput } from "react-native";
import { Block, Text, Divider } from "../../components";
import { theme } from "../../constants";
import { useDispatch, useSelector } from "react-redux";
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';


export default function Imagen(
    // {item, onChange}
    ) {
    const dispatch = useDispatch();
    const [text, setText] = useState(null)

  const handleChangeText =(text2) => {
    // this.setState({ text });
    setText(text2);
    // onChange(text2, item.id);
  }

    return (
      <Block 
    //   key={item.id}
       margin={[theme.sizes.base, theme.sizes.base * 2]}>
        <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
        <Text 
        // key={item.id} 
        // style={item.obligatorio ? styles.textRed : null}
        >
          {/* {item.etiqueta} */}
          ETIQUETA
        </Text>
        <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
          {/* <Block shadow>
            {/* <Text gray bold style={{ marginBottom: 10 }}>
              {item.etiqueta}
            </Text> */}
            {/* <TextInput
              type="numeric"
              keyboardType="numeric"
              style={styles.textArea}
              defaultValue={item.value ? item.value : text}
              onChangeText={text => handleChangeText(text)}
              placeholder={"Complete el campo"}
            /> */}
          {/* </Block> */}
        </Block>
      </Block>
    );
}

export const styles = StyleSheet.create({
  inputs: {
    marginTop: theme.sizes.base * 0.7,
    paddingHorizontal: theme.sizes.base * 2
  },
  inputRow: {
    alignItems: "flex-end"
  },
  textRed: {
    color: "red"
  },
  textArea: {
    // borderColor: "red",
    // borderWidth: 1
  }
  // block: {
  //   flex: 1,
  // },
  // row: {
  //   flexDirection: 'row',
  // },
  // column: {
  //   flexDirection: 'column',
  // },
  // card: {
  //   borderRadius: theme.sizes.radius,
  // },
  // center: {
  //   alignItems: 'center',
  // },
  // middle: {
  //   justifyContent: 'center',
  // },
  // left: {
  //   justifyContent: 'flex-start',
  // },
  // right: {
  //   justifyContent: 'flex-end',
  // },
  // top: {
  //   justifyContent: 'flex-start',
  // },
  // bottom: {
  //   justifyContent: 'flex-end',
  // },
  // shadow: {
  //   shadowColor: theme.colors.black,
  //   shadowOffset: { width: 0, height: 2 },
  //   shadowOpacity: 0.1,
  //   shadowRadius: 13,
  //   elevation: 2,
  // },
  // accent: { backgroundColor: theme.colors.accent, },
  // primary: { backgroundColor: theme.colors.primary, },
  // secondary: { backgroundColor: theme.colors.secondary, },
  // tertiary: { backgroundColor: theme.colors.tertiary, },
  // black: { backgroundColor: theme.colors.black, },
  // white: { backgroundColor: theme.colors.white, },
  // gray: { backgroundColor: theme.colors.gray, },
  // gray2: { backgroundColor: theme.colors.gray2, },
});
