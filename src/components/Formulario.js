import React, {Component} from 'react';
import {connect} from 'react-redux';
// import * as formActions from "../actions/formActions";
import {
  Block,
  Text,
  Textarea,
  TextFormulario,
  Radio,
  Numero,
  Multiple,
  Fecha,
  Lista,
  Label,
  Grupo,
  Repeticion,
  Foto,
  Cuve,
} from '../components';
import {theme} from '../constants';

class Formulario extends Component {
  constructor(props) {
    super(props);
  }
  //ACTUALIZAR SELECTED CON DATOS DE RESUELTOS
  // componentWillMount() {
  //   this.props.changeSelectedFormularioCompleted(this.props.itemsResueltos);
  // console.log("selected: ", this.props.formularioSelected);
  // console.log("itemsResueltos: ", this.props.itemsResueltos);
  // }

  render() {
    return (
      <Block margin={[theme.sizes.base, theme.sizes.base * 2]}>
        {this.props.formularioSelected.items.map((item, i) => {
          // console.log("item F: ", item);
          switch (item.tipoDato) {
            case 'LISTA':
              return (
                <Lista
                  key={item.id}
                  item={item}
                  onChange={this.props.handleModify}
                />
              );
            case 'LABEL':
              return <Label key={item.id} item={item} />;
            case 'CUVE':
              return <Cuve key={item.id} item={item} />;
            case 'TEXTAREA':
              return (
                <Textarea
                  key={item.id}
                  item={item}
                  onChange={this.props.handleModify}
                />
              );
            case 'FOTO':
              return (
                <Foto
                  key={item.id}
                  item={item}
                  onChange={this.props.handleModify}
                />
              );
            case 'FECHA':
              return (
                <Fecha
                  key={item.id}
                  item={item}
                  onChange={this.props.handleModify}
                />
              );
            case 'NUMERO':
              return (
                <Numero
                  key={item.id}
                  item={item}
                  onChange={this.props.handleModify}
                />
              );
            // case "IMAGEN":
            //   return (
            //     <Imagen
            //       key={item.id}
            //       item={item}
            //       onChange={this.props.handleModify}
            //     />
            //   );
            case 'GRUPO':
              return (
                <Grupo
                  key={item.id}
                  item={item}
                  onChange={this.props.handleModify}
                  guardar2={this.props.guardar2}
                />
              );
            case 'REPETICION':
              return (
                <Repeticion
                  key={item.id}
                  item={item}
                  onChange={this.props.handleModify}
                  guardar2={this.props.guardar2}
                />
              );
            case 'MULTIPLE':
              return (
                <Multiple
                  key={item.id}
                  item={item}
                  onChange={this.props.handleModify}
                />
              );
            case 'RADIO':
              return (
                <Radio
                  key={item.id}
                  item={item}
                  onChange={this.props.handleModify}
                />
              );
            case 'TEXT':
              return (
                <TextFormulario
                  key={item.id}
                  item={item}
                  onChange={this.props.handleModify}
                />
              );
            default:
              return <Text key={item.id}>no encontrado</Text>;
          }
        })}
        {/* <Imagen */}
        {/* // key={item.id}
                    // item={item}
                    // onChange={this.props.handleModify}
                  // /> */}
      </Block>
    );
  }
}

const mapStateToProps = (state) => ({
  formularioSelected: state.formularios.formularioSelected,
});

// const mapDispatchToProps = dispatch => ({
// changeSelectedFormularioCompleted: formCompleted =>
//   dispatch(formActions.changeSelectedFormularioCompleted(formCompleted))
// });

export default connect(mapStateToProps, null)(Formulario);
