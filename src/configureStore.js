// import { createStore, applyMiddleware, compose } from "redux";
// import reducers from "./reducers";
// import thunk from "redux-thunk";

// const initialState = {};
// const middleware = [thunk];

// export default (configureStore = () => {
//   let store = createStore(
//     reducers,
//     initialState,
//     compose(
//       applyMiddleware(...middleware),
//       window.__REDUX_DEVTOOLS_EXTENSION__
//         ? window.__REDUX_DEVTOOLS_EXTENSION__()
//         : f => f
//     )
//   );
//   return store;
// });
import { createStore, applyMiddleware, compose } from "redux";
import reducers from "./reducers";
import thunk from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
// import AsyncStorage from "@react-native-community/async-storage";
import { AsyncStorage } from "react-native";
// import storage from "redux-persist/lib/storage";

const initialState = {};
const middleware = [thunk];

// const persistConfig = {
//   key: "root",
//   storage
// };
const persistConfig = {
  keyPrefix: "",
  key: "root",
  storage: AsyncStorage
};

const persistedReducer = persistReducer(persistConfig, reducers);

// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// export const store = createStore(
//   persistedReducer,
//   initialState,
//   composeEnhancers(applyMiddleware(...middleware))
// );
export const store = createStore(
  persistedReducer,
  initialState,
  compose(
    applyMiddleware(...middleware),
    window.__REDUX_DEVTOOLS_EXTENSION__
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : f => f
  )
);
export const persistor = persistStore(store);
