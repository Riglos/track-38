import {
  COPY_FORM,
  SAVE_FORM,
  CHANGE_SELECTED_FORM,
  CHANGE_SELECTED_FORM_COMPLETED,
  ADD_REP,
  DELETE_REP,
  AGREGAR_CUVE,
  ELIMINAR_CUVE,
} from '../constants/api';

export const copyFormularioSelected = (form) => {
  return {
    type: COPY_FORM,
    form,
  };
};

export const agregarCuve = (idForm, cuve) => {
  return {
    type: AGREGAR_CUVE,
    idForm,
    cuve,
  };
};

export const eliminarCuve = (idForm, posicion) => {
  return {
    type: ELIMINAR_CUVE,
    idForm,
    posicion,
  };
};

export const agregarRepeticion = (repeticion, original) => {
  return {
    type: ADD_REP,
    repeticion,
    original,
  };
};

export const eliminarRepeticion = (repeticion) => {
  return {
    type: DELETE_REP,
    repeticion,
  };
};

export const saveNewFormulario = (form) => {
  return {
    type: SAVE_FORM,
    form,
  };
};

export const changeSelectedFormulario = (form) => {
  return {
    type: CHANGE_SELECTED_FORM,
    form,
  };
};

export const changeSelectedFormularioCompleted = (formCompleted) => {
  return {
    type: CHANGE_SELECTED_FORM_COMPLETED,
    formCompleted,
  };
};
