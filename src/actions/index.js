import * as userActions from "./userActions";
import * as taskActions from "./taskActions";
import * as recorridoActions from "./recorridoActions";
import * as persistenciaActions from "./persistenciaActions";

export { taskActions, userActions, recorridoActions, 
    persistenciaActions
 };
