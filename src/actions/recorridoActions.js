import moment from 'moment';
import {VERSION, AMBIENTE} from '../constants/version';
import {
  RECORRIDO_ATTEMPT,
  RECORRIDO_SUCCESS,
  RECORRIDO_FAILED,
  RECORRIDO_COMENZAR,
  RECORRIDO_ACTUALIZAR,
  RECORRIDO_FINALIZAR,
  ELIMINAR_RECORRIDO_PENDIENTE,
  RECORRIDO_FINALIZAR_PENDIENTE,
  RECORRIDO_COMENZAR_SIN_TRACKEO,
  ELIMINAR_RECORRIDO_SIN_TRACKEO,
} from '../constants/api';

export function isLoadingRecorrido(bool) {
  return {
    type: RECORRIDO_ATTEMPT,
    bool,
  };
}

export function recorridoSuccess() {
  return {
    type: RECORRIDO_SUCCESS,
  };
}

export function recorridoFailed(error) {
  return {
    type: RECORRIDO_FAILED,
    error,
  };
}

export function comenzar() {
  return {
    type: RECORRIDO_COMENZAR,
    isLoading: 'true',
  };
}

export function comenzarRecorridoSinTrackeo() {
  return {
    type: RECORRIDO_COMENZAR_SIN_TRACKEO,
    startedWithoutTracking: true,
  };
}

export function actualizar(locations) {
  return {
    type: RECORRIDO_ACTUALIZAR,
    locations,
  };
}

export function isUpdate(locations) {
  return (dispatch) => {
    dispatch(actualizar(locations));
  };
}

export function finalizar(taskSeleccionadas, request) {
  return {
    type: RECORRIDO_FINALIZAR,
    taskSeleccionadas,
    request,
  };
}

export function finalizarPendiente(taskSeleccionadas, request) {
  return {
    type: RECORRIDO_FINALIZAR_PENDIENTE,
    taskSeleccionadas,
    request,
  };
}

export function eliminarRecorridoPendiente(fecha) {
  return {
    type: ELIMINAR_RECORRIDO_PENDIENTE,
    fecha,
  };
}

export function finalizarRecorridoSinTrackeo() {
  return {
    type: ELIMINAR_RECORRIDO_SIN_TRACKEO,
  };
}

export function isStarted() {
  return (dispatch) => {
    dispatch(comenzar());
  };
}

export function postRecorrido(tareasCompletadas, recorrido, usuario, estado) {
  console.log('ESTADO: ', estado);
  return (dispatch) => {
    dispatch(isLoadingRecorrido(true));
    var actividades = [];
    var coordenadas = [];
    let cuves = [];
    let newResultadosFormulario;
    tareasCompletadas.map((finalizada) => {
      console.log('finalizada: ', finalizada);
      //CUVES
      finalizada.servicio.formularios.map((formulario) => {
        formulario.cuves
          ? finalizada.resultadosFormulario
            ? finalizada.resultadosFormulario.map((res) => {
                finalizada.resultadosFormulario =
                  res.idFormulario === formulario.id
                    ? {...res, cuves: formulario.cuves}
                    : null;
              })
            : // (cuves = [...cuves, {cuves: formulario.cuves, id: formulario.id}])
              null
          : null;
      });

      console.log(
        'CUVES resultadosFormulario: ',
        finalizada.resultadosFormulario
      );

      if (finalizada.id > 0) {
        actividad = {
          inicio: moment(new Date(finalizada.isStarted)).format(
            'MMMM D, YYYY h:mm:ss a'
          ),
          fin: moment(new Date(finalizada.isFinished)).format(
            'MMMM D, YYYY h:mm:ss a'
          ),
          idAsignacionAgenteServicio: finalizada.id,
          resultadoFormularios: finalizada.resultadosFormulario,
          coordenadaDTO: {
            latitud: JSON.stringify(finalizada.coordenada.latitude),
            longitud: JSON.stringify(finalizada.coordenada.longitude),
            horaRegistro: moment(
              new Date(finalizada.coordenada.timestamp)
            ).format('MMMM D, YYYY h:mm:ss a'),
          },
        };
      } else {
        actividad = {
          inicio: moment(new Date(finalizada.isStarted)).format(
            'MMMM D, YYYY h:mm:ss a'
          ),
          fin: moment(new Date(finalizada.isFinished)).format(
            'MMMM D, YYYY h:mm:ss a'
          ),
          idAsignacionAgenteServicio: finalizada.id,
          idTipoServicio: finalizada.servicio.id,
          resultadoFormularios: finalizada.resultadosFormulario,
          detalle: finalizada.detalle,
          ente: {
            tiposEntes: finalizada.servicio.tiposEntes,
            idEnte: finalizada.servicio.idEnte,
            nombreEnte: finalizada.servicio.nombreEnte,
          },
          coordenadaDTO: {
            latitud: JSON.stringify(finalizada.coordenada.latitude),
            longitud: JSON.stringify(finalizada.coordenada.longitude),
            horaRegistro: moment(
              new Date(finalizada.coordenada.timestamp)
            ).format('MMMM D, YYYY h:mm:ss a'),
          },
        };
      }
      actividades.push(actividad);
    });

    recorrido.locations.map((location) => {
      coordenada = {
        latitud: JSON.stringify(location[0].latitude),
        longitud: JSON.stringify(location[0].longitude),
        horaRegistro: moment(new Date(location[0].timestamp)).format(
          'MMMM D, YYYY h:mm:ss a'
        ),
      };
      coordenadas.push(coordenada);
    });

    var body = {
      token: usuario.token,
      usuario: usuario.nombreUsuario,
      actividades,
      coordenadas,
      inicio: moment(recorrido.dateStarted).format('MMMM D, YYYY h:mm:ss a'),
      fin: moment(new Date()).format('MMMM D, YYYY h:mm:ss a'),
      version: VERSION,
    };
    console.log('BODY: ', body);
    var request = {
      method: 'post',
      headers: {
        usuario: usuario.nombreUsuario,
        token: usuario.token,
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    };

    if (estado == 'online') {
      return fetch(
        'https://' +
          AMBIENTE +
          '.senasa.gov.ar/SAS/rest/service/enviarRecorrido',
        request
      )
        .then((response) => {
          console.log('RESPONSE: ', response);
          if (response.status < 300) {
            response.json().then((responseJSON) => {
              console.log('responseJSON: ', responseJSON);
              dispatch(finalizar(tareasCompletadas, request));
            });
          } else {
            dispatch(finalizarPendiente(tareasCompletadas, request));
            // dispatch(isLoading(false));
            dispatch(loginFailed(response.status));
            alert(
              'No se pudo sincronizar, vaya a la pantalla de Recorrido Pendientes.'
            );
          }
        })
        .catch((error) => {
          dispatch(isLoadingRecorrido(false));
        });
    } else {
      dispatch(finalizarPendiente(tareasCompletadas, request));
    }
  };
}

export function postRecorridoPendiente(recorrido) {
  return (dispatch) => {
    dispatch(isLoadingRecorrido(true));
    return fetch(
      'https://' + AMBIENTE + '.senasa.gov.ar/SAS/rest/service/enviarRecorrido',
      recorrido.request
    )
      .then((response) => {
        if (response.status < 300) {
          response.json().then((responseJSON) => {
            dispatch(eliminarRecorridoPendiente(recorrido.dateFinished));
          });
        } else {
          alert(
            'No se pudo sincronizar, vuelva a intentar o comuniquese con el equipo de SAS, muchas gracias.'
          );
        }
      })
      .catch((error) => {
        dispatch(isLoadingRecorrido(false));
      });
  };
}
