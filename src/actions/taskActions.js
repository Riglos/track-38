import { AMBIENTE } from "../constants/version";
import {
  FETCHING_DATA,
  FETCHING_DATA_SUCCESS,
  FETCHING_DATA_FAILURE,
  AGREGAR_TAREA,
  AGREGAR_TAREA_CONTINGENCIA,
  ELIMINAR_TAREA_CONTINGENCIA,
  ELIMINAR_TAREA,
  LISTAR_TAREAS,
  FINALIZAR_TAREA,
  COMENZAR_TAREA,
  AGREGAR_CUVE,
  ELIMINAR_CUVE,
  REINICIAR,
  FINALIZAR_TAREAS_HISTORIAL,
  TAREAS_ATTEMP,
  FINALIZAR_TAREAS,
  SAVE_FORM_TASK,
  SAVE_FORM_TASK_COMPLETED,
  DELETE_TASK,
  ADD_REP,
  DELETE_REP
} from "../constants/api";

import getDataApi from "../api";

// export const agregarRepeticion = (repeticion, original) => {
//   return {
//     type: ADD_REP,
//     repeticion,
//     original
//   };
// };

// export const eliminarRepeticion = repeticion => {
//   return {
//     type: DELETE_REP,
//     repeticion
//   };
// };

export const saveFormularioTask = (form, idTask, idForm, valido) => {
  return {
    type: SAVE_FORM_TASK,
    form,
    idTask,
    idForm,
    valido
  };
};

export const saveFormularioTaskCompleted = (form, idTask, idForm, valido) => {
  return {
    type: SAVE_FORM_TASK_COMPLETED,
    form,
    idTask,
    idForm,
    valido
  };
};

export const selected_tab = tabId => {
  return {
    type: "selected_tab",
    payload: tabId
  };
};

export function isLoading(bool) {
  return {
    type: TAREAS_ATTEMP,
    bool
  };
}

export function borrarFinalizadas(estado) {
  return dispatch =>
    dispatch({
      type: FINALIZAR_TAREAS,
      estado
    });
}

export function borrarFinalizadasHistorial(posicion) {
  return dispatch =>
    dispatch({
      type: FINALIZAR_TAREAS_HISTORIAL,
      posicion
    });
}

export const traerTareas = () => {
  return {
    type: LISTAR_TAREAS
  };
};

export const reiniciar = () => {
  return {
    type: REINICIAR
  };
};

export const agregarTarea = task => {
  return {
    type: AGREGAR_TAREA,
    task
  };
};

export const agregarTareaContingencia = task => {
  return {
    type: AGREGAR_TAREA_CONTINGENCIA,
    task
  };
};

export const eliminarTareaContingencia = task => {
  return {
    type: ELIMINAR_TAREA_CONTINGENCIA,
    task
  };
};

export const eliminarTarea = task => {
  return {
    type: ELIMINAR_TAREA,
    task
  };
};

export const deleteTask = task => {
  return {
    type: DELETE_TASK,
    task
  };
};

export const finalizarTarea = (idTask, location) => {
  return {
    type: FINALIZAR_TAREA,
    idTask,
    location
  };
};

export const agregarCuve = (idTask, cuve) => {
  return {
    type: AGREGAR_CUVE,
    idTask,
    cuve
  };
};

export const eliminarCuve = (idTask, posicion) => {
  return {
    type: ELIMINAR_CUVE,
    idTask,
    posicion
  };
};

export const comenzarTarea = idTask => {
  return {
    type: COMENZAR_TAREA,
    idTask
  };
};

export const getData = () => {
  return {
    type: FETCHING_DATA
  };
};

export const getDataSuccess = data => {
  return {
    type: FETCHING_DATA_SUCCESS,
    data
  };
};

export const getDataFailure = () => {
  return {
    type: FETCHING_DATA_FAILURE
  };
};

export const fetchData = () => {
  return dispatch => {
    dispatch(getData());
    getDataApi()
      .then(([response, json]) => {
        dispatch(getDataSuccess(json));
      })
      .catch(err => console.log(err));
  };
};

export function getTareas(usuario) {
  var request = {
    method: "get",
    headers: {
      usuario: usuario.userData.data.nombreUsuario,
      token: usuario.userData.data.token
    }
  };
  return dispatch => {
    dispatch(getData());
    return fetch(
      "https://" +
        AMBIENTE +
        ".senasa.gov.ar/SAS/rest/service/serviciosAsignados?usuario=" +
        usuario.userData.data.nombreUsuario,
      request
    )
      .then(response => Promise.all([response, response.json()]))
      .then(([response, json]) => {
        dispatch(getDataSuccess(json.data));
      })
      .catch(error => {
        dispatch(getDataFailure(error));
      });
  };
}
