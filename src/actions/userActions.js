import { AMBIENTE } from "../constants/version";
import {
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  LOGOUT,
  LOGIN_ATTEMPT,
  TOOGLE_SWITCH
} from "../constants/api";

export function isLoading(bool) {
  return {
    type: LOGIN_ATTEMPT,
    isLoading: bool
  };
}

export function toggleSwitch(status) {
  return {
    type: TOOGLE_SWITCH,
    enabledTracking: status
  };
}

export function loginSuccess(userData) {
  return {
    type: LOGIN_SUCCESS,
    userData
  };
}

export function loginFailed(error) {
  return {
    type: LOGIN_FAILED,
    error
  };
}

export function logoutSuccess() {
  return {
    type: LOGOUT
  };
}

export function logout() {
  return dispatch => {
    dispatch(logoutSuccess());
  };
}

export function login(email, password, version) {
  console.log("data: ", email);
  console.log("data: ", password);
  console.log("data: ", version);

  return dispatch => {
    dispatch(isLoading(true));
    return fetch(
      "https://" + AMBIENTE + ".senasa.gov.ar/SAS/rest/service/login",
      {
        method: "post",
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          usuario: email,
          password: password,
          version: version
        })
      }
    )
      .then(response => {
        console.log("response: ", response);
        if (response.status == 400) {
          dispatch(isLoading(false));
          alert(
            "La peticion no es correcta, por favor comuniquese con el equipo de SAS"
          );
          dispatch(loginFailed("Error de versión"));
        }
        if (response.status == 503) {
          dispatch(isLoading(false));
          alert("Servidor caido 503");
          dispatch(loginFailed("Servidor caido 503"));
        }
        if (response.status == 500) {
          dispatch(isLoading(false));
          alert("Servidor caido 500");
          dispatch(loginFailed("Servidor caido 500"));
        }
        if (response.status == 401) {
          dispatch(isLoading(false));
          alert("Credenciales invalidas");
          dispatch(loginFailed("Credenciales invalidas"));
        }
        if (response.status == 405) {
          dispatch(isLoading(false));
          alert("La versión no se encuentra actualizada");
          dispatch(loginFailed("Error de versión"));
        }
        if (response.status == 402) {
          dispatch(isLoading(false));
          alert("El usuario no cuenta con los roles necesarios para ingresar.");
          dispatch(
            loginFailed("El usuario no cuenta con los roles necesarios.")
          );
        }

        if (response.status < 300) {
          response.json().then(responseJSON => {
            console.log("responseJSON: ", responseJSON)
            dispatch(isLoading(false));
            if (responseJSON.estado != "OK") {
              alert("Credenciales invalidas", responseJSON);
              dispatch(loginFailed(responseJSON.estado));
            } else {
              dispatch(loginSuccess(responseJSON));
            }
          });
        }
      })
      .catch(error => {
        dispatch(isLoading(false));
        dispatch(loginFailed(error));
      });
  };
}
