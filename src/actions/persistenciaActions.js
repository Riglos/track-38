import {AMBIENTE} from '../constants/version';
import {
  FETCHING_DATA_SERVICIOS,
  FETCHING_DATA_SUCCESS_SERVICIOS,
  FETCHING_DATA_FAILURE_SERVICIOS,
  FETCHING_DATA_SUCCESS_TIPOSENTES,
  FETCHING_DATA_TIPOSENTES,
  FETCHING_DATA_SUCCESS_CONFIGURACIONUSUARIO,
} from '../constants/api';

import getDataApi from '../api';

export const getDataSuccess = (data) => {
  return {
    type: FETCHING_DATA_SUCCESS_SERVICIOS,
    data,
  };
};

export const getDataSuccessTiposEntes = (data) => {
  return {
    type: FETCHING_DATA_SUCCESS_TIPOSENTES,
    data,
  };
};

export const getDataSuccessConfiguracionUsuario = (data) => {
  return {
    type: FETCHING_DATA_SUCCESS_CONFIGURACIONUSUARIO,
    data,
  };
};

export const getDataFailure = () => {
  return {
    type: FETCHING_DATA_FAILURE_SERVICIOS,
  };
};

export const getData = () => {
  return {
    type: FETCHING_DATA_SERVICIOS,
  };
};

export const getDataTiposEntes = () => {
  return {
    type: FETCHING_DATA_TIPOSENTES,
  };
};

export function getServicios(usuario) {
  var request = {
    method: 'get',
    headers: {
      usuario: usuario.userData.data.nombreUsuario,
      token: usuario.userData.data.token,
    },
  };
  return (dispatch) => {
    dispatch(getData());
    return fetch(
      'https://' +
        AMBIENTE +
        '.senasa.gov.ar/SAS/rest/service/tipoServiciosAsignados',
      request
    )
      .then((response) => Promise.all([response, response.json()]))
      .then(([response, json]) => {
        // console.log('json servicios: ', json);
        dispatch(getDataSuccess(json.data));
      })
      .catch((error) => {
        dispatch(getDataFailure(error));
      });
  };
}

export function getTiposEntes(usuario) {
  var request = {
    method: 'get',
    headers: {
      usuario: usuario.userData.data.nombreUsuario,
      token: usuario.userData.data.token,
    },
  };
  return (dispatch) => {
    dispatch(getDataTiposEntes());
    return fetch(
      'https://' + AMBIENTE + '.senasa.gov.ar/SAS/rest/service/tipoEntes',
      request
    )
      .then((response) => Promise.all([response, response.json()]))
      .then(([response, json]) => {
        // console.log('json servicios getTiposEntes: ', json);
        dispatch(getDataSuccessTiposEntes(json.data));
      })
      .catch((error) => {
        dispatch(getDataFailure(error));
      });
  };
}

export function configuracionUsuario(usuario) {
  var request = {
    method: 'get',
    headers: {
      usuario: usuario.userData.data.nombreUsuario,
      token: usuario.userData.data.token,
    },
  };
  return (dispatch) => {
    dispatch(getDataTiposEntes());
    return fetch(
      'https://' +
        AMBIENTE +
        '.senasa.gov.ar/SAS/rest/service/configuracionUsuario',
      request
    )
      .then((response) => Promise.all([response, response.json()]))
      .then(([response, json]) => {
        // console.log('json configuracionUsuario: ', json);
        dispatch(getDataSuccessConfiguracionUsuario(json.data));
      })
      .catch((error) => {
        dispatch(getDataFailure(error));
      });
  };
}
