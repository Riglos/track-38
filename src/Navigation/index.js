import React from "react";
import { Image } from "react-native";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import AuthLoadingScrenn from "../screens/AuthLoadingScreen";
import HomeScreen from "../screens/HomeScreen";
import Login from "../screens/Login";
import Forgot from "../screens/Forgot";
import Browse from "../screens/Browse";
import AddTask from "../screens/AddTask";
import Settings from "../screens/Settings";
import ListadoRecorrido from "../screens/ListadoRecorrido";
import TaskDetail from "../screens/TaskDetail";
import FormularioDetail from "../screens/FormularioDetail";
import FormularioDetailEdit from "../screens/FormularioDetailEdit";
import HistorialRecorrido from "../screens/HistorialRecorridos";
import Formularios from "../screens/Formularios";
import DeleteModal from "../screens/DeleteModal";

import { theme } from "../constants";

const AuthStack = createStackNavigator(
  {
    HomeScreen,
    Login: {
      screen: Login
    },
    Forgot
  },
  {
    swipeEnabled: false,
    animationEnabled: false,
    // lazy: true,
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0
      }
    }),
    defaultNavigationOptions: {
      headerStyle: {
        height: theme.sizes.base * 3,
        backgroundColor: theme.colors.white, // or 'white
        borderBottomColor: "transparent",
        elevation: 0 // for android
      },
      headerBackImage: (
        <Image
          source={require("../../assets/icons/back.png")}
          style={{
            margin: 20
          }}
        />
      ),
      headerBackTitle: null,
      headerLeftContainerStyle: {
        alignItems: "center",
        marginLeft: theme.sizes.base * 0,
        paddingRight: theme.sizes.base
      },
      headerRightContainerStyle: {
        alignItems: "center",
        paddingRight: theme.sizes.base
      }
    }
  }
);

const AppStack = createStackNavigator(
  {
    Browse: {
      screen: Browse,
      navigationOptions: {
        headerLeft: null
      }
    },
    Formularios,
    AddTask,
    HistorialRecorrido,
    ListadoRecorrido,
    TaskDetail,
    FormularioDetail,
    FormularioDetailEdit,
    Settings,
    DeleteModal
  },
  {
    swipeEnabled: false,
    animationEnabled: false,
    // lazy: true,
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0
      }
    }),
    defaultNavigationOptions: {
      headerStyle: {
        height: theme.sizes.base * 3,
        backgroundColor: theme.colors.white, // or 'white
        borderBottomColor: "transparent",
        elevation: 0 // for android
      },
      headerBackImage: (
        <Image
          source={require("../../assets/icons/back.png")}
          style={{
            margin: 20
          }}
        />
      ),
      headerBackTitle: null,
      headerLeftContainerStyle: {
        alignItems: "center",
        marginLeft: theme.sizes.base * 0,
        paddingRight: theme.sizes.base
      },
      headerRightContainerStyle: {
        alignItems: "center",
        paddingRight: theme.sizes.base
      }
    }
  }
);

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScrenn,
      App: AppStack,
      Auth: AuthStack
    },
    {
      initialRouteName: "AuthLoading"
    }
  )
);
