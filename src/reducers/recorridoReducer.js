import {
  RECORRIDO_ATTEMPT,
  RECORRIDO_SUCCESS,
  RECORRIDO_FAILED,
  RECORRIDO_COMENZAR,
  RECORRIDO_ACTUALIZAR,
  RECORRIDO_FINALIZAR,
  ELIMINAR_RECORRIDO_PENDIENTE,
  RECORRIDO_FINALIZAR_PENDIENTE,
  RECORRIDO_GUARDAR,
  RECORRIDO_COMENZAR_SIN_TRACKEO
} from "../constants/api";

const INITIAL_STATE = {
  isLoading: false,
  dateStarted: undefined,
  dateFinished: undefined,
  taskSeleccionadas: [],
  isStarted: false,
  isFinished: false,
  locations: [],
  recorrido: [],
  error: undefined,
  recorridosTerminados: [],
  recorridosPendientes: [],
  startedWithoutTracking: false
};

export default function auth(state = INITIAL_STATE, action) {
  // console.log("action: ", action);
  var newState = { ...state };
  switch (action.type) {
    case RECORRIDO_ATTEMPT:
      return {
        ...state,
        isLoading: action.bool
      };
    case RECORRIDO_SUCCESS:
      return {
        ...state,
        isLoading: false
      };
    case RECORRIDO_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.error
      };
    case RECORRIDO_COMENZAR:
      return {
        ...state,
        dateStarted: new Date(),
        isStarted: true,
        isFinished: false
      };
    case RECORRIDO_COMENZAR_SIN_TRACKEO:
      return {
        ...state,
        dateStarted: new Date(),
        isStarted: true,
        startedWithoutTracking: action.startedWithoutTracking,
        isFinished: false
      };
    case RECORRIDO_ACTUALIZAR:
      return {
        ...state,
        locations: [...state.locations, action.locations]
      };
    case RECORRIDO_FINALIZAR:
      const fecha = new Date();
      const newHistorial = {
        dateFinished: fecha,
        tasks: action.taskSeleccionadas,
        request: action.request
      };
      return {
        ...state,
        isLoading: false,
        dateFinished: fecha,
        isStarted: false,
        isFinished: true,
        recorrido: [...state.recorrido, state.locations],
        locations: [],
        taskSeleccionadas: action.taskSeleccionadas,
        recorridosTerminados: [...state.recorridosTerminados, newHistorial]
      };

    case ELIMINAR_RECORRIDO_PENDIENTE:
      const newStateRecorridoPendientes = state.recorridosPendientes.filter(
        recorrido => recorrido.dateFinished !== action.fecha
      );
      return {
        ...state,
        recorridosPendientes: newStateRecorridoPendientes,
        isLoading: false
      };

    case RECORRIDO_FINALIZAR_PENDIENTE:
      const fechaRecorrido = new Date();
      const newHistorialPendiente = {
        dateFinished: fechaRecorrido,
        tasks: action.taskSeleccionadas,
        request: action.request
      };
      return {
        ...state,
        isLoading: false,
        dateFinished: fechaRecorrido,
        isStarted: false,
        isFinished: true,
        recorrido: [...state.recorrido, state.locations],
        locations: [],
        taskSeleccionadas: action.taskSeleccionadas,
        recorridosPendientes: [
          ...state.recorridosPendientes,
          newHistorialPendiente
        ]
      };
    case RECORRIDO_GUARDAR:
      return {
        ...state,
        recorrido: [...newState.recorrido, action.recorrido]
      };
    default:
      return state;
  }
}
