import {
  FETCHING_DATA_SERVICIOS,
  FETCHING_DATA_SUCCESS_SERVICIOS,
  FETCHING_DATA_FAILURE_SERVICIOS,
  AGREGAR_SERVICIO,
  FETCHING_DATA_SUCCESS_TIPOSENTES,
  FETCHING_DATA_TIPOSENTES,
  FETCHING_DATA_SUCCESS_CONFIGURACIONUSUARIO,
} from '../constants/api';

const initialState = {
  servicios: [],
  tiposEntes: [],
  configuracionUsuario: [],
  error: false,
  data: [],
  isFetching: false,
  error: false,
  isLoading: false,
};

export default persistenciaReducer = (state = initialState, action) => {
  var newState = {...state};
  switch (action.type) {
    case FETCHING_DATA_SERVICIOS:
      return {
        ...state,
        isFetching: true,
      };
    case FETCHING_DATA_SUCCESS_SERVICIOS:
      return {
        ...state,
        servicios: action.data,
        isFetching: false,
      };
    case FETCHING_DATA_SUCCESS_CONFIGURACIONUSUARIO:
      return {
        ...state,
        configuracionUsuario: action.data,
        isFetching: false,
      };
    case FETCHING_DATA_TIPOSENTES:
      return {
        ...state,
        isFetching: true,
      };
    case FETCHING_DATA_SUCCESS_TIPOSENTES:
      return {
        ...state,
        tiposEntes: action.data,
        isFetching: false,
      };
    case FETCHING_DATA_FAILURE_SERVICIOS:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case AGREGAR_SERVICIO:
      // const newStateContingencia = state.tareasContingencia.filter(
      //   tarea => tarea !== action.task
      // );
      return {
        ...newState,
        //   tareasSeleccionadas: [...newState.tareasSeleccionadas, action.task],
        //   tareasPendientes: newStatePendientes,
        //   tareasContingencia: newStateContingencia
      };

    default:
      return state;
  }
};
