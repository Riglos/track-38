import {
  AGREGAR_TAREA,
  ELIMINAR_TAREA,
  COMENZAR_TAREA,
  AGREGAR_TAREA_CONTINGENCIA,
  ELIMINAR_TAREA_CONTINGENCIA,
  FINALIZAR_TAREA,
  FETCHING_DATA,
  FETCHING_DATA_SUCCESS,
  FETCHING_DATA_FAILURE,
  FINALIZAR_TAREAS,
  FINALIZAR_TAREAS_HISTORIAL,
  AGREGAR_CUVE,
  ELIMINAR_CUVE,
  REINICIAR,
  SAVE_FORM_TASK,
  SAVE_FORM_TASK_COMPLETED,
  DELETE_TASK
} from "../constants/api";

const initialState = {
  tareasPendientes: [],
  tareasSeleccionadas: [],
  tareasFinalizadas: [],
  tareasContingencia: [],
  tareasHistorial: [],
  error: false,
  data: [],
  isFetching: false,
  error: false,
  isLoading: false
};

function formulariosFinalizados(task, itemsCompletos, idForm, valido) {
  console.log("valido: formulariosFinalizados", valido)
  // console.log("ITEMS COMPLETOS: ", itemsCompletos)
  const isEnded = task.resultadosFormulario.filter(
    item => item.idFormulario === idForm
  );

  //ESTA ENDEDD
  if (isEnded.length > 0) {
    console.log("isEnded")
    return task.resultadosFormulario.map(item =>
      item.idFormulario === idForm
        ? { ...item, items: itemsCompletos.items, modificado: true, valido }
        : item
    );
  } else {
    //ESTA PENDIENTE
    console.log("entra a pendiente")
    let dePendienteAFinalizado = task.servicio.formularios.filter(
      item => item.id === idForm
    );
    let formulario = dePendienteAFinalizado[0];
    let newItem = {
      ...formulario,
      idFormulario: idForm,
      items: itemsCompletos.items,
      modificado: true,
      valido
    };
    //PARSEAR EL ITEM DENTRO DE NEWITEM
    // console.log("itemsCompletos2: ", itemsCompletos);

    const soloItemCompletos = itemsCompletos.items.filter(item => item.value || item.items);
    // console.log("soloItemCompletos3: ", soloItemCompletos)  
    const soloItemCompletosParseados = soloItemCompletos.map(
      item =>
        (item = {
          idItemFormulario: item.id,
          idItemFormularioOriginal: item.original,
          datos: [
            {
              respuesta: item.items !== undefined ? item.items : item.value,
              codigoRespuesta: null
            }
          ]
        })
    );
    //PARSEAR EL NEWITEM A RESULTADOFORMULARIO
    let newItemParseado = {
      idFormulario: newItem.id,
      items: soloItemCompletosParseados,
      modificacionHabilitada: true,
      modificado: true,
      valido
    };
    console.log("newItemParseado: ",newItemParseado)
    return [...task.resultadosFormulario, newItemParseado];
  }
}

function formulariosFinalizadosCompleted(task, itemsCompletos, idForm, valido) {
  // console.log("formulariosFinalizadosCompleted")
  const soloItemCompletos = itemsCompletos.items.filter(
    item => item.value || item.value === 0 || item.items
  );
  console.log("soloItemCompletos: ", soloItemCompletos)  
  const soloItemCompletosParseados = soloItemCompletos.map(
    item =>
      (item = {
        idItemFormulario: item.id,
        idItemFormularioOriginal: item.original,
        datos: [
          {
            respuesta: item.items !== undefined ? item.items : item.value,
            codigoRespuesta: null
          }
        ]
      })
  );
  //PARSEAR EL NEWITEM A RESULTADOFORMULARIO
  let newItemParseado = {
    idFormulario: idForm,
    items: soloItemCompletosParseados,
    modificacionHabilitada: true,
    modificado: true,
    valido
  };
  const copia = task.resultadosFormulario;
  const newResultadoFormulario = copia.map(item =>
    item.idFormulario === newItemParseado.idFormulario ? newItemParseado : item
  );
  return newResultadoFormulario;
}

function estadoTask(estado, idTask, state, form, idForm, valido) {
  switch (estado) {
    case "pendiente":
      // console.log("state.tareasPendientes: ", state.tareasPendientes)
      // console.log("form: ", form)
      // console.log("idform: ", idForm)
      const resPendientes = state.tareasPendientes.map(task => {
        let newServicio = task.servicio.formularios.map(formulario =>
          formulario.id === idForm ? form : formulario
        );
        return task.id === idTask
          ? {
              ...task,
              servicio: { ...task.servicio, formularios: newServicio },
              // servicio: {...servicio, formularios: formularios.map((formulario) => formulario.id === idForm ? formulario= form : formulario ),
              resultadosFormulario: formulariosFinalizados(
                task,
                form,
                idForm,
                valido
              )
            }
          : task;
      });
      // console.log("respendientes: ", resPendientes)
      return resPendientes;
    case "seleccionada":
      const resSeleccionadas = state.tareasSeleccionadas.map(task => {
        let newServicio = task.servicio.formularios.map(formulario =>
          formulario.id === idForm ? form : formulario
        );
        return task.id === idTask
          ? {
              ...task,
              servicio: { ...task.servicio, formularios: newServicio },
              resultadosFormulario: formulariosFinalizados(
                task,
                form,
                idForm,
                valido
              )
            }
          : task;
      });
      return resSeleccionadas;
    case "finalizada":
      const resFinalizadas = state.tareasFinalizadas.map(task => {
        let newServicio = task.servicio.formularios.map(formulario =>
          formulario.id === idForm ? form : formulario
        );
        return task.id === idTask
          ? {
              ...task,
              servicio: { ...task.servicio, formularios: newServicio },
              resultadosFormulario: formulariosFinalizados(
                task,
                form,
                idForm,
                valido
              )
            }
          : task;
      });
      return resFinalizadas;
    default:
      null;
  }
}

function estadoTaskCompleted(estado, idTask, state, form, idForm, valido) {
  switch (estado) {
    case "pendiente":
      const resPendientes = state.tareasPendientes.map(task => {
        let newServicio = task.servicio.formularios.map(formulario =>
          formulario.id === idForm ? form : formulario
        );
        return task.id === idTask
          ? {
              ...task,
              servicio: { ...task.servicio, formularios: newServicio },
              resultadosFormulario: formulariosFinalizadosCompleted(
                task,
                form,
                idForm,
                valido
              )
            }
          : task;
      });
      return resPendientes;
    case "seleccionada":
      const resSeleccionadas = state.tareasSeleccionadas.map(task => {
        let newServicio = task.servicio.formularios.map(formulario =>
          formulario.id === idForm ? form : formulario
        );
        return task.id === idTask
          ? {
              ...task,
              servicio: { ...task.servicio, formularios: newServicio },
              resultadosFormulario: formulariosFinalizadosCompleted(
                task,
                form,
                idForm,
                valido
              )
            }
          : task;
      });
      return resSeleccionadas;
    case "finalizada":
      const resFinalizadas = state.tareasFinalizadas.map(task => {
        let newServicio = task.servicio.formularios.map(formulario =>
          formulario.id === idForm ? form : formulario
        );
        return task.id === idTask
          ? {
              ...task,
              servicio: { ...task.servicio, formularios: newServicio },
              resultadosFormulario: formulariosFinalizadosCompleted(
                task,
                form,
                idForm,
                valido
              )
            }
          : task;
      });
      return resFinalizadas;
    default:
      null;
  }
}

function compara(nuevo, state) {

  for (var i = nuevo.length - 1; i >= 0; i--) {
    for (var j = 0; j < state.tareasSeleccionadas.length; j++) {
      if (nuevo[i] && nuevo[i].id === state.tareasSeleccionadas[j].id) {
        console.log("reemplaza? tareasSeleccionadas")
        nuevo.splice(i, 1);
      }
    }
  }

  for (var i = nuevo.length - 1; i >= 0; i--) {
    for (var j = 0; j < state.tareasFinalizadas.length; j++) {
      if (nuevo[i] && nuevo[i].id === state.tareasFinalizadas[j].id) {
        console.log("reemplaza? tareasFinalizadas")
        nuevo.splice(i, 1);
      }
    }
  }

  for (var i = nuevo.length - 1; i >= 0; i--) {
    for (var j = 0; j < state.tareasHistorial.length; j++) {
      for (var z = 0; z < state.tareasHistorial[j].length; z++) {
        if (nuevo[i] && nuevo[i].id === state.tareasHistorial[j][z].id) {
          alert("Recuerde sincronizar los recorridos Pendientes.");
          console.log("reemplaza? tareasHistorial")
          nuevo.splice(i, 1);
        }
      }
    }
  }

  for (var i = nuevo.length - 1; i >= 0; i--) {
    for (var j = 0; j < state.tareasPendientes.length; j++) {
      if (nuevo[i] && nuevo[i].id === state.tareasPendientes[j].id) {
        console.log("nuevo[id]", nuevo[id]);
        console.log("state.tareasPendientes[j]", state.tareasPendientes[j]);
        nuevo[id] = state.tareasPendientes[j];
      }
    }
  }

  return nuevo;
}

export default tareasReducer = (state = initialState, action) => {
  var newState = { ...state };
  switch (action.type) {
    case "TAREAS_ATTEMP":
      return {
        ...state,
        isLoading: action.bool
      };
    case FETCHING_DATA:
      return {
        ...state,
        data: [],
        isFetching: true
      };
    case SAVE_FORM_TASK:
      return {
        ...state,
        tareasPendientes: estadoTask(
          "pendiente",
          action.idTask,
          newState,
          action.form,
          action.idForm,
          action.valido
        ),
        tareasSeleccionadas: estadoTask(
          "seleccionada",
          action.idTask,
          newState,
          action.form,
          action.idForm,
          action.valido
        ),
        tareasFinalizadas: estadoTask(
          "finalizada",
          action.idTask,
          newState,
          action.form,
          action.idForm,
          action.valido
        )
      };

    case SAVE_FORM_TASK_COMPLETED:
      return {
        ...state,
        tareasPendientes: estadoTaskCompleted(
          "pendiente",
          action.idTask,
          newState,
          action.form,
          action.idForm,
          action.valido
        ),
        tareasSeleccionadas: estadoTaskCompleted(
          "seleccionada",
          action.idTask,
          newState,
          action.form,
          action.idForm,
          action.valido
        ),
        tareasFinalizadas: estadoTaskCompleted(
          "finalizada",
          action.idTask,
          newState,
          action.form,
          action.idForm,
          action.valido
        )
      };

    case FINALIZAR_TAREAS:
      for (let i = 0; i < newState.tareasSeleccionadas.length; i++) {
        if (newState.tareasSeleccionadas[i].agente.cuit) {
          newState.tareasPendientes = [
            ...state.tareasPendientes,
            newState.tareasSeleccionadas[i]
          ];
        } else {
          newState.tareasContingencia = [
            ...state.tareasContingencia,
            newState.tareasSeleccionadas[i]
          ];
        }
      }

      if (action.estado != "online") {
        newState.tareasHistorial = [
          ...newState.tareasHistorial,
          newState.tareasFinalizadas
        ];
      }
      newState.tareasFinalizadas = [];

      return {
        ...state,
        tareasFinalizadas: newState.tareasFinalizadas,
        isLoading: false,
        tareasHistorial: newState.tareasHistorial,
        tareasContingencia: newState.tareasContingencia,
        tareasPendientes: newState.tareasPendientes,
        tareasSeleccionadas: []
      };

    // case ADD_REP:
    //   console.log("add rep: reducer", action.repeticion);

    //   state.formularioSelected.items.find(
    //     someobject => someobject.id == action.original.id
    //   ).copiaNumero++;
    //   // newItems = [newItemsOriginalUpdated, action.repeticion];
    //   newItems = [...state.formularioSelected.items, action.repeticion];
    //   return {
    //     ...newState,
    //     formularioSelected: {
    //       ...state.formularioSelected,
    //       items: newItems
    //     }
    //   };

    // case DELETE_REP:
    //   console.log("DELETE rep: reducer", action.repeticion);
    //   const index = state.formularioSelected.items.indexOf(action.repeticion);
    //   console.log("DELETE rep: index", index);

    //   let newItems2 = state.formularioSelected.items;
    //   // state.formularioSelected.items.splice(index, 1);
    //   newItems2.splice(index, 1);
    //   return {
    //     ...newState,
    //     formularioSelected: {
    //       ...state.formularioSelected,
    //       items: newItems2
    //     }
    //   };

    case FINALIZAR_TAREAS_HISTORIAL:
      var index = newState.tareasHistorial.indexOf(action.posicion);
      newState.tareasHistorial.splice(index, 1);
      // const newStateHistorial = state.tareasHistorial.slice(action.posicion, 1);
      return {
        ...state,
        isLoading: false,
        tareasHistorial: newState.tareasHistorial
      };
    case FETCHING_DATA_SUCCESS:
      return {
        ...state,
        data: action.data,
        tareasPendientes: compara(action.data, newState),
        // data: JSON.parse(action.data),
        // tareasPendientes: compara(JSON.parse(action.data),newState),
        isFetching: false
      };
    case FETCHING_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    case ELIMINAR_TAREA:
      const newStateSeleccionadas = state.tareasSeleccionadas.filter(
        tarea => tarea !== action.task
      );
      if (action.task.agente.cuit) {
        return {
          ...newState,
          tareasSeleccionadas: newStateSeleccionadas,
          tareasPendientes: [...state.tareasPendientes, action.task]
        };
      } else {
        return {
          ...newState,
          tareasSeleccionadas: newStateSeleccionadas,
          tareasContingencia: [...state.tareasContingencia, action.task]
        };
      }

    case AGREGAR_TAREA:
      const newStatePendientes = state.tareasPendientes.filter(
        tarea => tarea !== action.task
      );

      const newStateContingencia = state.tareasContingencia.filter(
        tarea => tarea !== action.task
      );
      return {
        ...newState,
        tareasSeleccionadas: [...newState.tareasSeleccionadas, action.task],
        tareasPendientes: newStatePendientes,
        tareasContingencia: newStateContingencia
      };
    case COMENZAR_TAREA:
      for (let i = 0; i < newState.tareasSeleccionadas.length; i++) {
        if (newState.tareasSeleccionadas[i].id == action.idTask) {
          newState.tareasSeleccionadas[i].isStarted = new Date();
        }
      }
      return {
        ...newState
        // tareasSeleccionadas: nuevoArreglo
      };

    case FINALIZAR_TAREA:
      for (let i = 0; i < newState.tareasSeleccionadas.length; i++) {
        if (newState.tareasSeleccionadas[i].id == action.idTask) {
          newState.tareasSeleccionadas[i].isFinished = new Date();
          newState.tareasSeleccionadas[i].coordenada = action.location;
          newState.tareasFinalizadas = [
            ...newState.tareasFinalizadas,
            newState.tareasSeleccionadas[i]
          ];
        }
      }

      const newStateSeleccionadas2 = state.tareasSeleccionadas.filter(
        tarea => tarea.id !== action.idTask
      );

      return {
        ...newState,
        tareasSeleccionadas: newStateSeleccionadas2
        // tareasFinalizadas: [...newState.tareasFinalizadas, tareaFinalizada]
      };

    case AGREGAR_CUVE:
      for (let i = 0; i < newState.tareasSeleccionadas.length; i++) {
        if (newState.tareasSeleccionadas[i].id == action.idTask) {
          if (newState.tareasSeleccionadas[i].cuves) {
            newState.tareasSeleccionadas[i].cuves = [
              ...newState.tareasSeleccionadas[i].cuves,
              action.cuve
            ];
          } else {
            newState.tareasSeleccionadas[i].cuves = [action.cuve];
          }
        }
      }

      for (let i = 0; i < newState.tareasFinalizadas.length; i++) {
        if (newState.tareasFinalizadas[i].id == action.idTask) {
          if (newState.tareasFinalizadas[i].cuves) {
            newState.tareasFinalizadas[i].cuves = [
              ...newState.tareasFinalizadas[i].cuves,
              action.cuve
            ];
          } else {
            newState.tareasFinalizadas[i].cuves = [action.cuve];
          }
        }
      }

      for (let i = 0; i < newState.tareasContingencia.length; i++) {
        if (newState.tareasContingencia[i].id == action.idTask) {
          if (newState.tareasContingencia[i].cuves) {
            newState.tareasContingencia[i].cuves = [
              ...newState.tareasContingencia[i].cuves,
              action.cuve
            ];
          } else {
            newState.tareasContingencia[i].cuves = [action.cuve];
          }
        }
      }

      for (let i = 0; i < newState.tareasPendientes.length; i++) {
        if (newState.tareasPendientes[i].id == action.idTask) {
          if (newState.tareasPendientes[i].cuves) {
            newState.tareasPendientes[i].cuves = [
              ...newState.tareasPendientes[i].cuves,
              action.cuve
            ];
          } else {
            newState.tareasPendientes[i].cuves = [action.cuve];
          }
        }
      }

      return {
        ...newState
        // tareasSeleccionadas: newStateSeleccionadas2,
        // tareasFinalizadas: [...newState.tareasFinalizadas, tareaFinalizada]
      };

    case ELIMINAR_TAREA_CONTINGENCIA:
      var index = newState.tareasContingencia.indexOf(action.task);
      newState.tareasContingencia.splice(index, 1);
      // for (let i = 0; i <newState.tareasContingencia.length; i++){
      //   if (newState.tareasContingencia[i].id == action.idTask){
      //     // newState.tareasContingencia[i].cuve = action.cuve;
      //       newState.tareasContingencia[i].cuves.splice(action.posicion, 1);
      //   }
      // }
      return {
        ...newState
      };

    case DELETE_TASK:
      // console.log("pend:", newState.tareasPendientes.indexOf(action.task));
      // console.log("selec: ", newState.tareasSeleccionadas.indexOf(action.task));
      // console.log(
      //   "finalizadas: ",
      //   newState.tareasFinalizadas.indexOf(action.task)
      // );
      var index_pendiente = newState.tareasPendientes.indexOf(action.task);
      var index_seleccionada = newState.tareasSeleccionadas.indexOf(
        action.task
      );
      var index_finalizada = newState.tareasFinalizadas.indexOf(action.task);

      if (index_pendiente > -1) {
        // console.log("elimino pendiente");
        newState.tareasPendientes.splice(index_pendiente, 1);
      } else {
        if (index_seleccionada > -1) {
          // console.log("elimino seleccionada");
          newState.tareasSeleccionadas.splice(index_seleccionada, 1);
        } else {
          // console.log("elimino finalizada");
          newState.tareasFinalizadas.splice(index_finalizada, 1);
        }
      }
      //
      // let newPendientes = newState.tareasPendientes.map(())
      // newState.tareasContingencia.splice(index, 1);
      // for (let i = 0; i <newState.tareasContingencia.length; i++){
      //   if (newState.tareasContingencia[i].id == action.idTask){
      //     // newState.tareasContingencia[i].cuve = action.cuve;
      //       newState.tareasContingencia[i].cuves.splice(action.posicion, 1);
      //   }
      // }
      return {
        ...newState
        // tareasPendientes: newPendientes
      };

    case ELIMINAR_CUVE:
      for (let i = 0; i < newState.tareasSeleccionadas.length; i++) {
        if (newState.tareasSeleccionadas[i].id == action.idTask) {
          newState.tareasSeleccionadas[i].cuves.splice(action.posicion, 1);
        }
      }

      for (let i = 0; i < newState.tareasFinalizadas.length; i++) {
        if (newState.tareasFinalizadas[i].id == action.idTask) {
          newState.tareasFinalizadas[i].cuves.splice(action.posicion, 1);
        }
      }

      for (let i = 0; i < newState.tareasContingencia.length; i++) {
        if (newState.tareasContingencia[i].id == action.idTask) {
          newState.tareasContingencia[i].cuves.splice(action.posicion, 1);
        }
      }

      for (let i = 0; i < newState.tareasPendientes.length; i++) {
        if (newState.tareasPendientes[i].id == action.idTask) {
          newState.tareasPendientes[i].cuves.splice(action.posicion, 1);
        }
      }

      return {
        ...newState
        // tareasSeleccionadas: newStateSeleccionadas2,
        // tareasFinalizadas: [...newState.tareasFinalizadas, tareaFinalizada]
      };

    case AGREGAR_TAREA_CONTINGENCIA:
      return {
        ...newState,
        tareasContingencia: [...newState.tareasContingencia, action.task]
      };

    case REINICIAR:
      newState.tareasSeleccionadas = [];
      newState.tareasPendientes = [];
      newState.data = [];
      newState.tareasContingencia = [];
      newState.tareasFinalizadas = [];
      newState.tareasHistorial = [];
      return {
        ...newState
      };

    default:
      return state;
  }
};
