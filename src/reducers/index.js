import { combineReducers } from "redux";
import tareasReducer from "./tareasReducer";
import formulariosReducer from "./formulariosReducer";
import persistenciaReducer from "./persistenciaReducer";
import userReducer from "./userReducer";
import recorridoReducer from "./recorridoReducer";

export default combineReducers({
  tareas: tareasReducer,
  formularios: formulariosReducer,
  persistencia: persistenciaReducer,
  auth: userReducer,
  recorrido: recorridoReducer
});
