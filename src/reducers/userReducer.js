import {
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  LOGOUT,
  LOGIN_ATTEMPT,
  TOOGLE_SWITCH
} from "../constants/api";

const INITIAL_STATE = {
  isLoggedIn: false,
  isLoading: false,
  userData: {},
  error: undefined,
  enabledTracking: false
};

export default function auth(state = INITIAL_STATE, action) {
  switch (action.type) {
    case LOGIN_ATTEMPT:
      return {
        ...state,
        isLoading: true
      };
    case TOOGLE_SWITCH:
      return {
        ...state,
        enabledTracking: action.enabledTracking
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isLoggedIn: true,
        userData: action.userData,
        error: undefined
      };
    case LOGIN_FAILED:
      return {
        ...state,
        isLoading: false,
        isLoggedIn: false,
        error: action.error
      };
    case LOGOUT:
      return {
        ...state,
        // isLoading: false,
        isLoggedIn: false,
        userData: {}
      };
    default:
      return state;
  }
}
