import {
  COPY_FORM,
  CHANGE_SELECTED_FORM,
  CHANGE_SELECTED_FORM_COMPLETED,
  ADD_REP,
  DELETE_REP,
  AGREGAR_CUVE,
  ELIMINAR_CUVE,
} from '../constants/api';

const initialState = {
  formularioSelected: [],
  formulariosFinalizados: [],
  isFetching: false,
  error: false,
  isLoading: false,
};

function respuestaMultiple(datos1, datos2) {
  let respuesta;
  if (datos2[0] === undefined) {
    respuesta = datos2[0];
  } else {
    if (typeof datos2.respuesta === 'object') {
      respuesta = datos2.respuesta;
    } else {
      if (typeof datos2[0].respuesta === 'object') {
        respuesta = datos2[0].respuesta;
      } else {
        respuesta = datos1.map((item) => ({
          ...item,
          value: datos2.some((item2) => item2.respuesta === item.valor),
        }));
      }
    }
  }
  return respuesta;
}

function respuestaLista(datos2) {
  if (datos2.respuesta) {
    return datos2.respuesta;
  } else {
    return datos2[0] === undefined ? datos2[0] : datos2[0].respuesta;
  }
}

function respuestaGrupo(element, element2) {
  if (datos2.respuesta) {
    return datos2.respuesta;
  } else {
    return datos2[0] === undefined ? datos2[0] : datos2[0].respuesta;
  }
}

function ejecutaSegunTipo(element, element2) {
  switch (element.tipoDato) {
    case 'MULTIPLE':
      respuestaMultiple(element.datos, element2.datos);
      break;
    case 'GRUPO':
      respuestaLista(element2.datos);
      // respuestaGrupo(element, element2);
      break;
    default:
      respuestaLista(element2.datos);
      break;
  }
}
// element.tipoDato === "MULTIPLE"
//   ? respuestaMultiple(element.datos, element2.datos)
//   : element.tipoDato === "GRUPO"
//   ? respuestaGrupo(element, element2)
//   : respuestaLista(element2.datos)
// element.tipoDato === "LISTA"
// ? respuestaLista(element2.datos)
// :
// null
// respuestaGrupo(element, element2)

export default formulariosReducer = (state = initialState, action) => {
  var newState = {...state};
  switch (action.type) {
    case ELIMINAR_CUVE:
      newState.formularioSelected.cuves.splice(action.posicion, 1);
      return {
        ...newState,
      };
    case AGREGAR_CUVE:
      return {
        ...newState,
        // cuves: [...newState.cuves, action.cuve],
        formularioSelected: {
          ...newState.formularioSelected,
          cuves: [...newState.formularioSelected.cuves, action.cuve],
        },
      };
    case COPY_FORM:
      const newCuve = action.form.cuves ? action.form.cuves : [];
      const newObject = {
        ...action.form,
        cuves: newCuve,
      };
      return {
        ...newState,
        formularioSelected: newObject,
        // formularioSelected: action.form,
        // cuves: action.form.cuves ? action.form.cuves : [],
      };

    case ADD_REP:
      // console.log("add rep: reducer", action.repeticion);

      state.formularioSelected.items.find(
        (someobject) => someobject.id == action.original.id
      ).copiaNumero++;
      // newItems = [newItemsOriginalUpdated, action.repeticion];
      newItems = [...state.formularioSelected.items, action.repeticion];
      return {
        ...newState,
        formularioSelected: {
          ...state.formularioSelected,
          items: newItems,
        },
      };

    case DELETE_REP:
      // console.log("DELETE rep: reducer", action.repeticion);
      const index = state.formularioSelected.items.indexOf(action.repeticion);
      // console.log("DELETE rep: index", index);

      let newItems2 = state.formularioSelected.items;
      // state.formularioSelected.items.splice(index, 1);
      newItems2.splice(index, 1);
      return {
        ...newState,
        formularioSelected: {
          ...state.formularioSelected,
          items: newItems2,
        },
      };

    case CHANGE_SELECTED_FORM:
      console.log('CHANGE_SELECTED_FORM ', action);
      return {
        ...newState,
        formularioSelected: {
          ...state.formularioSelected,
          items: action.form,
        },
      };
    case CHANGE_SELECTED_FORM_COMPLETED:
      let newItems = [];
      for (
        let index = 0;
        index < state.formularioSelected.items.length;
        index++
      ) {
        const element = state.formularioSelected.items[index];
        for (let index2 = 0; index2 < action.formCompleted.length; index2++) {
          const element2 = action.formCompleted[index2];
          element.id === element2.idItemFormulario
            ? (element = {
                ...element,
                value:
                  // ejecutaSegunTipo(element, element2)
                  element.tipoDato === 'MULTIPLE'
                    ? respuestaMultiple(element.datos, element2.datos)
                    : // : element.tipoDato === "GRUPO"
                      // ? respuestaGrupo(element, element2)
                      respuestaLista(element2.datos),
                // element.tipoDato === "LISTA"
                // ? respuestaLista(element2.datos)
                // :
                // null
                // respuestaGrupo(element, element2)
              })
            : null;
        }
        newItems = [...newItems, element];
      }
      return {
        ...newState,
        formularioSelected: {
          ...state.formularioSelected,
          items: newItems,
        },
      };

    default:
      return state;
  }
};
