import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Alert, StyleSheet, ScrollView, TextInput, Picker} from 'react-native';
// import Slider from 'react-native-slider';
import {Divider, Button, Block, Text, Switch} from '../components';
import {theme, mocks} from '../constants';
import {agregarTareaContingencia} from '../actions/taskActions';

export default function AddTask({navigation}) {
  const {auth, persistencia} = useSelector((state) => state);
  const dispatch = useDispatch();
  const [detalle, setDetalle] = useState(null);
  const [id, setID] = useState(null);
  const [nombre, setNombre] = useState(null);
  const [listaValue, setlistaValue] = useState(null);
  const [listaValueNoFavoritos, setlistaValueNoFavoritos] = useState(null);
  const [listaValueTiposEntesLocal, setlistaValueTiposEntesLocal] = useState(
    null
  );
  const [listaValueTiposEntes, setlistaValueTiposEntes] = useState(null);
  const [listaValueOficina, setlistaValueOficina] = useState(null);
  const [editingId, seteditingId] = useState(null);
  const [editingNombre, seteditingNombre] = useState(null);
  const [editingDetalle, seteditingDetalle] = useState(null);

  const handleEditDetalle = (text) => {
    setDetalle(text);
  };

  const handleEditId = (text) => {
    setID(text);
  };

  const handleEditNombre = (text) => {
    setNombre(text);
  };

  const handleChangeSelect = (itemValue) => {
    setlistaValue(itemValue);
    // this.props.onChange(itemValue, this.props.item.id);
  };

  const handleChangeSelectNoFavoritos = (itemValue) => {
    setlistaValueNoFavoritos(itemValue);
    // this.props.onChange(itemValue, this.props.item.id);
  };

  const handleChangeSelectOficina = (itemValue) => {
    setlistaValueOficina(itemValue);
    setlistaValueTiposEntesLocal('OFICINA');
  };

  const handleChangeSelectTiposEntes = (itemValue) => {
    console.log('TiposEntes ', itemValue);
    setlistaValueTiposEntes(itemValue);
    setlistaValueTiposEntesLocal('PERSONA');
    // this.props.onChange(itemValue, this.props.item.id);
  };

  const toggleEditDetalle = (name) => () => {
    seteditingDetalle(!editingDetalle ? name : null);
    // this.setState({ editingDetalle: !editingDetalle ? name : null });
  };

  const toggleEditId = (name) => () => {
    seteditingId(!editingId ? name : null);
    // this.setState({ editingDetalle: !editingDetalle ? name : null });
  };

  const toggleEditNombre = (name) => () => {
    seteditingNombre(!editingNombre ? name : null);
    // this.setState({ editingDetalle: !editingDetalle ? name : null });
  };

  function renderEditDetalle(name) {
    // const { detalle, editingDetalle } = this.state;

    if (editingDetalle === name) {
      return (
        <TextInput
          placeholder={'Escriba el detalle'}
          defaultValue={detalle}
          onChangeText={handleEditDetalle}
        />
      );
    }
    return detalle == null ? (
      <Text style={{fontSize: 14}} gray>
        Presione el boton editar
      </Text>
    ) : (
      <Text bold>{detalle}</Text>
    );
  }

  function renderEditId(name) {
    // const { detalle, editingDetalle } = this.state;

    if (editingId === name) {
      return (
        <TextInput
          placeholder={'Escriba el ID'}
          defaultValue={id}
          onChangeText={handleEditId}
        />
      );
    }
    return detalle == null ? (
      <Text style={{fontSize: 14}} gray>
        Presione el boton editar
      </Text>
    ) : (
      <Text bold>{id}</Text>
    );
  }

  function renderEditNombre(name) {
    // const { detalle, editingDetalle } = this.state;

    if (editingNombre === name) {
      return (
        <TextInput
          placeholder={'Escriba el Nombre'}
          defaultValue={nombre}
          onChangeText={handleEditNombre}
        />
      );
    }
    return detalle == null ? (
      <Text style={{fontSize: 14}} gray>
        Presione el boton editar
      </Text>
    ) : (
      <Text bold>{nombre}</Text>
    );
  }

  function renderLista() {
    return (
      <Picker
        selectedValue={listaValue}
        mode="dropdown"
        onValueChange={handleChangeSelect}
      >
        <Picker.Item
          key="seleccione"
          label="Seleccione el servicio"
          value={null}
        />
        {persistencia.configuracionUsuario.tiposServicio.map((dato) => {
          return (
            <Picker.Item key={dato.id} label={dato.nombre} value={dato.id} />
          );
        })}
      </Picker>
    );
  }

  function renderListaNoFavoritos() {
    return (
      <Picker
        selectedValue={listaValueNoFavoritos}
        mode="dropdown"
        onValueChange={handleChangeSelectNoFavoritos}
      >
        <Picker.Item key="seleccione" label="Seleccione" value={null} />
        <Picker.Item key="PERSONA" label="PERSONA" value="PERSONA" />
        <Picker.Item key="OFICINA" label="OFICINA" value="OFICINA" />
        <Picker.Item
          key="ESTABLECIMIENTO"
          label="ESTABLECIMIENTO"
          value="ESTABLECIMIENTO"
        />
      </Picker>
    );
  }

  function renderListaTiposEnte() {
    return (
      <Picker
        selectedValue={listaValueTiposEntes}
        mode="dropdown"
        onValueChange={handleChangeSelectTiposEntes}
      >
        <Picker.Item
          key="seleccione"
          label="Seleccione el Tipo de Ente"
          value={null}
        />
        {persistencia.configuracionUsuario.entesFavoritos.map((dato, i) => {
          return (
            <Picker.Item key={i} label={dato.razonSocial} value={dato.id} />
          );
        })}
      </Picker>
    );
  }

  function renderOficinas() {
    return (
      <Picker
        selectedValue={listaValueOficina}
        mode="dropdown"
        onValueChange={handleChangeSelectOficina}
      >
        <Picker.Item
          key="seleccione"
          label="Seleccione la oficina"
          value={null}
        />
        {persistencia.configuracionUsuario.oficinas.map((dato, i) => {
          return <Picker.Item key={i} label={dato.nombre} value={dato.id} />;
        })}
      </Picker>
    );
  }

  function renderEdit(name) {
    // const { editing } = this.state;
    // const { auth } = this.props;
    if (editingDetalle === name) {
      return (
        <TextInput
          defaultValue={auth.userData.data.nombreUsuario}
          onChangeText={(text) => handleEdit([name], text)}
        />
      );
    }
    return <Text bold>{auth.userData.data[name]}</Text>;
  }

  const agregarContingencia = (navigation, nombreUsuario, detalle) => () => {
    console.log('agregarContingencia');
    if (detalle == null || detalle == '') {
      console.log('entra al if');
      Alert.alert('Alerta', 'Por favor complete el campo detalle');
    } else {
      console.log('entra al else');
      const task = {
        agente: {
          nombreUsuario: nombreUsuario,
        },
        id: -Math.abs(new Date().getTime()),
        detalle,
        servicio: {
          id:
            listaValue !== null
              ? persistencia.servicios.find((item) => item.id === listaValue).id
              : null,
          formularios:
            listaValue !== null
              ? persistencia.servicios.find((item) => item.id === listaValue)
                  .formularios
              : null,
          tiposEntes: listaValueTiposEntes,
          oficina: listaValueOficina,
          idEnte: id,
          nombreEnte: nombre,
        },
        resultadosFormulario: [],
      };
      dispatch(agregarTareaContingencia(task));
      navigation.navigate('Browse');
    }
  };

  return (
    <Block>
      <Block flex={false} row center space="between" style={styles.header}>
        <Text h1 bold>
          Agregar Tarea
        </Text>
      </Block>

      <ScrollView showsVerticalScrollIndicator={false}>
        <Block style={styles.inputs}>
          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray style={{marginBottom: 10}}>
                Usuario
              </Text>
              {renderEdit('nombreUsuario')}
            </Block>
          </Block>
          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray style={{marginBottom: 10}}>
                CUIT
              </Text>
              {renderEdit('cuit')}
            </Block>
          </Block>
          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray style={{marginBottom: 10}}>
                Nombre
              </Text>
              <Text bold>{auth.userData.data.nombre}</Text>
            </Block>
          </Block>
        </Block>

        <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />

        <Block style={styles.inputs}>
          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray bold style={{marginBottom: 10}}>
                Detalle
              </Text>
              {renderEditDetalle('detalle')}
            </Block>
            <Text medium secondary onPress={toggleEditDetalle('detalle')}>
              {editingDetalle === 'detalle' ? 'Guardar' : 'Editar'}
            </Text>
          </Block>
        </Block>

        <Block style={styles.inputs}>
          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray bold style={{marginBottom: 10}}>
                Servicios
              </Text>
              {renderLista()}
            </Block>
          </Block>
        </Block>

        <Block style={styles.inputs}>
          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray bold style={{marginBottom: 10}}>
                Oficina - Origen, donde se genera el servicio
              </Text>
              {renderOficinas()}
            </Block>
          </Block>
        </Block>

        <Block style={styles.inputs}>
          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray bold style={{marginBottom: 10}}>
                Ente favoritos
                {/* Tipos Entes - (PERSONA-OFICINA-ESTABLECIMIENTO) */}
              </Text>
              {renderListaTiposEnte()}
            </Block>
          </Block>
        </Block>
        {listaValueTiposEntes === null && (
          <Block style={styles.inputs}>
            <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
              <Block>
                <Text gray bold style={{marginBottom: 10}}>
                  Seleccione No Favoritos
                </Text>
                {renderListaNoFavoritos()}
              </Block>
            </Block>
          </Block>
        )}

        {listaValueTiposEntes === null && listaValueNoFavoritos !== null && (
          <Block style={styles.inputs}>
            <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
              <Block>
                <Text gray bold style={{marginBottom: 10}}>
                  {listaValueNoFavoritos === 'PERSONA'
                    ? 'CUIT'
                    : listaValueNoFavoritos === 'OFICINA'
                    ? 'ID'
                    : 'N OFICIAL'}
                </Text>
                {renderEditId('detalle')}
              </Block>
              <Text medium secondary onPress={toggleEditId('detalle')}>
                {editingId === 'detalle' ? 'Guardar' : 'Editar'}
              </Text>
            </Block>
          </Block>
        )}

        {listaValueTiposEntes === null && listaValueNoFavoritos !== null && (
          <Block style={styles.inputs}>
            <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
              <Block>
                <Text gray bold style={{marginBottom: 10}}>
                  {listaValueNoFavoritos === 'PERSONA'
                    ? 'Razon Social'
                    : listaValueNoFavoritos === 'OFICINA'
                    ? 'Nombre Oficina'
                    : 'Cuit Propietario'}
                </Text>
                {renderEditNombre('detalle')}
              </Block>
              <Text medium secondary onPress={toggleEditNombre('detalle')}>
                {editingNombre === 'detalle' ? 'Guardar' : 'Editar'}
              </Text>
            </Block>
          </Block>
        )}

        <Block middle margin={[theme.sizes.base, theme.sizes.base * 2]}>
          <Button
            gradient
            onPress={agregarContingencia(
              navigation,
              auth.userData.data.nombreUsuario,
              detalle
            )}
          >
            <Text bold white center>
              Agregar Contingencia
            </Text>
          </Button>
        </Block>
      </ScrollView>
    </Block>
  );
}

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: theme.sizes.base * 2,
  },
  inputs: {
    marginTop: theme.sizes.base * 0.7,
    paddingHorizontal: theme.sizes.base * 2,
  },
  inputRow: {
    alignItems: 'flex-end',
  },
});
