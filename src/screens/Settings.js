import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Image, StyleSheet, ScrollView, Switch} from 'react-native';
import {Button, Block, Text} from '../components';
import {theme, mocks} from '../constants';
import * as actions from '../actions/userActions';
import * as actionsTask from '../actions/taskActions';
// import { connect } from "react-redux";
import {Overlay} from 'react-native-elements';
import AppLoading from '../components/AppLoading';
import * as Location from 'expo-location';
import {VERSION, AMBIENTE} from '../constants/version';
import * as Device from 'expo-device';
import Constants from 'expo-constants';
import {useNetInfo} from '@react-native-community/netinfo';

const LOCATION_UPDATES_TASK = 'location-updates';

export default function Settings({navigation}) {
  const netInfo = useNetInfo();
  console.log('Settings');
  const {auth, recorrido} = useSelector((state) => state);
  const dispatch = useDispatch();

  const [cargando, setcargando] = useState(false);
  const [profile, setlocalProfile] = useState(mocks.profile);
  const [isVisible, setisVisible] = useState(false);
  const [isTracking, setisTracking] = useState(null);
  // const [isConnected, setisConnected] = useState(false);
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     cargando: false,
  //     profile: {},
  //     isVisible: false,
  //     isTracking: null,
  //     isConnected: false
  //   };
  //   this.toggleSwitch = this.toggleSwitch.bind(this);
  //   this.handleLogout = this.handleLogout.bind(this);
  //   this.isVisibleFalse = this.isVisibleFalse.bind(this);
  //   this.cerrarSesion = this.cerrarSesion.bind(this);
  // }

  // componentWillMount = async () => {
  //   const isTracking = await Location.hasStartedLocationUpdatesAsync(
  //     LOCATION_UPDATES_TASK
  //   );
  //   this.setState({ isTracking });
  // };
  useEffect(() => {
    async function fetchLocation() {
      const isTracking = await Location.hasStartedLocationUpdatesAsync(
        LOCATION_UPDATES_TASK
      );
      return isTracking;
    }
    setisTracking(fetchLocation());
  }, []);

  // componentDidMount() {
  //   this.setState({
  //     profile: this.props.profile
  //   });
  useEffect(() => {
    setlocalProfile(profile);
    // const unsubscribe = NetInfo.addEventListener(state => {
    //   if (state.isConnected == true) {
    //     setisConnected(true);
    //   } else {
    //     setisConnected(false);
    //   }
    // });

    // To unsubscribe to these update, just use:
    // unsubscribe();
  }, []);

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   const { navigation } = this.props;
  //   if (prevProps.auth.isLoggedIn !== this.props.auth.isLoggedIn) {
  //     if (this.props.auth.isLoggedIn != true) {
  //       navigation.navigate("Login");
  //     }
  //   }
  // }
  useEffect(() => {
    if (auth.isLoggedIn !== auth.isLoggedIn) {
      if (auth.isLoggedIn != true) {
        navigation.navigate('Login');
      }
    }

    // return setcargando(false);
  }, [auth]);

  // componentWillUnmount() {
  //   this.setState({ cargando: false });
  // }

  const handleLogout = () => {
    setisVisible(true);
    // this.setState({
    //   isVisible: true
    // });
  };

  const toggleSwitch = () => {
    dispatch(actions.toggleSwitch(!auth.enabledTracking));
    if (recorrido.isStarted) {
      //!THIS.PROPS.AUTH.ENABLEDTRACKING is PERMISO HABILITADO
      if (auth.enabledTracking) {
        stopTracking();
      } else {
        startTracking();
      }
    }
  };

  const startTracking = async () => {
    // await Location.startLocationUpdatesAsync(LOCATION_UPDATES_TASK);
    await Location.startLocationUpdatesAsync(LOCATION_UPDATES_TASK, {
      // The best level of accuracy available.
      accuracy: Location.Accuracy.Highest,
      foregroundService: {
        notificationTitle: 'SENASA',
        notificationBody: 'Disfrute su recorrido.',
      },
      // Receive updates only when the location has changed by at least this distance in meters.
      distanceInterval: 10,
      //Minimum time to wait between each update in milliseconds.
      timeInterval: 2000,
    });
  };

  const stopTracking = async () => {
    await Location.stopLocationUpdatesAsync(LOCATION_UPDATES_TASK);
  };

  const isVisibleFalse = () => {
    setisVisible(false);
    // this.setState({ isVisible: false });
  };

  const cerrarSesion = () => {
    setcargando(true);
    setisVisible(false);
    // this.setState({ cargando: true, isVisible: false });
    dispatch(actionsTask.reiniciar());
    dispatch(actions.logout());
    navigation.navigate('Auth');
  };

  const renderEdit = (nombre) => {
    return <Text bold>{auth.userData.data[nombre]}</Text>;
  };

  // render() {
  // const { profile, cargando, isTracking, isConnected } = this.state;

  if (cargando) {
    return <AppLoading />;
  }
  return (
    <Block>
      <Block flex={false} row center space="between" style={styles.header}>
        <Text h1 bold>
          Ajustes
        </Text>
        <Button>
          <Image source={profile.avatar} style={styles.avatar} />
        </Button>
      </Block>

      <ScrollView showsVerticalScrollIndicator={false}>
        {isVisible && (
          <Overlay isVisible={isVisible} onBackdropPress={isVisibleFalse}>
            <Block margin={[theme.sizes.base, theme.sizes.base * 2]}>
              <Text style={{marginBottom: 15}}>
                Tenga en cuenta que al desloguearse perdera todos las tareas de
                contingencia y las tareas que se encuentren en estado
                seleccionadas y finalizadas al volver a loguearse apareceran en
                estado pendiente. Por favor finalice el recorrido.
              </Text>

              <Button gradient onPress={cerrarSesion}>
                <Text bold white center>
                  Cerrar sesion
                </Text>
              </Button>
            </Block>
          </Overlay>
        )}

        <Block style={styles.inputs}>
          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray style={{marginBottom: 10}}>
                Usuario
              </Text>
              {renderEdit('nombreUsuario')}
            </Block>
          </Block>
          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray style={{marginBottom: 10}}>
                CUIT
              </Text>
              {renderEdit('cuit')}
            </Block>
          </Block>
          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray style={{marginBottom: 10}}>
                Nombre
              </Text>
              {renderEdit('nombre')}
            </Block>
          </Block>
          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray style={{marginBottom: 10}}>
                Version
              </Text>
              <Text bold>
                {VERSION} - {AMBIENTE}
              </Text>
            </Block>
          </Block>

          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Text gray style={{marginBottom: 0}}>
              Trackear
            </Text>
            <Switch
              trackColor={{false: '#767577', true: '#81b0ff'}}
              thumbColor={auth.enabledTracking ? '#f5dd4b' : '#f4f3f4'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={auth.enabledTracking}
            />
          </Block>
          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray style={{marginBottom: 10}}>
                Trackeando
              </Text>
              <Text bold>{isTracking == true ? 'SI' : 'NO'}</Text>
            </Block>
          </Block>

          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray style={{marginBottom: 10}}>
                Conexión
              </Text>
              <Text bold>{netInfo.isConnected == true ? 'SI' : 'NO'}</Text>
            </Block>
          </Block>

          <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
            <Block>
              <Text gray style={{marginBottom: 10}}>
                ID Dispositivo
              </Text>
              <Text bold>{Constants.installationId}</Text>
            </Block>
          </Block>
        </Block>

        <Block middle margin={[theme.sizes.base, theme.sizes.base * 2]}>
          <Button gradient onPress={handleLogout}>
            <Text bold white center>
              Cerrar sesion
            </Text>
          </Button>
        </Block>
      </ScrollView>
    </Block>
  );
}

// Settings.defaultProps = {
//   profile: mocks.profile
// };

// const mapStateToProps = state => ({
//   auth: state.auth,
//   recorrido: state.recorrido
// });

// const mapDispatchToProps = dispatch => ({
//   logout: () => dispatch(actions.logout()),
//   reiniciar: () => dispatch(actionsTask.reiniciar()),
//   toggleSwitch: status => dispatch(actions.toggleSwitch(status))
// });
// export default connect(mapStateToProps, mapDispatchToProps)(Settings);

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: theme.sizes.base * 2,
  },
  avatar: {
    height: theme.sizes.base * 2.2,
    width: theme.sizes.base * 2.2,
  },
  inputs: {
    marginTop: theme.sizes.base * 0.7,
    paddingHorizontal: theme.sizes.base * 2,
  },
  inputRow: {
    alignItems: 'flex-end',
  },
});
