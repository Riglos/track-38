import React, { Component } from "react";
import { Image, StyleSheet } from "react-native";
import { Block } from "../components";
import { connect } from "react-redux";

class AuthLoadingScreen extends Component {
  componentDidMount() {
    this.loadApp();
  }
  loadApp = async () => {
    this.props.navigation.navigate(this.props.auth.isLoggedIn ? "App" : "Auth");
  };

  render() {
    return (
      <Block style={styles.container}>
        <Image
          source={require("../../assets/juanri.gif")}
          style={{ width: 300, height: 300 }}
        />
      </Block>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, null)(AuthLoadingScreen);
// export default AuthLoadingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});
