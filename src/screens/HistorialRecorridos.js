// import React, { Component } from "react";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Alert, View, ScrollView, StyleSheet, Dimensions } from "react-native";
import NetInfo from "@react-native-community/netinfo";
import _ from "lodash";
import { Text, Icon, Rating, Overlay, Card } from "react-native-elements";
import { Button, Text as TextComponent } from "../components";
import ListItem from "rne-listitem-web";
import { connect } from "react-redux";

import {
  postRecorridoPendiente,
  isLoadingRecorrido
} from "../actions/recorridoActions";
import AppLoading from "../components/AppLoading";
import { borrarFinalizadasHistorial } from "../actions/taskActions";
import { useNetInfo } from "@react-native-community/netinfo";

const { height } = Dimensions.get("window");

export default function HistorialRecorrido({ navigation }) {
  const netInfo = useNetInfo();
  const { recorrido } = useSelector(state => state);
  const dispatch = useDispatch();

  const [basic, setbasic] = useState(true);
  const [historial, sethistorial] = useState([]);
  const [visible, setvisible] = useState(false);
  const [recorridoSeleccionado, setrecorridoSeleccionado] = useState("");
  const [search, setsearch] = useState("");
  const [isLoading, setisLoading] = useState(false);

  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     basic: true,
  //     historial: [],
  //     visible: false,
  //     recorridoSeleccionado: "",
  //     search: "",
  //     isLoading: false
  //     // recorrido: []
  //   };
  // }

  // componentDidMount() {
  //   // this.setState({
  //   //   recorrido: this.props.recorrido.recorridosPendientes
  //   // });

  //   // NetInfo.addEventListener(
  //   //   "connectionChange",
  //   //   this._handleConnectivityChange
  //   // );

  //   NetInfo.fetch().then(isConnected => {
  //     if (isConnected.isConnected == true) {
  //       this.setState({ connection_Status: "Online" });
  //     } else {
  //       this.setState({ connection_Status: "Offline" });
  //     }
  //   });
  // }

  // componentWillUnmount() {
  //   const unsubscribe = NetInfo.addEventListener(
  //     "connectionChange",
  //     this._handleConnectivityChange
  //   );
  //   unsubscribe();
  // }

  // _handleConnectivityChange = isConnected => {
  //   if (isConnected == true) {
  //     this.setState({ connection_Status: "Online" });
  //   } else {
  //     this.setState({ connection_Status: "Offline" });
  //   }
  // };

  const toggleTracking = async () => {
    if (netInfo.isConnected) {
      // this.setState({ isLoading: true });
      setisLoading(true);
      dispatch(isLoadingRecorrido(true));
      finaliza();
    } else {
      Alert.alert(
        "Alerta",
        "Por favor vuelta a internet cuando se encuentre con conexion a Internet."
      );
    }
  };

  // componentWillUnmount() {
  //   this.setState({ isLoading: false });
  // }

  const finaliza = () => {
    // const { navigation } = this.props;
    dispatch(postRecorridoPendiente(recorridoSeleccionado));
    dispatch(borrarFinalizadasHistorial(recorridoSeleccionado));
    navigation.navigate("Browse");
    // console.log("post await ", postrecorrido);
    // // await Location.stopLocationUpdatesAsync(LOCATION_UPDATES_TASK);
    // const finalizadas = await ;
    // console.log("finalziadas: ", finalizadas);
  };

  const log = recorridoSeleccionado => {
    setrecorridoSeleccionado(recorridoSeleccionado);
    setvisible(!visible);
    // this.setState({
    //   recorridoSeleccionado,
    //   visible: !this.state.visible
    // });
  };

  const log2 = () => {
    setvisible(!visible);
    // this.setState({
    //   visible: !this.state.visible
    // });
  };

  // render() {
  // const { visible, recorridoSeleccionado } = this.state;

  if (recorrido.recorridosPendientes.isLoading || isLoading) {
    return <AppLoading />;
  }

  if (recorrido.recorridosPendientes.length == 0) {
    return (
      <View
        style={{
          flex: 1,
          marginTop: 50
          // justifyContent: "center"
        }}
      >
        <Card title="NOTA">
          <Text style={{ marginBottom: 10 }}>
            No hay recorridos en estado pendiente.
          </Text>
        </Card>
      </View>
    );
  }
  return (
    <ScrollView>
      <Overlay
        isVisible={visible}
        onBackdropPress={() => log2()}
        height={height / 3}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            justifyContent: "center"
          }}
        >
          <Text>
            {new Date(recorridoSeleccionado.dateFinished).toLocaleDateString()}
          </Text>
          <Text>
            Cantidad de Tareas:{" "}
            {recorridoSeleccionado != ""
              ? recorridoSeleccionado.tasks.length
              : null}
          </Text>

          <Button gradient onPress={toggleTracking}>
            <TextComponent bold white center>
              Sincronizar
            </TextComponent>
          </Button>
        </View>
      </Overlay>
      <View style={styles.headerContainer}>
        <Icon color="white" name="car" type="material-community" size={62} />
        <Text style={styles.heading}>Recorridos</Text>
      </View>
      {/* <View>
          <SearchBar
            lightTheme
            placeholder="Busque por..."
            onChangeText={search => setSearch(search)}
            value={search}
          />
        </View> */}
      <View style={styles.list}>
        {recorrido.recorridosPendientes.map((l, i) => (
          <ListItem
            leftIcon={{
              name: "car",
              type: "material-community",
              color: "blue"
            }}
            key={i}
            onPress={() => log(l)}
            title={new Date(l.dateFinished).toLocaleDateString()}
            subtitle={
              <View style={styles.subtitleView}>
                <Rating
                  imageSize={20}
                  readonly
                  startingValue={l.tasks.length}
                  // style={{ styles.rating }}
                />
                <Text style={styles.ratingText}>
                  Tareas {l.tasks.length.toString()}{" "}
                </Text>
              </View>
            }
            chevron
            bottomDivider
          />
        ))}
      </View>
    </ScrollView>
  );
}

// const mapStateToProps = state => {
//   return {
//     recorrido: state.recorrido
//     // usuario: state.auth
//   };
// };

// const mapDispatchToProps = dispatch => {
//   return {
//     postRecorridoPendiente: recorridoSeleccionado =>
//       dispatch(postRecorridoPendiente(recorridoSeleccionado)),
//     borrarFinalizadasHistorial: recorridoSeleccionado =>
//       dispatch(borrarFinalizadasHistorial(recorridoSeleccionado)),
//     isLoadingRecorrido: bool => dispatch(isLoadingRecorrido(bool))
//   };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(HistorialRecorrido);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  list: {
    marginTop: 0,
    borderTopWidth: 1,
    borderColor: "#cbd2d9",
    backgroundColor: "#fff"
  },
  headerContainer: {
    justifyContent: "center",
    alignItems: "center",
    padding: 40,
    backgroundColor: "#0083ff"
  },
  heading: {
    color: "white",
    marginTop: 10,
    fontSize: 22
  },
  fonts: {
    marginBottom: 8
  },
  user: {
    flexDirection: "row",
    marginBottom: 6
  },
  image: {
    width: 30,
    height: 30,
    marginRight: 10
  },
  name: {
    fontSize: 16,
    marginTop: 5
  },
  social: {
    flexDirection: "row",
    justifyContent: "center"
  },
  subtitleView: {
    flexDirection: "row",
    paddingLeft: 10,
    paddingTop: 5
  },
  ratingImage: {
    height: 19.21,
    width: 100
  },
  ratingText: {
    paddingLeft: 10,
    color: "grey"
  }
});

// export default HistorialRecorrido;
