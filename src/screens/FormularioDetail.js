import React, { Component } from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  ScrollView,
  Alert,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { Icon } from "react-native-elements";
import { Button, Block, Text, Formulario } from "../components";
import { theme } from "../constants";
import * as formActions from "../actions/formActions";
import { agregarCuve, eliminarCuve } from "../actions/formActions";
import { BarCodeScanner } from "expo-barcode-scanner";
import { taskActions } from "../actions";
import * as Permissions from "expo-permissions";

class FormularioDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      escaneado: null,
      hasCameraPermission: null,
      scanned: false,
      activarCodigoBarra: false,
      editingCuve: null,
      cuve: null,
      // formulario: this.props.navigation.getParam('formulario', 'NO-FORMULARIO'),
      formulario: this.props.formularios.formularioSelected,
      idTask: this.props.navigation.getParam("idTask", "NO-idTask"),
    };
    this.handleAgregarCuve = this.handleAgregarCuve.bind(this);
    this.handleEliminarCuve = this.handleEliminarCuve.bind(this);
    this.volver = this.volver.bind(this);
    this.guardar = this.guardar.bind(this);
    this.guardar2 = this.guardar2.bind(this);
    this.handleModify = this.handleModify.bind(this);
  }

  componentWillMount() {
    // console.log("tarea: ", this.state.formulario);
    this.props.copyFormularioSelected(
      this.props.navigation.getParam("formulario", "NO-FORMULARIO")
    );
  }

  async componentDidMount() {
    this.getPermissionsAsync();
  }

  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === "granted" });
  };

  handleEditCuve(text) {
    this.setState({ cuve: text });
  }

  handleAgregarCuve = () => {
    const { formulario, cuve } = this.state;
    const { formularioSelected } = this.props.formularios;
    if (cuve == null || cuve == "") {
      Alert.alert("Alerta", "Complete el CUVE.");
    } else {
      this.props.agregarCuve(formularioSelected.id, cuve);
      this.setState({ cuve: null });
    }
  };

  handleEliminarCuve = (posicion) => () => {
    const { formulario } = this.state;
    this.props.eliminarCuve(formulario.id, posicion);
  };

  volver() {
    this.props.navigation.goBack();
  }

  // guardar() {
  //   let valido = true;
  //   //VALIDACION MULTIPLE
  //   for (
  //     let index = 0;
  //     index < this.props.formularios.formularioSelected.items.length;
  //     index++
  //   ) {
  //     if (
  //       this.props.formularios.formularioSelected.items[index].idTipoDato === 5
  //     ) {
  //       valido = false;
  //       if (
  //         this.props.formularios.formularioSelected.items[index].obligatorio
  //       ) {
  //         if (this.props.formularios.formularioSelected.items[index].value) {
  //           this.props.formularios.formularioSelected.items[index].value.map(
  //             item => {
  //               item.value === true ? (valido = true) : null;
  //             }
  //           );
  //         }
  //       }
  //     }
  //   }

  //   //VALIDACION DE TODOS
  //   this.props.formularios.formularioSelected.items.map(item => {
  //     item.obligatorio ? (item.value ? null : (valido = false)) : null;
  //   });

  //   if (valido) {
  //     this.props.navigation.navigate("Browse");
  //     this.props.saveFormularioTask(
  //       this.props.formularios.formularioSelected,
  //       this.state.idTask,
  //       this.state.formulario.id
  //     );
  //   } else {
  //     Alert.alert(
  //       "Atención",
  //       "Revise los campos obligatorios antes de guardar.",
  //       [{ text: "Aceptar", onPress: () => console.log("OK Pressed") }],
  //       { cancelable: true }
  //     );
  //   }

  //   // console.log("idTarea: ", this.state.idTask);
  //   // console.log(
  //   //   "formularioSelected: ",
  //   //   this.props.formularios.formularioSelected
  //   // );
  //   //VALIDACION DE FORMULARIO
  //   // console.log("formulario nuevo: ", this.state.formulario);
  // }

  guardar() {
    let valido = true;
    //VALIDACION MULTIPLE
    for (
      let index = 0;
      index < this.props.formularios.formularioSelected.items.length;
      index++
    ) {
      if (
        this.props.formularios.formularioSelected.items[index].idTipoDato === 5
      ) {
        valido = false;
        if (
          this.props.formularios.formularioSelected.items[index].obligatorio
        ) {
          if (this.props.formularios.formularioSelected.items[index].value) {
            this.props.formularios.formularioSelected.items[index].value.map(
              (item) => {
                item.value === true ? (valido = true) : null;
              }
            );
          }
        }
      }
    }

    //VALIDACION DE TODOS
    this.props.formularios.formularioSelected.items.map((item) => {
      item.obligatorio ? (item.value ? null : (valido = false)) : null;
    });

    // console.log("this.state.formulario: ", this.state.formulario);
    // console.log("this.props.formularios.formularioSelected: ", this.props.formularios.formularioSelected);

    this.props.saveFormularioTask(
      this.props.formularios.formularioSelected,
      this.state.idTask,
      this.state.formulario.id,
      valido
    );
    if (valido) {
      // console.log("CERRADO");
      Alert.alert(
        "Guardado Cerrado",
        "Todos los campos obligatorios están cargados.",
        [
          {
            text: "Aceptar",
            onPress: () => this.props.navigation.navigate("Browse"),
          },
        ],
        { cancelable: true }
      );
    } else {
      // console.log("PARCIAL");
      Alert.alert(
        "Guardado Parcial",
        "Faltan campos obligatorios.",
        [
          {
            text: "Aceptar",
            onPress: () => this.props.navigation.navigate("Browse"),
          },
        ],
        { cancelable: true }
      );
    }
    // this.props.navigation.navigate("Browse");
  }

  guardar2() {
    // console.log("entra al guardar 2 aca?")
    let valido = true;
    //VALIDACION MULTIPLE
    for (
      let index = 0;
      index < this.props.formularios.formularioSelected.items.length;
      index++
    ) {
      if (
        this.props.formularios.formularioSelected.items[index].idTipoDato === 5
      ) {
        valido = false;
        if (
          this.props.formularios.formularioSelected.items[index].obligatorio
        ) {
          if (this.props.formularios.formularioSelected.items[index].value) {
            this.props.formularios.formularioSelected.items[index].value.map(
              (item) => {
                item.value === true ? (valido = true) : null;
              }
            );
          }
        }
      }
    }

    //VALIDACION DE TODOS
    this.props.formularios.formularioSelected.items.map((item) => {
      item.obligatorio ? (item.value ? null : (valido = false)) : null;
    });

    this.props.saveFormularioTask(
      this.props.formularios.formularioSelected,
      this.state.idTask,
      this.state.formulario.id,
      valido
    );
  }

  handleModify(e, itemId) {
    let newForm = this.props.formularios.formularioSelected.items.map((item) =>
      item.id === itemId
        ? item.tipoDato === "GRUPO"
          ? { ...item, items: e, value: e }
          : { ...item, value: e }
        : item
    );

    // console.log("newForm: ", newForm);

    this.props.changeSelectedFormulario(newForm);
    this.setState({
      formulario: {
        ...this.props.formularios.formularioSelected,
        items: newForm,
      },
      // formulario: { ...this.state.formulario, items: newForm }
    });
  }

  renderCuves() {
    // const {formulario} = this.state;
    const { formularioSelected } = this.props.formularios;
    console.log("FORM CUV: ", formularioSelected);

    if (formularioSelected.cuves == undefined) {
      return null;
    }
    return (
      <Block style={styles.datosTarea}>
        {formularioSelected.cuves.map((cuve, i) => {
          return (
            <Block
              key={i}
              row
              center
              space="between"
              style={{ marginBottom: theme.sizes.base * 2 }}
            >
              <Text gray>CUVE {i + 1}</Text>
              <Text>{cuve}</Text>
              <TouchableOpacity onPress={this.handleEliminarCuve(i)}>
                <Icon
                  name="remove-circle-outline"
                  type="material"
                  color={theme.colors.gray}
                />
              </TouchableOpacity>
            </Block>
          );
        })}
      </Block>
    );
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true });
    this.setState({ escaneado: data });
    Alert.alert(
      "Alerta",
      `Codigo de barra con tipo ${type} y valor ${data} se escaneo!`
    );
  };

  aceptarCuve() {
    Alert.alert("Alerta", "aceptar cuve");
    this.setState({ scanned: false, activarCodigoBarra: false });
    this.props.agregarCuve(this.state.tarea.id, this.state.escaneado);
  }

  render() {
    const {
      formulario,
      cuve,
      activarCodigoBarra,
      hasCameraPermission,
      scanned,
    } = this.state;
    if (activarCodigoBarra) {
      if (hasCameraPermission === null) {
        return <Text>Solicitando permiso para la camara</Text>;
      }
      if (hasCameraPermission === false) {
        return <Text>No posee acceso a la camará</Text>;
      }

      return (
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "flex-end",
          }}
        >
          <BarCodeScanner
            onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
            style={StyleSheet.absoluteFillObject}
          >
            <View style={styles.layerTop} />
            <View style={styles.layerCenter}>
              <View style={styles.layerLeft} />
              <View style={styles.focused} />
              <View style={styles.layerRight} />
            </View>
            <View style={styles.layerBottom} />
          </BarCodeScanner>

          {scanned && (
            <Button
              center
              gradient
              onPress={() => this.setState({ scanned: false })}
            >
              <Text center white>
                Presiona para escanear de nuevo
              </Text>
            </Button>
          )}
          {scanned && (
            <Button center gradient onPress={() => this.aceptarCuve()}>
              <Text center white>
                Aceptar Cuve
              </Text>
            </Button>
          )}
          {scanned && (
            <Button
              center
              gradient
              onPress={() =>
                this.setState({ scanned: false, activarCodigoBarra: false })
              }
            >
              <Text center white>
                Cancelar Cuve
              </Text>
            </Button>
          )}
        </View>
      );
    }

    return (
      <Block>
        <Block flex={false} row center space="between" style={styles.header}>
          <Text h1 bold>
            Formulario
          </Text>
        </Block>

        <ScrollView showsVerticalScrollIndicator={false}>
          <Formulario
            // items={this.state.formulario.items}
            items={this.state.formulario.items}
            handleModify={this.handleModify}
            guardar2={this.guardar2}
          />

          <Block middle margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Button gradient onPress={this.guardar}>
              <Text bold white center>
                Guardar
              </Text>
            </Button>
          </Block>
          {formulario.permiteIngresarCuve && (
            <Block>
              <Block style={styles.inputs}>
                <Block
                  row
                  space="between"
                  margin={[10, 0]}
                  style={styles.inputRow}
                >
                  <Block>
                    <Text gray style={{ marginBottom: 10 }}>
                      CUVES
                    </Text>
                    <TextInput
                      placeholder="Ingrese CUVE"
                      defaultValue={cuve}
                      onChangeText={(text) => this.handleEditCuve(text)}
                    />
                  </Block>
                  <TouchableOpacity onPress={this.handleAgregarCuve}>
                    <Icon
                      name="add-circle-outline"
                      type="material"
                      color={theme.colors.gray}
                    />
                  </TouchableOpacity>
                </Block>
              </Block>
              {this.renderCuves()}
              <Block middle margin={[theme.sizes.base, theme.sizes.base * 2]}>
                <Button
                  gradient
                  onPress={() => this.setState({ activarCodigoBarra: true })}
                >
                  <Text bold white center>
                    Ingrese CUVE con código de barras.
                  </Text>
                </Button>
              </Block>
            </Block>
          )}

          <Block middle margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Button gradient onPress={this.volver}>
              <Text bold white center>
                Volver
              </Text>
            </Button>
          </Block>
        </ScrollView>
      </Block>
    );
  }
}

const mapStateToProps = (state) => ({
  formularios: state.formularios,
});

const mapDispatchToProps = (dispatch) => ({
  copyFormularioSelected: (form) =>
    dispatch(formActions.copyFormularioSelected(form)),
  changeSelectedFormulario: (form) =>
    dispatch(formActions.changeSelectedFormulario(form)),
  saveFormularioTask: (form, idTask, idForm, valido) =>
    dispatch(taskActions.saveFormularioTask(form, idTask, idForm, valido)),
  agregarCuve: (idForm, cuve) => dispatch(agregarCuve(idForm, cuve)),
  eliminarCuve: (idForm, posicion) => dispatch(eliminarCuve(idForm, posicion)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormularioDetail);

const opacity = "rgba(0, 0, 0, .6)";
const styles = StyleSheet.create({
  layerTop: {
    flex: 2,
    backgroundColor: opacity,
  },
  layerCenter: {
    flex: 1,
    flexDirection: "row",
  },
  layerLeft: {
    flex: 1,
    backgroundColor: opacity,
  },
  focused: {
    flex: 10,
  },
  layerRight: {
    flex: 1,
    backgroundColor: opacity,
  },
  layerBottom: {
    flex: 2,
    backgroundColor: opacity,
  },
  header: {
    paddingHorizontal: theme.sizes.base * 2,
  },
  datosTarea: {
    paddingHorizontal: theme.sizes.base * 2,
  },
  datosRecorrido: {
    paddingHorizontal: theme.sizes.base * 2,
  },
  inputRow: {
    alignItems: "flex-end",
  },
  datosAgente: {
    marginTop: theme.sizes.base * 2,
    paddingHorizontal: theme.sizes.base * 2,
  },
  inputs: {
    paddingHorizontal: theme.sizes.base * 2,
  },
});
