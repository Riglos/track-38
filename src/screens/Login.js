import React, { useState, useEffect } from "react";
import {
  ActivityIndicator,
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  Platform,
  Alert
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
// import NetInfo from "@react-native-community/netinfo";
import { Button, Block, Input, Text } from "../components";
import { theme } from "../constants";
import AppLoading from "../components/AppLoading";
// import { connect } from "react-redux";
import * as actions from "../actions/userActions";
import { VERSION } from "../constants/version";
import { useNetInfo } from "@react-native-community/netinfo";

const VALID_EMAIL = null;
const VALID_PASSWORD = null;

export default function Login({ navigation }) {
  const dispatch = useDispatch();
  const netInfo = useNetInfo();
  const { isLoggedIn, isLoading, userData, error } = useSelector(
    state => state.auth
  );
  // isLoggedIn: state.auth.isLoggedIn,
  // isLoading: state.auth.isLoading,
  // userData: state.auth.userData,
  // error: state.auth.error

  const [email, setemail] = useState(VALID_EMAIL);
  const [password, setpassword] = useState(VALID_PASSWORD);
  const [errors, setErrors] = useState([]);
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     email: VALID_EMAIL,
  //     password: VALID_PASSWORD,
  //     errors: [],
  //     connection_Status: ""
  //   };
  //   this.handleLogin = this.handleLogin.bind(this);
  //   this.irForgot = this.irForgot.bind(this);
  // }

  // componentDidMount() {
  //   // NetInfo.addEventListener(
  //   //   "connectionChange",
  //   //   this._handleConnectivityChange
  //   // );

  //   NetInfo.fetch().then(isConnected => {
  //     console.log("isConnect componentDidMount: ", isConnected.isConnected);
  //     if (isConnected.isConnected == true) {
  //       this.setState({ connection_Status: "Online" });
  //     } else {
  //       this.setState({ connection_Status: "Offline" });
  //     }
  //   });
  // }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   const { navigation, isLoggedIn } = this.props;
  //   if (isLoggedIn) {
  //     navigation.navigate("Browse");
  //   }
  // }
  useEffect(() => {
    if (isLoggedIn) {
      navigation.navigate("Browse");
    }
  }, []);

  useEffect(() => {
    if (isLoggedIn) {
      navigation.navigate("Browse");
    }
  }, [isLoggedIn]);

  // componentWillUnmount() {
  //   const unsubscribe = NetInfo.addEventListener(
  //     "connectionChange",
  //     this._handleConnectivityChange
  //   );
  //   unsubscribe();
  // }

  // _handleConnectivityChange = isConnected => {
  //   console.log("_handleConnectivityChange: ", isConnected);
  //   if (isConnected == true) {
  //     this.setState({ connection_Status: "Online" });
  //   } else {
  //     this.setState({ connection_Status: "Offline" });
  //   }
  // };

  const isForgot = () => {
    navigation.navigate("Forgot");
  };

  const login = () => {
    console.log("entra aca?aaaa netinfo", netInfo.isConnected);
    if (netInfo.isConnected) {
      dispatch(actions.login(email, password, VERSION));
    } else {
      Alert.alert(
        "Sin conexión",
        "Por favor verifique su conexión.",
        [
          {
            text: "OK",
            onPress: () => {
              null;
            }
          }
        ],
        { cancelable: false }
      );
    }
  };

  const handleLogin = () => {
    // const { email, password } = this.state;
    const localerrors = [];

    Keyboard.dismiss();

    if (email == null || email == "") {
      localerrors.push("email");
    }
    if (password == null || password == "") {
      localerrors.push("password");
    }

    // this.setState({ errors });
    setErrors(localerrors);

    if (!localerrors.length) {
      login();
    }
  };

  // render() {
  // const { isLoading } = this.props;
  // const { loading, errors } = this.state;
  const hasErrors = key => (errors.includes(key) ? styles.hasErrors : null);

  if (isLoading) {
    return <AppLoading />;
  }
  return (
    <KeyboardAvoidingView
      style={styles.login}
      behavior={Platform.OS === "ios" ? "padding" : null}
    >
      <Block padding={[0, theme.sizes.base * 2]}>
        <Text h1 bold>
          Iniciar sesión
        </Text>
        <Block middle>
          <Input
            label="Usuario"
            placeholder={"Complete el campo"}
            error={hasErrors("email")}
            style={[styles.input, hasErrors("email")]}
            defaultValue={email}
            onChangeText={text => setemail(text.trim())}
          />
          <Input
            secure
            label="Password"
            placeholder={"Complete el campo"}
            error={hasErrors("password")}
            style={[styles.input, hasErrors("password")]}
            defaultValue={password}
            onChangeText={text => setpassword(text)}
          />

          <Button gradient onPress={handleLogin}>
            {isLoading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <Text bold white center>
                Iniciar sesión
              </Text>
            )}
          </Button>

          <Button onPress={isForgot}>
            <Text
              gray
              caption
              center
              style={{ textDecorationLine: "underline" }}
            >
              ¿Olvidaste tu contraseña?
            </Text>
          </Button>
        </Block>
      </Block>
    </KeyboardAvoidingView>
  );
}

// const mapStateToProps = state => ({
//   isLoggedIn: state.auth.isLoggedIn,
//   isLoading: state.auth.isLoading,
//   userData: state.auth.userData,
//   error: state.auth.error
// });

// const mapDispatchToProps = dispatch => ({
//   login: (email, password, version) =>
//     dispatch(actions.login({ email, password, version }))
// });

// export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  login: {
    flex: 1,
    justifyContent: "center"
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  hasErrors: {
    borderBottomColor: theme.colors.accent
  }
});
