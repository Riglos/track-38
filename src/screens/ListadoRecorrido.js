import React, { Component } from "react";
import { StyleSheet, ScrollView, Alert } from "react-native";
import NetInfo from "@react-native-community/netinfo";
import Moment from "moment";
import { Button, Block, Text, Divider } from "../components";
import { theme } from "../constants";
import { connect } from "react-redux";
import * as Location from "expo-location";
import { postRecorrido, isLoadingRecorrido } from "../actions/recorridoActions";
import { borrarFinalizadas, isLoading } from "../actions/taskActions";
import AppLoading from "../components/AppLoading";

const LOCATION_UPDATES_TASK = "location-updates";

class ListadoRecorrido extends Component {
  state = {
    connection_Status: "",
    isActive: false,
    estado: "offline",
    cumple: true
  };

  componentDidMount() {
    NetInfo.addEventListener(
      "connectionChange",
      this._handleConnectivityChange
    );

    NetInfo.fetch().then(isConnected => {
      if (isConnected.isConnected == true) {
        this.setState({ connection_Status: "Online" });
      } else {
        this.setState({ connection_Status: "Offline" });
      }
    });
  }

  componentWillUnmount() {
    const unsubscribe = NetInfo.addEventListener(
      "connectionChange",
      this._handleConnectivityChange
    );
    unsubscribe();
  }

  _handleConnectivityChange = isConnected => {
    if (isConnected == true) {
      this.setState({ connection_Status: "Online" });
    } else {
      this.setState({ connection_Status: "Offline" });
    }
  };

  validarSelecciondas() {
    if (this.props.tareas.tareasSeleccionadas.length > 0) {
      this.props.tareas.tareasSeleccionadas.map(tareasSeleccionada => {
        if (tareasSeleccionada.isStarted) {
          this.setState({ cumple: false });
        }
      });
    }
  }

  toggleTracking = async () => {
    await this.validarSelecciondas();
    const { navigation } = this.props;
    const { cumple } = this.state;
    if (cumple) {
      this.setState({ isActive: true });
      if (this.state.connection_Status == "Online") {
        this.finaliza("online");
        //CAMBIAR ESTO A ONLINE EN PRODUCCIONNNNN
        // this.finaliza("offline");
      } else {
        Alert.alert(
          "Alerta",
          "Se encuentra sin conexion se guarda en Recorridos Pendientes"
        );
        this.finaliza("offline");
      }
      navigation.navigate("Browse");
    } else {
      Alert.alert(
        "Alerta",
        "No puede finalizar un recorrido con tareas comenzadas y no finalizadas, por favor finalice todas sus tareas."
      );
    }
  };

  componentWillUnmount() {
    this.setState({ isActive: false });
  }

  async finaliza(estado) {
    await Location.stopLocationUpdatesAsync(LOCATION_UPDATES_TASK);
    this.props.postRecorrido(
      this.props.tareas.tareasFinalizadas,
      this.props.recorrido,
      this.props.usuario.userData.data,
      estado
    );
    this.props.borrarFinalizadas(estado);
  }

  renderTareas() {
    if (this.props.tareas.tareasFinalizadas.length === 0) {
      return null;
    }
    return (
      <Block style={styles.datosTarea}>
        {this.props.tareas.tareasFinalizadas.map((tarea, i) => {
          return (
            <Block
              key={i}
              row
              center
              space="between"
              style={{ marginBottom: theme.sizes.base * 1 }}
            >
              <Text gray>Tarea id </Text>
              <Text>{JSON.stringify(tarea.id)}</Text>
            </Block>
          );
        })}
      </Block>
    );
  }

  renderUsuario() {
    const { usuario } = this.props;
    return (
      <Block style={styles.datosAgente}>
        <Block
          row
          center
          space="between"
          style={{ marginBottom: theme.sizes.base * 2 }}
        >
          <Text gray>Usuario </Text>
          <Text>{usuario.userData.data.nombreUsuario}</Text>
        </Block>
        <Block
          row
          center
          space="between"
          style={{ marginBottom: theme.sizes.base * 2 }}
        >
          <Text gray>CUIT </Text>
          <Text>{usuario.userData.data.cuit}</Text>
        </Block>
      </Block>
    );
  }

  renderRecorrido() {
    const { recorrido } = this.props;
    Moment.locale("en");

    if (recorrido.dateStarted) {
      return (
        <Block style={styles.datosRecorrido}>
          <Block
            row
            center
            space="between"
            style={{ marginBottom: theme.sizes.base * 2 }}
          >
            <Text gray>Fecha Inicio </Text>
            <Text>
              {Moment(new Date(recorrido.dateStarted)).format("d MMM HH:mm:ss")}
            </Text>
          </Block>
          <Block
            row
            center
            space="between"
            style={{ marginBottom: theme.sizes.base * 2 }}
          >
            <Text gray>Fecha Fin </Text>
            <Text>{Moment(new Date()).format("d MMM HH:mm:ss")}</Text>
          </Block>
        </Block>
      );
    }
    return null;
  }

  render() {
    const { tareas, recorrido } = this.props;

    if (this.state.isActive || recorrido.isLoading || tareas.isLoading) {
      return <AppLoading />;
    }
    return (
      <Block>
        <Block flex={false} row center space="between" style={styles.header}>
          <Text h1 bold>
            Finalizar Recorrido
          </Text>
        </Block>

        <ScrollView showsVerticalScrollIndicator={false}>
          {this.renderUsuario()}
          <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          {this.renderTareas()}
          <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          {this.renderRecorrido()}
          <Block middle margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Button gradient onPress={this.toggleTracking}>
              <Text bold white center>
                Finalizar
              </Text>
            </Button>
          </Block>
        </ScrollView>
      </Block>
    );
  }
}

const mapStateToProps = state => {
  return {
    recorrido: state.recorrido,
    tareas: state.tareas,
    usuario: state.auth
  };
};
//
const mapDispatchToProps = dispatch => {
  return {
    actualizar: recorrido => dispatch(actualizar(recorrido)),
    postRecorrido: (tareasFinalizadas, recorrido, usuario, estado) =>
      dispatch(postRecorrido(tareasFinalizadas, recorrido, usuario, estado)),
    borrarFinalizadas: estado => dispatch(borrarFinalizadas(estado)),
    isLoading: bool => dispatch(isLoading(bool)),
    isLoadingRecorrido: bool => dispatch(isLoadingRecorrido(bool))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListadoRecorrido);

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: theme.sizes.base * 2
  },
  datosTarea: {
    paddingHorizontal: theme.sizes.base * 2,
    marginBottom: theme.sizes.base * 1.5
  },
  datosRecorrido: {
    paddingHorizontal: theme.sizes.base * 2
  },
  datosAgente: {
    marginTop: theme.sizes.base * 2,
    paddingHorizontal: theme.sizes.base * 2
  }
});
