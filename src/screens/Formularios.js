import React, { Component } from "react";
import { Image, StyleSheet } from "react-native";
import { Tab, Tabs } from "native-base";
import { Button, Block, Text } from "../components";
import Pendientes from "../tabs/formularios/Pendientes";
import Completadas from "../tabs/formularios/Completadas";
import AppLoading from "../components/AppLoading";
import { theme, mocks } from "../constants";
import { connect } from "react-redux";

class Formularios extends Component {
  constructor(props) {
    super(props);
    // No llames this.setState() aquí!
    this.state = {
      tarea: this.props.navigation.getParam("tarea", "NO-TAREA"),
      checkedIds: [],
      finalizados: [],
      pendientes: []
    };
    this.goSettings = this.goSettings.bind(this);
    this.getFormulariosFinalizados = this.getFormulariosFinalizados.bind(this);
  }

  componentDidMount() {
    this.getFormulariosFinalizados();
  }

  goSettings() {
    this.props.navigation.navigate("Settings");
  }

  getFormulariosFinalizados() {
    const { formularios } = this.state.tarea.servicio;
    const { resultadosFormulario } = this.state.tarea;

    console.log("formularios: ", formularios);
    console.log("resultadosFormulario: ", resultadosFormulario);

    let finalizados = [];
    let pendientes = [];

    if (formularios && formularios.length > 0) { 
    for (let index = 0; index < formularios.length; index++) {
      let found = false;
      for (let index2 = 0; index2 < resultadosFormulario.length; index2++) {
        if (
          resultadosFormulario[index2].idFormulario === formularios[index].id
        ) {
          resultadosFormulario[index2].idFormulario;
          found = true;
        }
      }
      found
        ? (finalizados = [...finalizados, formularios[index]])
        : (pendientes = [...pendientes, formularios[index]]);
    }

    this.setState({
      finalizados,
      pendientes
    });
    }
  }

  render() {
    const { profile, tareas, recorrido, auth } = this.props;

    if (tareas.isFetching || tareas.isLoading) {
      return <AppLoading />;
    }

    return (
      //ENTERO
      <Block flex={1}>
        {/* //HEADER  */}
        <Block
          flex={0.09}
          row
          center
          space="between"
          style={styles.header}
          padding={[0, theme.sizes.base * 2]}
        >
          <Text h1 bold>
            Formularios
          </Text>
          <Button
            // onPress={this.getFormulariosFinalizados}
            onPress={this.goSettings}
          >
            <Image source={profile.avatar} style={styles.avatar} />
          </Button>
        </Block>
        <Block flex={0.9}>
          <Tabs
            tabBarUnderlineStyle={{ backgroundColor: theme.colors.primary }}
          >
            <Tab
              heading="Pendientes"
              tabStyle={{ backgroundColor: theme.colors.white }}
              textStyle={{ color: theme.colors.gray }}
              activeTabStyle={{ backgroundColor: theme.colors.white }}
              activeTextStyle={{ color: theme.colors.primary }}
            >
              <Pendientes
                navigation={this.props.navigation}
                idTask={this.state.tarea.id}
                formularios={this.state.pendientes}
                // formularios={this.state.tarea.servicio.formularios}
                // resultadosFormulario={this.state.tarea.resultadosFormulario}
              />
            </Tab>
            <Tab
              heading="Completadas"
              tabStyle={{ backgroundColor: theme.colors.white }}
              textStyle={{ color: theme.colors.gray }}
              activeTabStyle={{ backgroundColor: theme.colors.white }}
              activeTextStyle={{ color: theme.colors.primary }}
            >
              <Completadas
                navigation={this.props.navigation}
                idTask={this.state.tarea.id}
                formularios={this.state.finalizados}
                resultadosFormulario={this.state.tarea.resultadosFormulario}
              />
            </Tab>
          </Tabs>
        </Block>
      </Block>
    );
  }
}

Formularios.defaultProps = {
  profile: mocks.profile
};

const mapStateToProps = state => ({
  // recorrido: state.recorrido,
  auth: state.auth,
  tareas: state.tareas
});

const mapDispatchToProps = dispatch => ({
  // postRecorrido: (tareasFinalizadas, recorrido, usuario, estado) =>
  //   dispatch(
  //     recorridoActions.postRecorrido(
  //       tareasFinalizadas,
  //       recorrido,
  //       usuario,
  //       estado
  //     )
  //   ),
  // borrarFinalizadas: estado => dispatch(taskActions.borrarFinalizadas(estado)),
  // logout: () => dispatch(actions.logout()),
  // comenzarRecorrido: () => dispatch(recorridoActions.comenzar()),
  // comenzarRecorridoSinTrackeo: () =>
  //   dispatch(recorridoActions.comenzarRecorridoSinTrackeo()),
  // finalizarRecorridoSinTrackeo: () =>
  //   dispatch(recorridoActions.finalizarRecorridoSinTrackeo()),
  // actualizarRecorrido: recorrido =>
  //   dispatch(recorridoActions.actualizar(recorrido)),
  // clickRecorrido: recorrido => dispatch(recorridoActions.click(recorrido)),
  // getTareas: usuario => dispatch(taskActions.getTareas(usuario))
});

export default connect(mapStateToProps, mapDispatchToProps)(Formularios);

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: theme.sizes.base * 1
  },
  avatar: {
    height: theme.sizes.base * 2,
    width: theme.sizes.base * 2
  }
});
