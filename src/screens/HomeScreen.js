import React, {Component} from 'react';
import {
  Animated,
  Modal,
  ScrollView,
  StyleSheet,
  FlatList,
  Dimensions,
} from 'react-native';
import {Image} from 'react-native-elements';
import {Button, Text, Block} from '../components';
import {theme} from '../constants';
import {connect} from 'react-redux';
import {VERSION, AMBIENTE} from '../constants/version';
const {width, height} = Dimensions.get('window');

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showTerms: false,
    };
    // This binding is necessary to make `this` work in the callback
    this.isAuthenticated = this.isAuthenticated.bind(this);
    this.showTerms = this.showTerms.bind(this);
    this.goSignUp = this.goSignUp.bind(this);
  }

  static navigationOptions = {
    header: null,
  };
  scrollX = new Animated.Value(0);

  componentDidMount() {
    const {navigation, auth} = this.props;
    if (auth.isLoggedIn) {
      navigation.navigate('Browse');
    }
  }

  goSignUp() {
    this.props.navigation.navigate('SignUp');
  }

  showTerms() {
    this.setState({showTerms: !this.state.showTerms});
  }

  renderTermsService() {
    return (
      <Modal
        animationType="slide"
        visible={this.state.showTerms}
        // onRequestClose={() => {
        //   // console.log("cerrar modal");
        // }}
      >
        <Block
          padding={[theme.sizes.padding * 2, theme.sizes.padding]}
          space="between"
        >
          <Text h2 light>
            Términos de servicios
          </Text>

          <ScrollView style={{marginVertical: theme.sizes.padding}}>
            <Text
              caption
              gray
              height={24}
              style={{marginBottom: theme.sizes.base}}
            >
              1. Su uso del Servicio es bajo su único riesgo. El servicio se
              proporciona "tal cual" y "según disponibilidad".
            </Text>
            <Text
              caption
              gray
              height={24}
              style={{marginBottom: theme.sizes.base}}
            >
              2. El registro y uso de los Servicios Digitales por parte de un
              usuario (en adelante el/los “Usuario/s”) indica la aceptación
              absoluta de los presentes Términos y Condiciones y de la Política
              de Privacidad.
            </Text>
            <Text
              caption
              gray
              height={24}
              style={{marginBottom: theme.sizes.base}}
            >
              3. El acceso a los Servicios Digitales es libre y gratuito. El
              Usuario garantiza y declara ser mayor de 18 años, y/o en su caso,
              que tiene la expresa autorización de sus padres o de sus tutores
              para poder acceder al sitio web y que es competente para entender
              y aceptar sin reservas las obligaciones, afirmaciones,
              representaciones y garantías establecidas en estos Términos y
              Condiciones. Por su parte, el Estado Nacional, en adelante “el
              Administrador”, deslinda su responsabilidad en el caso de no ser
              veraz la información suministrada al respecto.
            </Text>
            <Text
              caption
              gray
              height={24}
              style={{marginBottom: theme.sizes.base}}
            >
              4. No debe modificar, adaptar o piratear el Servicio ni modificar
              otro sitio web para dar a entender falsamente que está asociado
              con el Servicio, Senasa o cualquier otro servicio de Senasa.
            </Text>
            <Text
              caption
              gray
              height={24}
              style={{marginBottom: theme.sizes.base}}
            >
              5. El Usuario deberá respetar estos Términos y Condiciones de Uso
              del sitio web. Las infracciones por acción u omisión de los
              presentes Términos y Condiciones de Uso generarán el derecho a
              favor del Administrador de suspender al Usuario que las haya
              realizado.
            </Text>
            <Text
              caption
              gray
              height={24}
              style={{marginBottom: theme.sizes.base}}
            >
              6. Usted acepta no reproducir, duplicar, copiar, vender, revender
              o explotar ninguna parte del Servicio, su uso o el acceso al
              Servicio sin el permiso expreso por escrito de Senasa.
            </Text>
            <Text
              caption
              gray
              height={24}
              style={{marginBottom: theme.sizes.base}}
            >
              7. Podemos, pero no tenemos la obligación de, eliminar el
              Contenido y las Cuentas que contengan Contenido que, a nuestro
              exclusivo criterio, consideremos ilegales, ofensivos, amenazantes,
              difamatorios, difamatorios, pornográficos, obscenos u objetables,
              o que infrinja la propiedad intelectual de cualquier parte o estos
              Términos. de servicio.
            </Text>
            <Text
              caption
              gray
              height={24}
              style={{marginBottom: theme.sizes.base}}
            >
              8. El abuso verbal, físico, escrito o de otro tipo (incluidas las
              amenazas de abuso o retribución) de cualquier cliente, empleado,
              miembro o funcionario de Expo dará como resultado la cancelación
              inmediata de la cuenta.
            </Text>
            <Text
              caption
              gray
              height={24}
              style={{marginBottom: theme.sizes.base}}
            >
              9. Comprende que el procesamiento técnico y la transmisión del
              Servicio, incluido su Contenido, pueden transferirse sin cifrar e
              implican (a) transmisiones a través de varias redes; y (b) cambios
              para cumplir y adaptarse a los requisitos técnicos de conexión de
              redes o dispositivos.
            </Text>
            <Text
              caption
              gray
              height={24}
              style={{marginBottom: theme.sizes.base}}
            >
              10. No debe cargar, publicar, alojar ni transmitir mensajes de
              correo electrónico, SMS o "spam" no solicitados.
            </Text>
          </ScrollView>

          <Block middle padding={[theme.sizes.base / 2, 0]}>
            <Button gradient onPress={this.showTerms}>
              <Text center white>
                Entiendo
              </Text>
            </Button>
          </Block>
        </Block>
      </Modal>
    );
  }

  renderIllustrations() {
    const {illustrations} = this.props;

    return (
      <FlatList
        horizontal
        pagingEnabled
        scrollEnabled
        showsHorizontalScrollIndicator={false}
        scrollEventThrottle={16}
        snapToAlignment="center"
        data={illustrations}
        extraDate={this.state}
        keyExtractor={(item, index) => `${item.id}`}
        renderItem={({item}) => (
          <Image
            source={item.source}
            resizeMode="contain"
            style={{width, height: height / 2, overflow: 'visible'}}
          />
        )}
        onScroll={Animated.event([
          {
            nativeEvent: {contentOffset: {x: this.scrollX}},
          },
        ])}
      />
    );
  }
  isAuthenticated() {
    const {navigation} = this.props;

    if (this.props.auth.isLoggedIn) {
      navigation.navigate('Browse');
    }
    navigation.navigate('Login');
  }

  renderSteps() {
    const {illustrations} = this.props;
    const stepPosition = Animated.divide(this.scrollX, width);
    return (
      <Block row center middle style={styles.stepsContainer}>
        {illustrations.map((item, index) => {
          const opacity = stepPosition.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [0.4, 1, 0.4],
            extrapolate: 'clamp',
          });

          return (
            <Block
              animated
              flex={false}
              key={`step-${index}`}
              color="gray"
              style={[styles.steps, {opacity}]}
            />
          );
        })}
      </Block>
    );
  }

  render() {
    return (
      <Block>
        <Block center bottom flex={0.4}>
          <Text h1 center bold>
            SIG
            <Text h1 primary>
              {' '}
              APP.
            </Text>
          </Text>
          <Text h3 gray2 style={{marginTop: theme.sizes.padding / 2}}>
            Disfruta de esta experiencia.
          </Text>
        </Block>
        <Block center middle>
          {this.renderIllustrations()}
          {this.renderSteps()}
        </Block>

        <Block middle flex={0.5} margin={[0, theme.sizes.padding * 2]}>
          <Button gradient onPress={this.isAuthenticated}>
            <Text center semibold white>
              Iniciar sesión
            </Text>
          </Button>
          <Button shadow onPress={this.goSignUp}>
            <Text center semibold>
              Regístrate
            </Text>
          </Button>
          <Button onPress={this.showTerms}>
            <Text center caption gray>
              Términos de servicio -
              <Text caption bold>
                {VERSION} - {AMBIENTE}
              </Text>
            </Text>
          </Button>
        </Block>
        {this.renderTermsService()}
      </Block>
    );
  }
}

HomeScreen.defaultProps = {
  illustrations: [
    {id: 1, source: require('../../assets/images/illustration_1.png')},
    {id: 2, source: require('../../assets/images/illustration_2.png')},
    {id: 3, source: require('../../assets/images/illustration_3.png')},
  ],
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, null)(HomeScreen);

const styles = StyleSheet.create({
  stepsContainer: {
    position: 'absolute',
    bottom: theme.sizes.base * 3,
    right: 0,
    left: 0,
  },
  steps: {
    width: 5,
    height: 5,
    borderRadius: 5,
    marginHorizontal: 2.5,
  },
});
