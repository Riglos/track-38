import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  Alert,
  Image,
  StyleSheet,
  TouchableOpacity,
  AppState,
  Linking,
} from 'react-native';
import {useNetInfo} from '@react-native-community/netinfo';
import {
  Fab,
  Icon as IconBase,
  Button as ButtonBase,
  Tab,
  Tabs,
} from 'native-base';
import {AMBIENTE} from '../constants/version';
import {Icon} from 'react-native-elements';
import {Badge, Button, Block, Text} from '../components';
import Pendientes from '../tabs/Pendientes';
import Seleccionadas from '../tabs/Seleccionadas';
import Completadas from '../tabs/Completadas';
import * as actions from '../actions/userActions';
import * as recorridoActions from '../actions/recorridoActions';
import AppLoading from '../components/AppLoading';
import * as taskActions from '../actions/taskActions';
import {theme, mocks} from '../constants';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import {persistenciaActions} from '../actions';
const LOCATION_UPDATES_TASK = 'location-updates';

export default function Browse({navigation}) {
  const netInfo = useNetInfo();

  const {auth, recorrido, tareas} = useSelector((state) => state);
  const dispatch = useDispatch();
  const [profile, setlocalProfile] = useState(mocks.profile);

  const [activePopup, setactivePopup] = useState(false);
  const [accuracy, setaccuracy] = useState(Location.Accuracy.BestForNavigation);
  const [isTracking, setisTracking] = useState(false);
  const [
    showsBackgroundLocationIndicator,
    setshowsBackgroundLocationIndicator,
  ] = useState(true);
  const [savedLocations, setsavedLocations] = useState([]);
  const [tareaslocal, setltareaslocal] = useState([]);
  const [checkedIds, setcheckedIds] = useState([]);
  const [localizacionHabilitada, setlocalizacionHabilitada] = useState(false);
  const [estado, setestado] = useState('offline');
  const [error, seterror] = useState();
  const [color, setcolor] = useState(theme.colors.gray);

  useEffect(() => {
    setcolor(
      recorrido.isStarted
        ? auth.enabledTracking
          ? theme.colors.green2
          : theme.colors.orange
        : theme.colors.gray
    );
  }, [recorrido.isStarted, auth.enabledTracking]);

  // constructor(props) {
  //   super(props);
  //   // No llames this.setState() aquí!
  //   this.state = {
  //     activePopup: false,
  //     accuracy: Location.Accuracy.BestForNavigation,
  //     isTracking: false,
  //     showsBackgroundLocationIndicator: true,
  //     savedLocations: [],
  //     tareas: [],
  //     checkedIds: [],
  //     localizacionHabilitada: false,
  //     estado: "offline"
  //   };
  //   this.openWeb = this.openWeb.bind(this);
  //   this.goSettings = this.goSettings.bind(this);
  //   this.goHistorialRecorrido = this.goHistorialRecorrido.bind(this);
  //   this.goAddTask = this.goAddTask.bind(this);
  //   this.activePopUp = this.activePopUp.bind(this);
  // }

  // async componentDidMount() {
  //   this._getLocationAsync();
  // NetInfo.addEventListener(
  //   "connectionChange",
  //   this._handleConnectivityChange
  // );

  // NetInfo.fetch().then(isConnected => {
  //   if (isConnected.isConnected == true) {
  //     this.setState({ connection_Status: "Online" });
  //   } else {
  //     this.setState({ connection_Status: "Offline" });
  //   }
  // });
  // }
  useEffect(() => {
    _getLocationAsync();
  }, []);

  // componentWillUnmount() {
  //   const unsubscribe = NetInfo.addEventListener(
  //     "connectionChange",
  //     this._handleConnectivityChange
  //   );
  //   unsubscribe();
  // }

  // _handleConnectivityChange = isConnected => {
  //   if (isConnected == true) {
  //     this.setState({ connection_Status: "Online" });
  //   } else {
  //     this.setState({ connection_Status: "Offline" });
  //   }
  // };

  // componentWillMount = async () => {
  //   this.props.getTareas(this.props.auth);
  //   let gpsStatus = await Location.hasServicesEnabledAsync();
  //   const isTracking = await Location.hasStartedLocationUpdatesAsync(
  //     LOCATION_UPDATES_TASK
  //   );
  //   if (gpsStatus) {
  //     if (this.props.recorrido.isStarted && isTracking == false) {
  //       await Location.startLocationUpdatesAsync(LOCATION_UPDATES_TASK, {
  //         // The best level of accuracy available.
  //         accuracy: Location.Accuracy.Highest,
  //         foregroundService: {
  //           notificationTitle: "SENASA",
  //           notificationBody: "Disfrute su recorrido."
  //         },
  //         // Receive updates only when the location has changed by at least this distance in meters.
  //         distanceInterval: 10,
  //         //Minimum time to wait between each update in milliseconds.
  //         timeInterval: 2000
  //       });
  //     }
  //   } else {
  //     Alert.alert(
  //       "Alerta",
  //       "Active el GPS o no podra inicializar o finalizar el Recorrido."
  //     );
  //   }
  // };

  useEffect(() => {
    dispatch(taskActions.getTareas(auth));

    dispatch(persistenciaActions.configuracionUsuario(auth));
    dispatch(persistenciaActions.getServicios(auth));
    dispatch(persistenciaActions.getTiposEntes(auth));

    async function localizacion() {
      let gpsStatus = await Location.hasServicesEnabledAsync();
      const isTracking = await Location.hasStartedLocationUpdatesAsync(
        LOCATION_UPDATES_TASK
      );

      if (gpsStatus) {
        if (recorrido.isStarted && isTracking == false) {
          await Location.startLocationUpdatesAsync(LOCATION_UPDATES_TASK, {
            // The best level of accuracy available.
            accuracy: Location.Accuracy.Highest,
            foregroundService: {
              notificationTitle: 'SENASA',
              notificationBody: 'Disfrute su recorrido.',
            },
            // Receive updates only when the location has changed by at least this distance in meters.
            distanceInterval: 10,
            //Minimum time to wait between each update in milliseconds.
            timeInterval: 2000,
          });
        }
      } else {
        Alert.alert(
          'Alerta',
          'Active el GPS o no podra inicializar o finalizar el Recorrido.'
        );
      }
    }
    localizacion();
  }, []);

  // componentWillUpdate(newProps) {
  //   if (newProps.recorrido !== this.props.recorrido) {
  //     this._isTracking();
  //   }
  // }
  const _isTracking = async () => {
    // const { coords } = await Location.getCurrentPositionAsync();
    //A promise resolving to boolean value indicating whether the location task is started or not.
    const isTracking = await Location.hasStartedLocationUpdatesAsync(
      LOCATION_UPDATES_TASK
    );

    // this.setState({
    //   isTracking
    // });
    setisTracking(isTracking);
  };

  useEffect(() => {
    _isTracking();
  }, [recorrido]);

  const openWeb = () => {
    Linking.openURL(
      'https://' +
        AMBIENTE +
        '.senasa.gov.ar//SAS/faces/app/loginApp.xhtml?user=' +
        auth.userData.data.nombreUsuario +
        '&token=' +
        auth.userData.data.token
    );
  };

  const goSettings = () => {
    navigation.navigate('Settings');
  };

  const goHistorialRecorrido = () => {
    navigation.navigate('HistorialRecorrido');
  };

  const goAddTask = () => {
    navigation.navigate('AddTask');
  };

  const activePopUp = () => {
    setactivePopup(!activePopup);
    // this.setState({ activePopup: !this.state.activePopup });
  };

  const _getLocationAsync = async () => {
    let {status} = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      AppState.addEventListener('change', handleAppStateChange);
      seterror(
        'Se requieren permisos de ubicación para utilizar esta función. Puede habilitarlos manualmente en cualquier momento en la sección "Servicios de ubicación" de la aplicación Configuración.'
      );
      // this.setState({
      //   error:
      //     'Se requieren permisos de ubicación para utilizar esta función. Puede habilitarlos manualmente en cualquier momento en la sección "Servicios de ubicación" de la aplicación Configuración.'
      // });
      return;
    } else {
      seterror(null);
      // this.setState({ error: null });
    }
  };

  const startLocationUpdates = async () => {
    console.log('startLocationUpdates');
    await Location.startLocationUpdatesAsync(LOCATION_UPDATES_TASK, {
      // The best level of accuracy available.
      accuracy: Location.Accuracy.Highest,
      foregroundService: {
        notificationTitle: 'SENASA',
        notificationBody: 'Disfrute su recorrido.',
      },
      // Receive updates only when the location has changed by at least this distance in meters.
      distanceInterval: 10,
      //Minimum time to wait between each update in milliseconds.
      timeInterval: 2000,
    });

    if (!isTracking) {
      Alert.alert(
        'Alerta',
        'Ahora puede enviar la aplicación al fondo, ir a algún lugar y volver aquí. Incluso puede click la aplicación y se activará cuando se produzca el nuevo cambio de ubicación significativo.'
      );
    }
    // this.setState({ isTracking: true });
    // dispatch(recorridoActions.comenzarRecorrido());
    dispatch(recorridoActions.comenzar());
  };

  const stopLocationUpdates = async () => {
    await Location.stopLocationUpdatesAsync(LOCATION_UPDATES_TASK);
    if (recorrido.isStarted) {
      Alert.alert('Finalizo el recorrido.', 'Con trackeo.');
      dispatch(
        recorridoActions.postRecorrido(
          tareas.tareasFinalizadas,
          recorrido,
          auth.userData.data,
          netInfo.isConnected ? 'online' : 'offline'
        )
      );
      dispatch(taskActions.borrarFinalizadas(estado));
    } else {
      Alert.alert('Algo salio mal.');
      // Alert.alert("Comenzo el recorrido.", "Sin trackeo.");
      // dispatch(recorridoActions.comenzarRecorridoSinTrackeo());
    }
  };

  toggleTracking = async () => {
    console.log('toggleTracking');
    if (auth.enabledTracking) {
      const gpsStatus = await Location.hasServicesEnabledAsync();
      if (gpsStatus) {
        if (recorrido.isStarted) {
          console.log('hace un stop');
          stopLocationUpdates();
        } else {
          console.log('hace un start');
          await startLocationUpdates();
        }
      } else {
        Alert.alert(
          'Alerta',
          'Active el GPS o no podra inicializar o finalizar el Recorrido.'
        );
      }
    } else {
      if (recorrido.isStarted) {
        Alert.alert('Finalizo el recorrido.', 'Sin trackeo.');
        // let estado = netInfo.isConnected ? "online" : "offline";
        // console.log("estado")
        dispatch(
          recorridoActions.postRecorrido(
            tareas.tareasFinalizadas,
            recorrido,
            auth.userData.data,
            netInfo.isConnected ? 'online' : 'offline'
          )
        );
        dispatch(taskActions.borrarFinalizadas(estado));
        // this.props.finalizarRecorridoSinTrackeo();
      } else {
        Alert.alert('Comenzo el recorrido.', 'Sin trackeo.');
        dispatch(recorridoActions.comenzarRecorridoSinTrackeo());
      }
    }
  };

  function renderOpciones() {
    return (
      <Fab
        active={activePopup}
        direction="up"
        containerStyle={{}}
        style={{backgroundColor: '#0083ff'}}
        position="bottomRight"
        onPress={activePopUp}
      >
        <IconBase name="share" />
        <ButtonBase onPress={openWeb} style={{backgroundColor: '#0083ff'}}>
          <IconBase name="md-send" />
        </ButtonBase>
        <ButtonBase
          onPress={goHistorialRecorrido}
          style={{backgroundColor: '#0083ff'}}
        >
          <IconBase name="list" />
        </ButtonBase>
      </Fab>
    );
  }

  // render() {
  // const { profile, tareas, recorrido, auth } = this.props;

  if (tareas.isFetching || recorrido.isLoading || tareas.isLoading) {
    return <AppLoading />;
  }
  // let color;
  // console.log(auth);
  // if (recorrido.isStarted) {
  //   auth.enabledTracking
  //     ? (color = theme.colors.green2)
  //     : (color = theme.colors.orange);
  // }
  // if (recorrido.isStarted) {
  //   var color;
  //   recorrido.startedWithoutTracking
  //     ? (color = theme.colors.orange)
  //     : (color = theme.colors.green2);
  // }

  return (
    //ENTERO
    <Block flex={1}>
      {/* //HEADER  */}
      <Block
        flex={0.09}
        row
        center
        space="between"
        style={styles.header}
        padding={[0, theme.sizes.base * 2]}
      >
        <Text h1 bold>
          Tareas
        </Text>
        <Button onPress={goSettings}>
          <Image source={profile.avatar} style={styles.avatar} />
        </Button>
      </Block>
      <Block flex={0.8}>
        <Tabs tabBarUnderlineStyle={{backgroundColor: theme.colors.primary}}>
          <Tab
            heading="Pendientes"
            tabStyle={{backgroundColor: theme.colors.white}}
            textStyle={{color: theme.colors.gray}}
            activeTabStyle={{backgroundColor: theme.colors.white}}
            activeTextStyle={{color: theme.colors.primary}}
          >
            <Pendientes navigation={navigation} tareas={tareas} />
          </Tab>
          <Tab
            heading="Seleccionadas"
            tabStyle={{backgroundColor: theme.colors.white}}
            textStyle={{color: theme.colors.gray}}
            activeTabStyle={{backgroundColor: theme.colors.white}}
            activeTextStyle={{color: theme.colors.primary}}
          >
            <Seleccionadas
              navigation={navigation}
              tareas={tareas}
              recorrido={recorrido}
            />
          </Tab>
          <Tab
            heading="Completadas"
            tabStyle={{backgroundColor: theme.colors.white}}
            textStyle={{color: theme.colors.gray}}
            activeTabStyle={{backgroundColor: theme.colors.white}}
            activeTextStyle={{color: theme.colors.primary}}
          >
            <Completadas navigation={navigation} tareas={tareas} />
          </Tab>
        </Tabs>
      </Block>

      <Block row flex={0.15}>
        <Block center middle column>
          <TouchableOpacity onPress={goAddTask}>
            <Badge size={50} color={theme.colors.gray}>
              <Icon
                name="add-circle-outline"
                type="material"
                color={theme.colors.white}
              />
            </Badge>
          </TouchableOpacity>
        </Block>

        <Block center middle column>
          <TouchableOpacity onPress={toggleTracking}>
            <Badge size={50} color={color}>
              {/* <Icon
                name="directions-car"
                type="material"
                color={theme.colors.white}
              /> */}
              <Icon
                name="play-arrow"
                type="material"
                color={theme.colors.white}
              />
            </Badge>
          </TouchableOpacity>
        </Block>
        <Block center middle column>
          {renderOpciones()}
        </Block>
      </Block>
    </Block>
  );
}

// Browse.defaultProps = {
//   profile: mocks.profile
// };

// const mapStateToProps = state => ({
//   recorrido: state.recorrido,
//   auth: state.auth,
//   tareas: state.tareas
// });

// const mapDispatchToProps = dispatch => ({
//   postRecorrido: (tareasFinalizadas, recorrido, usuario, estado) =>
//     dispatch(
//       recorridoActions.postRecorrido(
//         tareasFinalizadas,
//         recorrido,
//         usuario,
//         estado
//       )
//     ),
//   borrarFinalizadas: estado => dispatch(taskActions.borrarFinalizadas(estado)),

//   logout: () => dispatch(actions.logout()),
//   comenzarRecorrido: () => dispatch(recorridoActions.comenzar()),
//   comenzarRecorridoSinTrackeo: () =>
//     dispatch(recorridoActions.comenzarRecorridoSinTrackeo()),
//   finalizarRecorridoSinTrackeo: () =>
//     dispatch(recorridoActions.finalizarRecorridoSinTrackeo()),
//   actualizarRecorrido: recorrido =>
//     dispatch(recorridoActions.actualizar(recorrido)),
//   clickRecorrido: recorrido => dispatch(recorridoActions.click(recorrido)),
//   getTareas: usuario => dispatch(taskActions.getTareas(usuario))
// });

// export default connect(mapStateToProps, mapDispatchToProps)(Browse);

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: theme.sizes.base * 1,
  },
  avatar: {
    height: theme.sizes.base * 2,
    width: theme.sizes.base * 2,
  },
});
