import React, {Component} from 'react';
import {
  Alert,
  StyleSheet,
  ScrollView,
  TextInput,
  TouchableOpacity,
  View,
  Linking,
} from 'react-native';
import {Icon, Badge} from 'react-native-elements';
import {Button, Block, Text, Divider} from '../components';
import {theme} from '../constants';
import {AMBIENTE} from '../constants/version';
import {connect} from 'react-redux';
import {agregarCuve, eliminarCuve} from '../actions/taskActions';
import {BarCodeScanner} from 'expo-barcode-scanner';
import MapView from 'react-native-maps';
import * as Permissions from 'expo-permissions';

class TaskDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      escaneado: null,
      hasCameraPermission: null,
      scanned: false,
      activarCodigoBarra: false,
      tarea: this.props.navigation.getParam('tarea', 'NO-TAREA'),
      tareas: [],
      usuario: [],
      editingCuve: null,
      cuve: null,
    };
    this.handleAgregarCuve = this.handleAgregarCuve.bind(this);
    this.handleEliminarCuve = this.handleEliminarCuve.bind(this);
    this.volver = this.volver.bind(this);
    this.openFormulariosApp = this.openFormulariosApp.bind(this);
    this.openWeb = this.openWeb.bind(this);
    this.eliminarTarea = this.eliminarTarea.bind(this);
  }

  volver() {
    this.props.navigation.goBack();
  }

  eliminarTarea() {
    const {tarea} = this.state;
    this.props.navigation.navigate('DeleteModal', {task: tarea});
  }

  openFormulariosApp() {
    this.props.navigation.navigate('Formularios', {tarea: this.state.tarea});
  }

  async componentDidMount() {
    this.setState({
      tareas: this.props.tareas.data,
      usuario: this.props.usuario.userData.data,
    });
    this.getPermissionsAsync();
  }

  getPermissionsAsync = async () => {
    const {status} = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({hasCameraPermission: status === 'granted'});
  };

  handleEditCuve(text) {
    this.setState({cuve: text});
  }

  handleAgregarCuve = () => {
    const {tarea, cuve} = this.state;
    if (cuve == null || cuve == '') {
      Alert.alert('Alerta', 'Complete el CUVE.');
    } else {
      this.props.agregarCuve(tarea.id, cuve);
      this.setState({cuve: null});
    }
  };

  handleEliminarCuve = (posicion) => () => {
    const {tarea} = this.state;
    this.props.eliminarCuve(tarea.id, posicion);
  };

  renderAgente() {
    const {tarea} = this.state;

    if (tarea.length === 0) {
      return null;
    }

    if (tarea.id >= 0) {
      return (
        <Block style={styles.datosTarea}>
          <Block
            row
            center
            space="between"
            style={{marginBottom: theme.sizes.base * 2}}
          >
            <Text gray>Nombre </Text>
            <Text>{tarea.agente.nombre}</Text>
          </Block>
          <Block
            row
            center
            space="between"
            style={{marginBottom: theme.sizes.base * 2}}
          >
            <Text gray>CUIT </Text>
            <Text>{tarea.agente.cuit}</Text>
          </Block>
        </Block>
      );
    }
    return null;
  }

  renderCuves() {
    const {tarea} = this.state;

    if (tarea.cuves == undefined) {
      return null;
    }
    return (
      <Block style={styles.datosTarea}>
        {tarea.cuves.map((cuve, i) => {
          return (
            <Block
              key={i}
              row
              center
              space="between"
              style={{marginBottom: theme.sizes.base * 2}}
            >
              <Text gray>CUVE {i + 1}</Text>
              <Text>{cuve}</Text>
              <TouchableOpacity onPress={this.handleEliminarCuve(i)}>
                <Icon
                  name="remove-circle-outline"
                  type="material"
                  color={theme.colors.gray}
                />
              </TouchableOpacity>
            </Block>
          );
        })}
      </Block>
    );
  }

  renderUsuario() {
    const {usuario} = this.state;
    return (
      <Block style={styles.datosAgente}>
        <Block
          row
          center
          space="between"
          style={{marginBottom: theme.sizes.base * 2}}
        >
          <Text gray>Usuario </Text>
          <Text>{usuario.nombreUsuario}</Text>
        </Block>
        <Block
          row
          center
          space="between"
          style={{marginBottom: theme.sizes.base * 2}}
        >
          <Text gray>CUIT </Text>
          <Text>{usuario.cuit}</Text>
        </Block>
      </Block>
    );
  }

  renderEnte() {
    const {tarea} = this.state;
    if (tarea.id >= 0) {
      return (
        <Block style={styles.datosRecorrido}>
          <Block
            row
            center
            space="between"
            style={{marginBottom: theme.sizes.base * 2}}
          >
            <Text gray>Identificador </Text>
            <Text>{tarea.servicio.ente.identificador}</Text>
          </Block>
          <Block
            row
            center
            space="between"
            style={{marginBottom: theme.sizes.base * 2}}
          >
            <Text gray>Razon Social </Text>
            <Text>{tarea.servicio.ente.razonSocial}</Text>
          </Block>
          <Block
            row
            center
            space="between"
            style={{marginBottom: theme.sizes.base * 2}}
          >
            <Text gray>Numero Cliente </Text>
            <Text>{tarea.servicio.ente.numeroCliente}</Text>
          </Block>
          <Block
            row
            center
            space="between"
            style={{marginBottom: theme.sizes.base * 2}}
          >
            <Text gray>Telefono </Text>
            <Text>{tarea.servicio.telefono}</Text>
          </Block>
          <Block
            row
            center
            space="between"
            style={{marginBottom: theme.sizes.base * 2}}
          >
            <Text gray>Email </Text>
            <Text>{tarea.servicio.email}</Text>
          </Block>
        </Block>
      );
    }
    return null;
  }

  renderCoordenada() {
    const {tarea} = this.state;
    if (tarea.id >= 0 && tarea.servicio.latitud) {
      return (
        <MapView
          style={{
            alignSelf: 'stretch',
            height: 300,
            marginLeft: theme.sizes.base * 2,
            marginRight: theme.sizes.base * 2,
            marginBottom: theme.sizes.base * 2,
          }}
          initialRegion={{
            latitude: tarea.servicio.latitud,
            longitude: tarea.servicio.longitud,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          provider={MapView.PROVIDER_GOOGLE}
        >
          <MapView.Marker
            key={tarea.servicio.latitud}
            coordinate={{
              latitude: tarea.servicio.latitud,
              longitude: tarea.servicio.longitud,
            }}
            title={tarea.servicio.nombreOficina}
            description={tarea.servicio.detalleTramite}
          />
        </MapView>
      );
    }
    return null;
  }

  renderTurno() {
    const {tarea} = this.state;

    if (tarea.id >= 0) {
      const horaDesde =
        tarea.servicio.turno == null ? (
          <Block row center right>
            <Badge status="warning" />
            <Text>No posee turno</Text>
          </Block>
        ) : (
          <Text>{tarea.servicio.turno.horaDesdeString}</Text>
        );
      const horaHasta =
        tarea.servicio.turno == null ? (
          <Block row center right>
            <Badge status="warning" />
            <Text>No posee turno</Text>
          </Block>
        ) : (
          <Text>{tarea.servicio.turno.horaHastaString}</Text>
        );
      const fechaTurno =
        tarea.servicio.turno == null ? (
          <Block row center right>
            <Badge status="warning" />
            <Text>No posee turno</Text>
          </Block>
        ) : (
          <Text>{tarea.servicio.turno.fechaString}</Text>
        );
      const observacionTurno =
        tarea.servicio.turno.observacion == null ? (
          <Block row center right>
            <Badge status="warning" />
            <Text>No posee observacion</Text>
          </Block>
        ) : (
          <Text>{tarea.servicio.turno.observacion}</Text>
        );

      return (
        <Block style={styles.datosRecorrido}>
          <Block
            row
            center
            space="between"
            style={{marginBottom: theme.sizes.base * 2}}
          >
            <Text gray>Hora Desde </Text>
            {horaDesde}
          </Block>
          <Block
            row
            center
            space="between"
            style={{marginBottom: theme.sizes.base * 2}}
          >
            <Text gray>Hora Hasta </Text>
            {horaHasta}
          </Block>
          <Block
            row
            center
            space="between"
            style={{marginBottom: theme.sizes.base * 2}}
          >
            <Text gray>Fecha Turno </Text>
            {fechaTurno}
          </Block>
          <Block
            row
            center
            space="between"
            style={{marginBottom: theme.sizes.base * 2}}
          >
            <Text gray>Observacion </Text>
            {observacionTurno}
          </Block>
        </Block>
      );
    }
    return null;
  }

  handleBarCodeScanned = ({type, data}) => {
    this.setState({scanned: true});
    this.setState({escaneado: data});
    Alert.alert(
      'Alerta',
      `Codigo de barra con tipo ${type} y valor ${data} se escaneo!`
    );
  };

  aceptarCuve() {
    Alert.alert('Alerta', 'aceptar cuve');
    this.setState({scanned: false, activarCodigoBarra: false});
    this.props.agregarCuve(this.state.tarea.id, this.state.escaneado);
  }

  openWeb() {
    // SAS/faces/app/loginApp.xhtml?user=marellano&token=aaaa&view=formularios&param=541
    // console.log("tarea id: ", this.state.tarea.id);
    Linking.openURL(
      'https://' +
        AMBIENTE +
        '.senasa.gov.ar//SAS/faces/app/loginApp.xhtml?user=' +
        this.props.usuario.userData.data.nombreUsuario +
        '&token=' +
        this.props.usuario.userData.data.token +
        '&view=formularios&param=' +
        this.state.tarea.id
    );
  }

  render() {
    const {
      cuve,
      activarCodigoBarra,
      hasCameraPermission,
      scanned,
      tarea,
    } = this.state;

    if (activarCodigoBarra) {
      if (hasCameraPermission === null) {
        return <Text>Solicitando permiso para la camara</Text>;
      }
      if (hasCameraPermission === false) {
        return <Text>No posee acceso a la camará</Text>;
      }

      return (
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'flex-end',
          }}
        >
          <BarCodeScanner
            onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
            style={StyleSheet.absoluteFillObject}
          >
            <View style={styles.layerTop} />
            <View style={styles.layerCenter}>
              <View style={styles.layerLeft} />
              <View style={styles.focused} />
              <View style={styles.layerRight} />
            </View>
            <View style={styles.layerBottom} />
          </BarCodeScanner>

          {scanned && (
            <Button
              center
              gradient
              onPress={() => this.setState({scanned: false})}
            >
              <Text center white>
                Presiona para escanear de nuevo
              </Text>
            </Button>
          )}
          {scanned && (
            <Button center gradient onPress={() => this.aceptarCuve()}>
              <Text center white>
                Aceptar Cuve
              </Text>
            </Button>
          )}
          {scanned && (
            <Button
              center
              gradient
              onPress={() =>
                this.setState({scanned: false, activarCodigoBarra: false})
              }
            >
              <Text center white>
                Cancelar Cuve
              </Text>
            </Button>
          )}
        </View>
      );
    }

    return (
      <Block>
        <Block flex={false} row center space="between" style={styles.header}>
          <Text h1 bold>
            Tarea
          </Text>
        </Block>

        <ScrollView showsVerticalScrollIndicator={false}>
          <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          {this.renderEnte()}
          <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          {this.renderTurno()}
          <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          {this.renderAgente()}
          <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          {/* <Block middle margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Button gradient onPress={this.openWeb}>
              <Text bold white center>
                Ver Formularios Web
              </Text>
            </Button>
          </Block> */}
          <Block middle margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Button gradient onPress={this.openFormulariosApp}>
              <Text bold white center>
                Ver Formularios App
              </Text>
            </Button>
          </Block>

          {this.renderCoordenada()}
          {/* <Block style={styles.inputs}>
            <Block row space="between" margin={[10, 0]} style={styles.inputRow}>
              <Block>
                <Text gray style={{marginBottom: 10}}>
                  CUVES
                </Text>
                <TextInput
                  placeholder="Ingrese CUVE"
                  defaultValue={cuve}
                  onChangeText={(text) => this.handleEditCuve(text)}
                />
              </Block>
              <TouchableOpacity onPress={this.handleAgregarCuve}>
                <Icon
                  name="add-circle-outline"
                  type="material"
                  color={theme.colors.gray}
                />
              </TouchableOpacity>
            </Block>
          </Block> */}
          {/* {this.renderCuves()} */}

          {/* <Block middle margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Button
              gradient
              onPress={() => this.setState({activarCodigoBarra: true})}
            >
              <Text bold white center>
                Ingrese CUVE con código de barras.
              </Text>
            </Button>
          </Block> */}

          <Block middle margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Button gradient onPress={this.volver}>
              <Text bold white center>
                Volver
              </Text>
            </Button>
          </Block>
          {tarea.id && tarea.id > 0 ? (
            <Block middle margin={[theme.sizes.base, theme.sizes.base * 2]}>
              <Button gradient onPress={this.eliminarTarea}>
                <Text bold white center>
                  Eliminar Tarea
                </Text>
              </Button>
            </Block>
          ) : null}
        </ScrollView>
      </Block>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tareas: state.tareas,
    usuario: state.auth,
  };
};
//
// const mapDispatchToProps = (dispatch) => {
//   return {
//     agregarCuve: (idTarea, cuve) => dispatch(agregarCuve(idTarea, cuve)),
//     eliminarCuve: (idTarea, posicion) =>
//       dispatch(eliminarCuve(idTarea, posicion)),
//   };
// };

export default connect(mapStateToProps)(TaskDetail);

// export default Completadas;
const opacity = 'rgba(0, 0, 0, .6)';
const styles = StyleSheet.create({
  layerTop: {
    flex: 2,
    backgroundColor: opacity,
  },
  layerCenter: {
    flex: 1,
    flexDirection: 'row',
  },
  layerLeft: {
    flex: 1,
    backgroundColor: opacity,
  },
  focused: {
    flex: 10,
  },
  layerRight: {
    flex: 1,
    backgroundColor: opacity,
  },
  layerBottom: {
    flex: 2,
    backgroundColor: opacity,
  },
  header: {
    paddingHorizontal: theme.sizes.base * 2,
  },
  datosTarea: {
    paddingHorizontal: theme.sizes.base * 2,
  },
  datosRecorrido: {
    paddingHorizontal: theme.sizes.base * 2,
  },
  inputRow: {
    alignItems: 'flex-end',
  },
  datosAgente: {
    marginTop: theme.sizes.base * 2,
    paddingHorizontal: theme.sizes.base * 2,
  },
  inputs: {
    paddingHorizontal: theme.sizes.base * 2,
  },
});
