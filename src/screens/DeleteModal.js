import React from "react";
import { AMBIENTE } from "../constants/version";
import { useDispatch, useSelector } from "react-redux";
import { View, Alert } from "react-native";
import { Button, Block, Text, Divider } from "../components";
import * as actionsTask from "../actions/taskActions";
import { useNetInfo } from "@react-native-community/netinfo";

export default function DeleteModal({ navigation }) {
  const { auth } = useSelector(state => state);
  const netInfo = useNetInfo();
  const dispatch = useDispatch();
  const ID_TASK = navigation.getParam("task");
  console.log("ID_TASK: ", ID_TASK.id);
  const eliminarTarea = () => {
    // dispatch(actionsTask.deleteTask(navigation.getParam("task")));
    // navigation.navigate("Browse");
    if (netInfo.isConnected == true) {
      return fetch(
        "https://" +
          AMBIENTE +
          ".senasa.gov.ar/SAS/rest/service/rechazar/" +
          ID_TASK.id,
        {
          method: "POST",
          headers: {
            usuario: auth.userData.data.nombreUsuario,
            token: auth.userData.data.token,
            Accept: "application/json, text/plain, */*",
            "Content-Type": "application/json"
          }
          // body: JSON.stringify({
          //   firstParam: 'yourValue',
          //   secondParam: 'yourOtherValue'
          //
        }
      )
        .then(response => {
          console.log("response: ", response);
          if (response.status == 400) {
            alert(
              "La peticion no es correcta, por favor comuniquese con el equipo de SAS"
            );
          }
          if (response.status == 503) {
            alert("Servidor caido 503");
          }
          if (response.status == 500) {
            alert("Servidor caido 500");
          }
          if (response.status == 401) {
            alert("Credenciales invalidas");
          }
          if (response.status == 405) {
            alert("La versión no se encuentra actualizada");
          }
          if (response.status == 402) {
            alert(
              "El usuario no cuenta con los roles necesarios para ingresar."
            );
          }
          if (response.status == 200) {
            dispatch(actionsTask.deleteTask(navigation.getParam("task")));
            navigation.navigate("Browse");
          }
        })
        .catch(error => {
          console.error(error);
        });
    } else {
      Alert.alert(
        "Sin conexión",
        "En este momento no puede eliminar la tarea, asegurese de tener conexión."
      );
    }
  };

  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text style={{ fontSize: 30 }}>Presione para eliminar!</Text>
      <Button center color="#fc3003" onPress={eliminarTarea}>
        <Text center white>
          Eliminar Tarea
        </Text>
      </Button>
    </View>
  );
}
