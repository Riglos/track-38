import React, { Component } from "react";
import {
  Alert,
  Dimensions,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View,
  RefreshControl
} from "react-native";
import {
  Icon,
  CheckBox,
  Overlay,
  Badge as BadgeElements
} from "react-native-elements";
import { connect } from "react-redux";
import {
  Card,
  Badge,
  Block,
  Button as ButtonComponents,
  Text,
  Divider
} from "../../components";
import { theme } from "../../constants";

import { taskActions } from "../../actions/index";

const height = Dimensions.get("window").height;
const { width } = Dimensions.get("window");

class Finalizados extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tareaContingencia: null,
      tarea: null,
      visibleAgregar: false,
      visibleConfirmar: false,
      visibleEliminarContingencia: false,
      tareas: [],
      tareasContingencia: [],
      checkedIds: [],
      refreshing: false
    };

    this.habilitadoModificacion = this.habilitadoModificacion.bind(this);
    this.valido = this.valido.bind(this);
    this.getFormularioResuelto = this.getFormularioResuelto.bind(this);
    // {
    //   console.log("formularios completadas: ", this.props.formularios);
    //   console.log(
    //     "formularios resultadosFormulario: ",
    //     this.props.resultadosFormulario
    //   );
    // }
  }

  getFormularioResuelto = id => {
    let formularioResuelto;
    this.props.resultadosFormulario.map(item => {
      item.idFormulario === id ? (formularioResuelto = item) : null;
    });
    // console.log("getFormularoResulta: ", formularioResuelto);
    return formularioResuelto;
  };

  verFormulario(formulario) {
    this.props.navigation.navigate("FormularioDetailEdit", {
      formulario,
      idTask: this.props.idTask,
      formularioResuelto: this.getFormularioResuelto(formulario.id)
    });
  }

  habilitadoModificacion = id => {
    let habilitado = false;
    this.props.resultadosFormulario.map(item => {
      item.idFormulario === id
        ? item.modificacionHabilitada
          ? (habilitado = true)
          : null
        : null;
    });
    return habilitado;
  };

  valido = id => {
    let habilitado = false;
    this.props.resultadosFormulario.map(item => {
      item.idFormulario === id
        ? item.valido
          ? (habilitado = true)
          : null
        : null;
    });
    return habilitado;
  };

  render() {
    return (
      <Block flex={1}>
        <Overlay
          height={height / 3}
          isVisible={this.state.visibleAgregar}
          onBackdropPress={this.visible}
        >
          <Block margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Text style={{ marginBottom: 15 }}>
              Al confirmar pasara la tarea a estado seleccionada.
            </Text>
            <Block row space="between">
              <ButtonComponents
                color={theme.colors.green2}
                onPress={this.confirmarAgregarTarea}
              >
                <Text bold white center>
                  Confirmar
                </Text>
              </ButtonComponents>
              <ButtonComponents
                color={theme.colors.accent}
                onPress={this.visible}
              >
                <Text bold white center>
                  Cancelar
                </Text>
              </ButtonComponents>
            </Block>
          </Block>
        </Overlay>

        <Overlay
          height={height / 3}
          isVisible={this.state.visibleEliminarContingencia}
          onBackdropPress={this.visible}
        >
          <Block margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Text style={{ marginBottom: 15 }}>
              Al confirmar eliminara la tarea de Contingencia.
            </Text>
            <Block row space="between">
              <ButtonComponents
                color={theme.colors.green2}
                onPress={this.confirmarEliminarContingencia}
              >
                <Text bold white center>
                  Confirmar
                </Text>
              </ButtonComponents>
              <ButtonComponents
                color={theme.colors.accent}
                onPress={this.visibleEliminarContingencia}
              >
                <Text bold white center>
                  Cancelar
                </Text>
              </ButtonComponents>
            </Block>
          </Block>
        </Overlay>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          <Block>
            <Block flex={false} row space="between" style={styles.categories}>
              {this.props.formularios.map((formulario, i) => (
                <TouchableOpacity
                  key={formulario.id}
                  onPress={() =>
                    this.habilitadoModificacion(formulario.id)
                      ? this.verFormulario(formulario)
                      : Alert.alert(
                          "No habilitada",
                          "La modificacion no esta permitida en este formulario.",
                          [
                            {
                              text: "OK"
                            }
                          ]
                        )
                  }
                >
                  <Card
                    key={formulario.id}
                    middle
                    shadow
                    style={
                      this.habilitadoModificacion(formulario.id)
                        ? styles.category
                        : styles.categoryNotModify
                    }
                  >
                    <View style={{ flexDirection: "row", flex: 1 }}>
                      <View style={{ flexDirection: "column", flex: 0.3 }}>
                        <Badge
                          margin={[0, 0, 5]}
                          size={50}
                          color={theme.colors.tertiary}
                        >
                          <Icon
                            name="certificate"
                            type="font-awesome"
                            color={theme.colors.white}
                          />
                        </Badge>
                        <Text medium height={20}>
                          {`Id: ` + formulario.id}
                        </Text>
                      </View>
                      <View style={{ flexDirection: "column", flex: 0.7 }}>
                        <View
                          style={{
                            flexDirection: "column"
                          }}
                        >
                          <View
                            style={{
                              marginTop: 15,
                              flexDirection: "row",
                              alignItems: "center",
                              justifyContent: "center"
                            }}
                          >
                            <Icon
                              name="clipboard"
                              type="font-awesome"
                              color={theme.colors.black}
                            />
                            <Text caption>{` ` + formulario.nombre}</Text>
                          </View>
                          {/* <Text caption center> */}
                          {/* {JSON.stringify(this.valido(formulario.id))} */}
                          <Icon
                            name={
                              this.valido(formulario.id)
                                ? "check"
                                : "star-half-o"
                            }
                            type="font-awesome"
                            color={
                              this.valido(formulario.id)
                                ? theme.colors.green2
                                : theme.colors.tertiary
                            }
                          />
                          {/* </Text> */}
                        </View>
                      </View>
                    </View>
                  </Card>
                </TouchableOpacity>
              ))}
            </Block>
            <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          </Block>
        </ScrollView>
      </Block>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    guardarTareas: task => dispatch(taskActions.agregarTarea(task)),
    eliminarTareaContingencia: task =>
      dispatch(taskActions.eliminarTareaContingencia(task)),
    getTareas: usuario => dispatch(taskActions.getTareas(usuario))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Finalizados);

const styles = StyleSheet.create({
  categories: {
    paddingTop: theme.sizes.base,
    flexWrap: "wrap",
    paddingHorizontal: theme.sizes.base * 2
  },
  category: {
    minWidth: width - theme.sizes.base * 4
  },
  categoryNotModify: {
    minWidth: width - theme.sizes.base * 4,
    backgroundColor: "lightgray"
  }
});
