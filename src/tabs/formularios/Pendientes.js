import React, { Component } from "react";
import {
  Alert,
  Dimensions,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View,
  RefreshControl
} from "react-native";
import { Icon, Overlay, Badge as BadgeElements } from "react-native-elements";
import { connect } from "react-redux";
import {
  Card,
  Badge,
  Block,
  Button as ButtonComponents,
  Text,
  Divider
} from "../../components";
import { theme } from "../../constants";

import { taskActions } from "../../actions/index";

const height = Dimensions.get("window").height;
const { width } = Dimensions.get("window");

class Pendientes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tareaContingencia: null,
      tarea: null,
      visibleAgregar: false,
      visibleConfirmar: false,
      visibleEliminarContingencia: false,
      tareas: [],
      tareasContingencia: [],
      checkedIds: [],
      refreshing: false
    };

    this.verFormulario = this.verFormulario.bind(this);
  }

  verFormulario = formulario => () => {
    // console.log("ver form: ", this.props.idTask);
    this.props.navigation.navigate("FormularioDetail", {
      formulario,
      idTask: this.props.idTask
    });
  };

  render() {
    return (
      <Block flex={1}>
        <Overlay
          height={height / 3}
          isVisible={this.state.visibleAgregar}
          onBackdropPress={this.visible}
        >
          <Block margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Text style={{ marginBottom: 15 }}>
              Al confirmar pasara la tarea a estado seleccionada.
            </Text>
            <Block row space="between">
              <ButtonComponents
                color={theme.colors.green2}
                onPress={this.confirmarAgregarTarea}
              >
                <Text bold white center>
                  Confirmar
                </Text>
              </ButtonComponents>
              <ButtonComponents
                color={theme.colors.accent}
                onPress={this.visible}
              >
                <Text bold white center>
                  Cancelar
                </Text>
              </ButtonComponents>
            </Block>
          </Block>
        </Overlay>

        <Overlay
          height={height / 3}
          isVisible={this.state.visibleEliminarContingencia}
          onBackdropPress={this.visible}
        >
          <Block margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Text style={{ marginBottom: 15 }}>
              Al confirmar eliminara la tarea de Contingencia.
            </Text>
            <Block row space="between">
              <ButtonComponents
                color={theme.colors.green2}
                onPress={this.confirmarEliminarContingencia}
              >
                <Text bold white center>
                  Confirmar
                </Text>
              </ButtonComponents>
              <ButtonComponents
                color={theme.colors.accent}
                onPress={this.visibleEliminarContingencia}
              >
                <Text bold white center>
                  Cancelar
                </Text>
              </ButtonComponents>
            </Block>
          </Block>
        </Overlay>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          <Block>
            <Block flex={false} row space="between" style={styles.categories}>
              {this.props.formularios.map((formulario, i) => (
                <TouchableOpacity
                  key={formulario.id}
                  onPress={this.verFormulario(formulario)}
                >
                  <Card
                    key={formulario.id}
                    middle
                    shadow
                    style={styles.category}
                  >
                    <View style={{ flexDirection: "row", flex: 1 }}>
                      <View style={{ flexDirection: "column", flex: 0.3 }}>
                        <Badge
                          margin={[0, 0, 5]}
                          size={50}
                          color={theme.colors.tertiary}
                        >
                          <Icon
                            name="certificate"
                            type="font-awesome"
                            color={theme.colors.white}
                          />
                        </Badge>
                        <Text medium height={20}>
                          {`Id: ` + formulario.id}
                        </Text>
                      </View>
                      <View style={{ flexDirection: "column", flex: 0.7 }}>
                        <View
                          style={{
                            flexDirection: "column"
                          }}
                        >
                          <View
                            style={{
                              marginTop: 15,
                              flexDirection: "row",
                              alignItems: "center",
                              justifyContent: "center"
                            }}
                          >
                            <Icon
                              name="clipboard"
                              type="font-awesome"
                              color={theme.colors.black}
                            />
                            <Text caption>{` ` + formulario.nombre}</Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </Card>
                </TouchableOpacity>
              ))}
            </Block>
            {/* <Block flex={false} row space="between" style={styles.categories}>
              {this.props.tareas.tareasPendientes.map((tarea, i) => (
                <TouchableOpacity key={tarea.id} onPress={this.verTarea(tarea)}>
                  <Card
                    key={tarea.agente.nombre}
                    middle
                    shadow
                    style={styles.category}
                  >
                    <View style={{ flexDirection: "row", flex: 1 }}>
                      <View style={{ flexDirection: "column", flex: 0.3 }}>
                        <Badge
                          margin={[0, 0, 5]}
                          size={50}
                          color={
                            tarea.estadoAsignado == "ACEPTADO"
                              ? theme.colors.primary
                              : theme.colors.tertiary
                          }
                        >
                          <Icon
                            name="certificate"
                            type="font-awesome"
                            color={theme.colors.white}
                          />
                        </Badge>
                        {tarea.servicio.tramite.id ? (
                          <Text medium height={20}>
                            {tarea.servicio.tramite
                              ? tarea.servicio.tramite.codigo
                              : tarea.servicio.tramite.codigo}
                            {"\n"}
                          </Text>
                        ) : (
                          <Block row center right>
                            <BadgeElements status="warning" />
                            <Text medium height={20}>
                              No posee ID tramite
                            </Text>
                          </Block>
                        )}
                      </View>
                      <View style={{ flexDirection: "column", flex: 0.7 }}>
                        {tarea.servicio.tramite ? (
                          <View
                            style={{
                              flexDirection: "column"
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row"
                              }}
                            >
                              <Icon
                                name="user"
                                type="font-awesome"
                                color={theme.colors.black}
                              />

                              <Text caption>
                                {tarea.servicio.lugar
                                  ? tarea.servicio.ente.numeroCliente
                                  : tarea.servicio.ente.razonSocial}
                                {"\n"}
                              </Text>
                            </View>
                            <View style={{ flexDirection: "row" }}>
                              <Icon
                                name="clock-o"
                                type="font-awesome"
                                color={theme.colors.black}
                              />
                              <Text caption>
                                {tarea.servicio.turno
                                  ? tarea.servicio.turno.horaDesdeString +
                                    " - " +
                                    tarea.servicio.turno.horaHastaString
                                  : tarea.servicio.ente.estadoServicio}
                                {"\n"}
                              </Text>
                            </View>
                            <View style={{ flexDirection: "row" }}>
                              <Icon
                                name="store"
                                type="material"
                                color={theme.colors.black}
                              />
                              <Text caption>
                                {tarea.servicio.lugar
                                  ? tarea.servicio.lugar
                                  : tarea.servicio.descripcionLugar}
                              </Text>
                            </View>
                          </View>
                        ) : (
                          <Block row center right>
                            <BadgeElements status="warning" />
                            <Text caption>No posee tramite</Text>
                          </Block>
                        )}
                      </View>
                    </View>
                  </Card>
                </TouchableOpacity>
              ))}
            </Block> */}
            <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />
          </Block>
        </ScrollView>
      </Block>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    guardarTareas: task => dispatch(taskActions.agregarTarea(task)),
    eliminarTareaContingencia: task =>
      dispatch(taskActions.eliminarTareaContingencia(task)),
    getTareas: usuario => dispatch(taskActions.getTareas(usuario))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Pendientes);

const styles = StyleSheet.create({
  categories: {
    paddingTop: theme.sizes.base,
    flexWrap: "wrap",
    paddingHorizontal: theme.sizes.base * 2
  },
  category: {
    minWidth: width - theme.sizes.base * 4
  }
});
