import React, { Component } from "react";
import {
  Dimensions,
  Alert,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View
} from "react-native";
import {
  Icon,
  CheckBox,
  Badge as BadgeElements,
  Overlay
} from "react-native-elements";
import {
  Card,
  Badge,
  Button as ButtonComponents,
  Block,
  Text
} from "../components";
import { theme, mocks } from "../constants";
import { connect } from "react-redux";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";
const height = Dimensions.get("window").height;
import {
  eliminarTarea,
  traerTareas,
  finalizarTarea,
  comenzarTarea
} from "../actions/taskActions";

class Seleccionadas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleRetroceder: false,
      tareaRetroceder: null,
      tareaId: null,
      visibleFinalizar: false,
      visibleComenzar: false,
      tareas: [],
      checkedIds: [],
      hasLocationPermissions: false,
      locationResult: null
    };

    // This binding is necessary to make `this` work in the callback
    this.renderTareas = this.renderTareas.bind(this);
    this.verTarea = this.verTarea.bind(this);
    this.visibleRetroceder = this.visibleRetroceder.bind(this);
    this.visibleRetrocederFalse = this.visibleRetrocederFalse.bind(this);
    this.toggleTarea = this.toggleTarea.bind(this);
    this.eliminarTareaRetroceder = this.eliminarTareaRetroceder.bind(this);
    this.visibleComenzarFalse = this.visibleComenzarFalse.bind(this);
    this.comenzarTarea = this.comenzarTarea.bind(this);
    this.visibleFinalizarFalse = this.visibleFinalizarFalse.bind(this);
    this.finalizarTarea = this.finalizarTarea.bind(this);
  }

  visibleComenzarFalse() {
    this.setState({ visibleComenzar: false });
  }

  visibleFinalizarFalse() {
    this.setState({ visibleFinalizar: false });
  }

  visibleRetrocederFalse() {
    this.setState({ visibleRetroceder: false });
  }

  eliminarTareaRetroceder() {
    this.props.eliminarTarea(this.state.tareaRetroceder);
    this.setState({ visibleRetroceder: false });
  }

  visibleRetroceder = tarea => () => {
    this.setState({
      visibleRetroceder: true,
      tareaRetroceder: tarea
    });
  };

  verTarea = tarea => () => {
    this.props.navigation.navigate("TaskDetail", { tarea });
  };

  componentDidMount() {
    this._getLocationAsync();
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        locationResult: "Permission to access location was denied"
      });
    } else {
      this.setState({ hasLocationPermissions: true });
    }
  };

  toggleTracking = async () => {
    let gpsStatus = await Location.hasServicesEnabledAsync();

    if (gpsStatus) {
      if (this.props.recorrido.isStarted) {
        this.stopLocationUpdates();
      } else {
        await this.startLocationUpdates();
      }
    } else {
      Alert.alert(
        "Alerta",
        "Active el GPS o no podra inicializar o finalizar el Recorrido."
      );
    }
  };

  comenzarTarea = async () => {
    let gpsStatus = await Location.hasServicesEnabledAsync();

    if (gpsStatus) {
      this.setState({
        visibleComenzar: false
      });
      this.props.comenzarTarea(this.state.tareaId);
    } else {
      Alert.alert(
        "Alerta",
        "Active el GPS o no podra inicializar o comenzar la Tarea."
      );
    }
  };

  finalizarTarea = async () => {
    let gpsStatus = await Location.hasServicesEnabledAsync();

    if (gpsStatus) {
      let location = await Location.getCurrentPositionAsync({});
      const newLocation = {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        timestamp: location.timestamp
      };
      this.setState({
        visibleFinalizar: false
      });
      await this.props.finalizarTarea(this.state.tareaId, newLocation);
    } else {
      Alert.alert(
        "Alerta",
        "Active el GPS o no podra inicializar o finalizar la tarea."
      );
    }
  };

  toggleTarea = task => () => {
    if (this.props.recorrido.isStarted) {
      if (task.isStarted) {
        this.setState({
          visibleFinalizar: true,
          tareaId: task.id
        });
      } else {
        this.setState({
          visibleComenzar: true,
          tareaId: task.id
        });
      }
    } else {
      Alert.alert(
        "Alerta",
        "Para comenzar la tarea, primero debe comenzar el recorrido."
      );
    }
  };

  renderTareas() {
    const { navigation } = this.props;
    if (this.props.tareas.tareasSeleccionadas.length === 0) {
      return null;
    }
    return (
      <Block>
        {this.props.tareas.tareasSeleccionadas.map((tarea, i) => {
          if (tarea.id >= 0) {
            return (
              <TouchableOpacity key={tarea.id} onPress={this.verTarea(tarea)}>
                <Card
                  key={tarea.agente.nombre}
                  middle
                  shadow
                  style={styles.category}
                >
                  <View style={{ flexDirection: "row", flex: 1 }}>
                    <View style={{ flexDirection: "column", flex: 0.3 }}>
                      <Badge
                        margin={[0, 0, 5]}
                        size={50}
                        color={
                          tarea.estadoAsignado == "ACEPTADO"
                            ? theme.colors.primary
                            : theme.colors.tertiary
                        }
                      >
                        <Icon
                          name="briefcase"
                          type="font-awesome"
                          color={theme.colors.white}
                        />
                      </Badge>
                      {tarea.servicio.tramite.id ? (
                        <Text medium height={20}>
                          {tarea.servicio.tramite
                            ? tarea.servicio.tramite.codigo
                            : tarea.servicio.tramite.codigo}
                        </Text>
                      ) : (
                        <Block row center right>
                          <BadgeElements status="warning" />
                          <Text medium height={20}>
                            No posee tramite
                          </Text>
                        </Block>
                      )}
                    </View>
                    <View style={{ flexDirection: "column", flex: 0.7 }}>
                      {tarea.servicio.tramite ? (
                        <View>
                          <View style={{ flexDirection: "row" }}>
                            <Icon
                              name="user"
                              type="font-awesome"
                              color={theme.colors.black}
                            />
                            <Text caption>
                              {tarea.servicio.lugar
                                ? tarea.servicio.ente.numeroCliente
                                : tarea.servicio.ente.razonSocial}
                              {"\n"}
                            </Text>
                          </View>
                          <View style={{ flexDirection: "row" }}>
                            <Icon
                              name="clock-o"
                              type="font-awesome"
                              color={theme.colors.black}
                            />
                            <Text caption>
                              {tarea.servicio.turno
                                ? tarea.servicio.turno.horaDesdeString +
                                  " - " +
                                  tarea.servicio.turno.horaHastaString
                                : +tarea.servicio.ente.estadoServicio}
                              {"\n"}
                            </Text>
                          </View>
                          <View style={{ flexDirection: "row" }}>
                            <Icon
                              name="store"
                              type="material"
                              color={theme.colors.black}
                            />
                            <Text caption>
                              {tarea.servicio.lugar
                                ? tarea.servicio.lugar
                                : tarea.servicio.descripcionLugar}
                            </Text>
                          </View>
                        </View>
                      ) : (
                        <Block row center right>
                          <BadgeElements status="warning" />
                          <Text caption>No posee tramite</Text>
                        </Block>
                      )}
                    </View>
                  </View>
                  <View
                    style={{
                      justifyContent: "space-evenly",
                      flexDirection: "row"
                    }}
                  >
                    <CheckBox
                      size={26}
                      iconRight
                      iconType="material"
                      checkedIcon="check-circle"
                      uncheckedIcon="remove-circle-outline"
                      checkedColor="green"
                      uncheckedColor={theme.colors.black}
                      checked={this.state.checked}
                      onPress={this.visibleRetroceder(tarea)}
                    />
                    <CheckBox
                      size={26}
                      iconRight
                      iconType="material"
                      checkedIcon="alarm-on"
                      uncheckedIcon="alarm-off"
                      checkedColor="green"
                      uncheckedColor={theme.colors.black}
                      checked={tarea.isStarted ? true : false}
                      onPress={this.toggleTarea(tarea)}
                    />
                  </View>
                </Card>
              </TouchableOpacity>
            );
          } else {
            return (
              <TouchableOpacity key={tarea.id} onPress={this.verTarea(tarea)}>
                <Card key={i} middle shadow>
                  <View style={{ flexDirection: "row", flex: 1 }}>
                    <View style={{ flexDirection: "column", flex: 0.3 }}>
                      <Badge
                        margin={[0, 0, 15]}
                        size={50}
                        color={theme.colors.primary}
                      >
                        <Icon
                          name="briefcase"
                          type="font-awesome"
                          color={theme.colors.white}
                        />
                      </Badge>
                    </View>

                    <View style={{ flexDirection: "column", flex: 0.7 }}>
                      <Text medium height={20}>
                        Contingencia
                      </Text>
                      <Text gray caption>
                        {tarea.detalle}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      justifyContent: "space-evenly",
                      flexDirection: "row"
                    }}
                  >
                    <CheckBox
                      size={26}
                      iconRight
                      iconType="material"
                      checkedIcon="check-circle"
                      uncheckedIcon="remove-circle-outline"
                      checkedColor="green"
                      uncheckedColor={theme.colors.black}
                      checked={this.state.checked}
                      onPress={this.visibleRetroceder(tarea)}
                    />
                    <CheckBox
                      size={26}
                      iconRight
                      iconType="material"
                      checkedIcon="alarm-on"
                      uncheckedColor={theme.colors.black}
                      uncheckedIcon="alarm-off"
                      checkedColor="green"
                      checked={tarea.isStarted ? true : false}
                      onPress={this.toggleTarea(tarea)}
                    />
                  </View>
                </Card>
              </TouchableOpacity>
            );
          }
        })}
      </Block>
    );
  }

  render() {
    return (
      <Block flex={0.9}>
        <Overlay
          height={height / 3}
          isVisible={this.state.visibleRetroceder}
          onBackdropPress={this.visibleRetrocederFalse}
        >
          <Block margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Text style={{ marginBottom: 15 }}>
              Al confirmar la tarea volvera a Pendiente.
            </Text>
            <Block row space={"between"}>
              <ButtonComponents
                color={theme.colors.green2}
                onPress={this.eliminarTareaRetroceder}
              >
                <Text bold white center>
                  Confirmar
                </Text>
              </ButtonComponents>
              <ButtonComponents
                color={theme.colors.accent}
                onPress={this.visibleRetrocederFalse}
              >
                <Text bold white center>
                  Cancelar
                </Text>
              </ButtonComponents>
            </Block>
          </Block>
        </Overlay>

        <Overlay
          height={height / 3}
          isVisible={this.state.visibleComenzar}
          onBackdropPress={this.visibleComenzarFalse}
        >
          <Block margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Text style={{ marginBottom: 15 }}>
              Al confirmar dara inicio a la tarea.
            </Text>
            <Block row space={"between"}>
              <ButtonComponents
                color={theme.colors.green2}
                onPress={this.comenzarTarea}
              >
                <Text bold white center>
                  Confirmar
                </Text>
              </ButtonComponents>
              <ButtonComponents
                color={theme.colors.accent}
                onPress={this.visibleComenzarFalse}
              >
                <Text bold white center>
                  Cancelar
                </Text>
              </ButtonComponents>
            </Block>
          </Block>
        </Overlay>

        <Overlay
          height={height / 3}
          isVisible={this.state.visibleFinalizar}
          onBackdropPress={this.visibleFinalizarFalse}
        >
          <Block margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Text style={{ marginBottom: 15 }}>
              Al confirmar finalizara la tarea.
            </Text>
            <Block row space={"between"}>
              <ButtonComponents
                color={theme.colors.green2}
                onPress={this.finalizarTarea}
              >
                <Text bold white center>
                  Finalizar
                </Text>
              </ButtonComponents>
              <ButtonComponents
                color={theme.colors.accent}
                onPress={this.visibleFinalizarFalse}
              >
                <Text bold white center>
                  Cancelar
                </Text>
              </ButtonComponents>
            </Block>
          </Block>
        </Overlay>

        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{ paddingVertical: theme.sizes.base * 1 }}
        >
          <Block>
            <Block flex={false} row space="between" style={styles.categories}>
              {this.renderTareas()}
            </Block>
          </Block>
        </ScrollView>
      </Block>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    eliminarTarea: task => dispatch(eliminarTarea(task)),
    traerTareas: () => dispatch(traerTareas()),
    finalizarTarea: (idTask, location) =>
      dispatch(finalizarTarea(idTask, location)),
    comenzarTarea: idTask => dispatch(comenzarTarea(idTask))
  };
};

export default connect(null, mapDispatchToProps)(Seleccionadas);

const styles = StyleSheet.create({
  categories: {
    flexWrap: "wrap",
    paddingHorizontal: theme.sizes.base * 2,
    marginBottom: theme.sizes.base * 3
  }
});
