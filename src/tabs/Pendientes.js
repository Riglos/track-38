import React, { Component } from "react";
import {
  Alert,
  Dimensions,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View,
  RefreshControl
} from "react-native";
import {
  Icon,
  CheckBox,
  Overlay,
  Badge as BadgeElements
} from "react-native-elements";
import { connect } from "react-redux";
import {
  Card,
  Badge,
  Block,
  Button as ButtonComponents,
  Text,
  Divider
} from "../components";
import { theme } from "../constants";

import { taskActions } from "../actions/index";

const height = Dimensions.get("window").height;
const { width } = Dimensions.get("window");

class Pendientes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tareaContingencia: null,
      tarea: null,
      visibleAgregar: false,
      visibleConfirmar: false,
      visibleEliminarContingencia: false,
      tareas: [],
      tareasContingencia: [],
      checkedIds: [],
      refreshing: false
    };
    this.confirmarEliminarContingencia = this.confirmarEliminarContingencia.bind(
      this
    );
    this.confirmarAgregarTarea = this.confirmarAgregarTarea.bind(this);
    this.agregarTarea = this.agregarTarea.bind(this);
    this.eliminarContingencia = this.eliminarContingencia.bind(this);
    this.visible = this.visible.bind(this);
    this.visibleEliminarContingencia = this.visibleEliminarContingencia.bind(
      this
    );
    this.reduxGuardarTareas = this.reduxGuardarTareas.bind(this);
    this.verTarea = this.verTarea.bind(this);
  }

  reduxGuardarTareas = tarea => () => {
    this.props.guardarTareas(tarea);
  };

  agregarTarea = tarea => () => {
    this.setState({
      visibleAgregar: !this.state.visibleAgregar,
      tarea
    });
  };

  verTarea = tarea => () => {
    this.props.navigation.navigate("TaskDetail", { tarea });
  };

  visible() {
    this.setState({ visibleAgregar: false });
  }

  visibleEliminarContingencia() {
    this.setState({ visibleEliminarContingencia: false });
  }

  eliminarContingencia = tarea => () => {
    this.setState({
      visibleEliminarContingencia: !this.state.eliminarTareaContingencia,
      tareaContingencia: tarea
    });
  };

  confirmar() {
    Alert.alert("Alerta", "confirme la operacion");
    this.setState({ visibleConfirmar: true });
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.props.getTareas(this.props.auth).then(() => {
      this.setState({ refreshing: false });
    });
  };

  renderTareasContingencia() {
    if (this.props.tareas.tareasContingencia.length === 0) {
      return null;
    }
    return (
      <Block>
        {this.props.tareas.tareasContingencia.map((tarea, i) => {
          return (
            <TouchableOpacity key={tarea.id} onPress={this.verTarea(tarea)}>
              <Card key={i} middle shadow>
                <View
                  style={{
                    flexDirection: "row",
                    flex: 1
                  }}
                >
                  <View
                    style={{
                      flexDirection: "column",
                      flex: 0.3
                    }}
                  >
                    <Badge
                      margin={[0, 0, 15]}
                      size={50}
                      color={theme.colors.primary}
                    >
                      <Icon
                        name="briefcase"
                        type="font-awesome"
                        color={theme.colors.white}
                      />
                    </Badge>
                  </View>
                  <View
                    style={{
                      flexDirection: "column",
                      flex: 0.7
                    }}
                  >
                    <Text medium height={20}>
                      Contingencia {i}
                    </Text>
                    <Text caption>{tarea.detalle}</Text>
                  </View>
                </View>
                <View
                  style={{
                    justifyContent: "space-evenly",
                    flexDirection: "row"
                  }}
                >
                  <CheckBox
                    size={26}
                    iconRight
                    iconType="material"
                    checkedIcon="check-circle"
                    uncheckedIcon="add-circle-outline"
                    checkedColor="green"
                    uncheckedColor={theme.colors.black}
                    checked={this.state.checked}
                    onPress={this.agregarTarea(tarea)}
                  />
                  <CheckBox
                    size={26}
                    iconRight
                    iconType="material-community"
                    checkedIcon="delete"
                    uncheckedIcon="delete-circle"
                    checkedColor="green"
                    uncheckedColor={theme.colors.black}
                    checked={this.state.checked}
                    onPress={this.eliminarContingencia(tarea)}
                  />
                </View>
              </Card>
            </TouchableOpacity>
          );
        })}
      </Block>
    );
  }

  confirmarAgregarTarea() {
    const { tarea } = this.state;
    this.setState({
      visibleAgregar: false
    });
    this.props.guardarTareas(tarea);
  }

  confirmarEliminarContingencia() {
    const { tareaContingencia } = this.state;
    this.setState({ visibleEliminarContingencia: false });
    this.props.eliminarTareaContingencia(tareaContingencia);
  }

  render() {
    return (
      <Block flex={1}>
        <Overlay
          height={height / 3}
          isVisible={this.state.visibleAgregar}
          onBackdropPress={this.visible}
        >
          <Block margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Text style={{ marginBottom: 15 }}>
              Al confirmar pasara la tarea a estado seleccionada.
            </Text>
            <Block row space="between">
              <ButtonComponents
                color={theme.colors.green2}
                onPress={this.confirmarAgregarTarea}
              >
                <Text bold white center>
                  Confirmar
                </Text>
              </ButtonComponents>
              <ButtonComponents
                color={theme.colors.accent}
                onPress={this.visible}
              >
                <Text bold white center>
                  Cancelar
                </Text>
              </ButtonComponents>
            </Block>
          </Block>
        </Overlay>

        <Overlay
          height={height / 3}
          isVisible={this.state.visibleEliminarContingencia}
          onBackdropPress={this.visible}
        >
          <Block margin={[theme.sizes.base, theme.sizes.base * 2]}>
            <Text style={{ marginBottom: 15 }}>
              Al confirmar eliminara la tarea de Contingencia.
            </Text>
            <Block row space="between">
              <ButtonComponents
                color={theme.colors.green2}
                onPress={this.confirmarEliminarContingencia}
              >
                <Text bold white center>
                  Confirmar
                </Text>
              </ButtonComponents>
              <ButtonComponents
                color={theme.colors.accent}
                onPress={this.visibleEliminarContingencia}
              >
                <Text bold white center>
                  Cancelar
                </Text>
              </ButtonComponents>
            </Block>
          </Block>
        </Overlay>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          <Block>
            <Block flex={false} row space="between" style={styles.categories}>
              {this.props.tareas.tareasPendientes.map((tarea, i) => (
                <TouchableOpacity key={tarea.id} onPress={this.verTarea(tarea)}>
                  <Card
                    key={tarea.agente.nombre}
                    middle
                    shadow
                    style={styles.category}
                  >
                    <View style={{ flexDirection: "row", flex: 1 }}>
                      <View style={{ flexDirection: "column", flex: 0.3 }}>
                        <Badge
                          margin={[0, 0, 5]}
                          size={50}
                          color={
                            tarea.estadoAsignado == "ACEPTADO"
                              ? theme.colors.primary
                              : theme.colors.tertiary
                          }
                        >
                          <Icon
                            name="briefcase"
                            type="font-awesome"
                            color={theme.colors.white}
                          />
                        </Badge>
                        {tarea.servicio.tramite.id ? (
                          <Text medium height={20}>
                            {tarea.servicio.tramite
                              ? tarea.servicio.tramite.codigo
                              : tarea.servicio.tramite.codigo}
                            {"\n"}
                          </Text>
                        ) : (
                          <Block row center right>
                            <BadgeElements status="warning" />
                            <Text medium height={20}>
                              No posee ID tramite
                            </Text>
                          </Block>
                        )}
                      </View>
                      <View style={{ flexDirection: "column", flex: 0.7 }}>
                        {tarea.servicio.tramite ? (
                          <View
                            style={{
                              flexDirection: "column"
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row"
                              }}
                            >
                              <Icon
                                name="user"
                                type="font-awesome"
                                color={theme.colors.black}
                              />

                              <Text caption>
                                {tarea.servicio.lugar
                                  ? tarea.servicio.ente.numeroCliente
                                  : tarea.servicio.ente.razonSocial}
                                {"\n"}
                              </Text>
                            </View>
                            <View style={{ flexDirection: "row" }}>
                              <Icon
                                name="clock-o"
                                type="font-awesome"
                                color={theme.colors.black}
                              />
                              <Text caption>
                                {tarea.servicio.turno
                                  ? tarea.servicio.turno.horaDesdeString +
                                    " - " +
                                    tarea.servicio.turno.horaHastaString
                                  : tarea.servicio.ente.estadoServicio}
                                {"\n"}
                              </Text>
                            </View>
                            <View style={{ flexDirection: "row" }}>
                              <Icon
                                name="store"
                                type="material"
                                color={theme.colors.black}
                              />
                              <Text caption>
                                {tarea.servicio.lugar
                                  ? tarea.servicio.lugar
                                  : tarea.servicio.descripcionLugar}
                              </Text>
                            </View>
                          </View>
                        ) : (
                          <Block row center right>
                            <BadgeElements status="warning" />
                            <Text caption>No posee tramite</Text>
                          </Block>
                        )}
                      </View>
                    </View>
                    <View
                      style={{
                        justifyContent: "space-evenly",
                        flexDirection: "row"
                      }}
                    >
                      <CheckBox
                        size={26}
                        iconRight
                        iconType="material"
                        checkedIcon="check-circle"
                        uncheckedIcon="add-circle-outline"
                        checkedColor="green"
                        uncheckedColor={theme.colors.black}
                        checked={this.state.checked}
                        onPress={this.agregarTarea(tarea)}
                      />
                      <CheckBox
                        size={26}
                        iconRight
                        iconType="material-community"
                        checkedIcon="check-circle"
                        uncheckedIcon="close-circle-outline"
                        checkedColor="green"
                        uncheckedColor={theme.colors.black}
                        checked={this.state.checked}
                        onPress={this.reduxGuardarTareas(tarea)}
                      />
                    </View>
                  </Card>
                </TouchableOpacity>
              ))}
            </Block>
            <Divider margin={[theme.sizes.base, theme.sizes.base * 2]} />

            <Block flex={false} row space="between" style={styles.categories}>
              {this.renderTareasContingencia()}
            </Block>
          </Block>
        </ScrollView>
      </Block>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    guardarTareas: task => dispatch(taskActions.agregarTarea(task)),
    eliminarTareaContingencia: task =>
      dispatch(taskActions.eliminarTareaContingencia(task)),
    getTareas: usuario => dispatch(taskActions.getTareas(usuario))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Pendientes);

const styles = StyleSheet.create({
  categories: {
    paddingTop: theme.sizes.base,
    flexWrap: "wrap",
    paddingHorizontal: theme.sizes.base * 2
  },
  category: {
    minWidth: width - theme.sizes.base * 4
  }
});
