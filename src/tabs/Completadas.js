import React, { Component } from "react";
import {
  Dimensions,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View
} from "react-native";

import { Icon, Badge as BadgeElements } from "react-native-elements";
import { Card, Badge, Block, Text } from "../components";
import { theme } from "../constants";
import { connect } from "react-redux";

import { finalizar } from "../actions/recorridoActions";

class Completadas extends Component {
  constructor(props) {
    super(props);
    this.verTarea = this.verTarea.bind(this);
  }

  verTarea = tarea => () => {
    this.props.navigation.navigate("TaskDetail", { tarea });
  };

  renderTareas() {
    const { navigation } = this.props;
    if (this.props.tareas.tareasFinalizadas.length === 0) {
      return null;
    }
    return (
      <Block>
        {this.props.tareas.tareasFinalizadas.map((tarea, i) => {
          if (tarea.id >= 0) {
            return (
              <TouchableOpacity key={tarea.id} onPress={this.verTarea(tarea)}>
                <Card
                  key={tarea.agente.nombre}
                  middle
                  shadow
                  style={styles.category}
                >
                  <View style={{ flexDirection: "row", flex: 1 }}>
                    <View style={{ flexDirection: "column", flex: 0.3 }}>
                      <Badge
                        margin={[0, 0, 15]}
                        size={50}
                        color={theme.colors.green2}
                      >
                        <Icon
                          name="briefcase"
                          type="font-awesome"
                          color={theme.colors.white}
                        />
                      </Badge>
                      {tarea.servicio.tramite.id ? (
                        <Text medium height={20}>
                          {tarea.servicio.tramite
                            ? tarea.servicio.tramite.codigo
                            : tarea.servicio.tramite.codigo}
                          {"\n"}
                        </Text>
                      ) : (
                        <Block row center right>
                          <BadgeElements status="warning" />
                          <Text medium height={20}>
                            No posee tramite
                          </Text>
                        </Block>
                      )}
                    </View>
                    <View style={{ flexDirection: "column", flex: 0.7 }}>
                      {tarea.servicio.tramite ? (
                        <View>
                          <View style={{ flexDirection: "row" }}>
                            <Icon
                              name="user"
                              type="font-awesome"
                              color={theme.colors.black}
                            />
                            <Text caption>
                              {tarea.servicio.lugar
                                ? tarea.servicio.ente.numeroCliente
                                : tarea.servicio.ente.razonSocial}
                              {"\n"}
                            </Text>
                          </View>
                          <View style={{ flexDirection: "row" }}>
                            <Icon
                              name="clock-o"
                              type="font-awesome"
                              color={theme.colors.black}
                            />
                            <Text caption>
                              {tarea.servicio.turno
                                ? tarea.servicio.turno.horaDesdeString +
                                  " - " +
                                  tarea.servicio.turno.horaHastaString
                                : tarea.servicio.ente.estadoServicio}
                              {"\n"}
                            </Text>
                          </View>
                          <View style={{ flexDirection: "row" }}>
                            <Icon
                              name="store"
                              type="material"
                              color={theme.colors.black}
                            />
                            <Text caption>
                              {tarea.servicio.lugar
                                ? tarea.servicio.lugar
                                : tarea.servicio.descripcionLugar}
                            </Text>
                          </View>
                        </View>
                      ) : (
                        <Block row center right>
                          <BadgeElements status="warning" />
                          <Text caption>No posee tramite</Text>
                        </Block>
                      )}
                    </View>
                  </View>
                </Card>
              </TouchableOpacity>
            );
          } else {
            return (
              <TouchableOpacity key={tarea.id} onPress={this.verTarea(tarea)}>
                <Card key={i} center middle shadow style={styles.category}>
                  <View style={{ flexDirection: "row", flex: 1 }}>
                    <View style={{ flexDirection: "column", flex: 0.3 }}>
                      <Badge
                        margin={[0, 0, 15]}
                        size={50}
                        color={theme.colors.green2}
                      >
                        <Icon
                          name="briefcase"
                          type="font-awesome"
                          color={theme.colors.white}
                        />
                      </Badge>
                    </View>
                    <View style={{ flexDirection: "column", flex: 0.7 }}>
                      <Text medium height={20}>
                        Contingencia
                      </Text>
                      <Text gray caption>
                        {tarea.detalle}
                      </Text>
                    </View>
                  </View>
                </Card>
              </TouchableOpacity>
            );
          }
        })}
      </Block>
    );
  }

  render() {
    return (
      <Block flex={0.9}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{ paddingVertical: theme.sizes.base * 1 }}
        >
          <Block>
            <Block flex={false} row space="between" style={styles.categories}>
              {this.renderTareas()}
            </Block>
          </Block>
        </ScrollView>
      </Block>
    );
  }
}

const mapStateToProps = state => {
  return { recorrido: state.recorrido, tareas: state.tareas };
};

const mapDispatchToProps = dispatch => {
  return {
    finalizar: recorrido => dispatch(finalizar(recorrido))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Completadas);

const styles = StyleSheet.create({
  categories: {
    flexWrap: "wrap",
    paddingHorizontal: theme.sizes.base * 2,
    marginBottom: theme.sizes.base * 3
  }
});
